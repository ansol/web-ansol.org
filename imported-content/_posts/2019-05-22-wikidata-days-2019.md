---
categories: []
metadata:
  event_location:
  - event_location_value: Carcavelos
  event_site:
  - event_site_url: http://libraries.fe.unl.pt/index.php/wikidata-days-2019-share-collaborate-transform-shaping-data-in-a-changing-world
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-06-06 23:00:00.000000000 +01:00
    event_start_value2: 2019-06-07 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 670
layout: evento
title: WikiData Days 2019
created: 1558551110
date: 2019-05-22
---

