---
categories: []
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d527939485645524252454d7651584a7864576c3262304e7662576c7a633246764c3039795a4756756379426b5a534255636d46695957786f627939485645524252454e664d5335775a47593d&Fich=GTDADC_1.pdf&Inline=true
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-12-19 14:30:00.000000000 +00:00
    event_start_value2: 2014-12-19 17:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 257
layout: evento
title: Audiências no âmbito da apreciação na especialidade das Propostas de Lei sobre
  Direito de Autor
created: 1418675694
date: 2014-12-15
---
<h5>COMISSÃO DE ASSUNTOS CONSTITUCIONAIS, DIREITOS, LIBERDADES E GARANTIAS</h5><p><strong>Grupo de Trabalho - Direito de Autor e Direitos Conexos [PPL 245, 246 e 247/XII/3.ª (GOV)]</strong></p><p>Audiências no âmbito da apreciação na especialidade da:</p><ul><li>Proposta de Lei n.º 245/XII/3.ª (GOV) - "Regula as entidades de gestão coletiva do direito de autor e dos direitos conexos, inclusive quanto ao estabelecimento em território nacional e à livre prestação de serviços das entidades previamente estabelecidas noutro Estado-Membro da União Europeia ou do Espaço Económico Europeu"</li><li>Proposta de Lei n.º 246/XII/3.ª (GOV) - "Procede à segunda alteração à Lei n.º 62/98, de 1 de setembro, que regula o disposto no artigo 82.º do Código do Direito de Autor e dos Direitos Conexos, sobre a compensação equitativa relativa à cópia privada"</li><li>Proposta de Lei n.º 247/XII/3.ª (GOV) - "Transpõe a Diretiva n.º 2012/28/UE, do Parlamento Europeu e do Conselho, de 25 de outubro, relativa a determinadas utilizações permitidas de obras órfãs, e procede à décima alteração ao Código do Direito de Autor e dos Direitos"</li></ul><p>Audiência conjunta</p><ul><li>AUDIOGEST - Associação para a Gestão e Distribuição de Direitos</li><li>AHP - Associação da Hotelaria de Portugal</li><li>AHRESP - Associação da Hotelaria, Restauração e Similares de Portugal</li><li>GEDIPE - Associação para a Gestão de Direitos de Autor, Produtores e Editores</li><li>FEVIP - Associação Portuguesa de Defesa de Obras Audiovisuais</li></ul><p>Audiência conjunta</p><ul><li><strong>ANSOL - Associação Nacional para o Software Livre</strong></li><li>APED - Associação Portuguesa de Empresas de Distribuição</li><li>GDA - Cooperativa de Gestão dos Direitos dos Artistas, Intérpretes ou Executantes</li></ul>
