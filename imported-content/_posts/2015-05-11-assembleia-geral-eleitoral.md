---
categories:
- assembleia geral
- eleições
metadata:
  tags:
  - tags_tid: 97
  - tags_tid: 98
  node_id: 318
layout: article
title: Assembleia Geral Eleitoral
created: 1431369999
date: 2015-05-11
---
<p>Convocam-se todos os sócios para a Assembleia Eleitoral que terá lugar no dia 11 de Julho de 2015 pelas 14 horas e 30 minutos na Casa das Associações – FAJDP, com a seguinte ordem de trabalhos:</p><ul><li>Eleição dos Orgãos Sociais 2015/2016;</li><li>Apresentação do programa da lista vencedora;</li><li>Outros assuntos.</li></ul><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p>Morada do local onde se irá realizar a Assembleia:</p><p style="margin-left: 30px;">Casa das Associações – FAJDP <br>Rua Mouzinho da Silveira, 234/6/8 <br>4050-017 Porto</p><p>A lista dos sócios efectivos no pleno gozo dos seus direitos sociais pode ser vista em https://ansol.org/socios/lista , e uma indicação dos cargos que são actualmente exercidos pode ser vista em https://ansol.org/contato .</p><h2>Candidaturas</h2><p>Foi apresentada uma candidatura para os órgãos sociais. A Lista A é constituída por:</p><p>Direcção</p><ul><li>Presidente: Marcos Marado (sócio 63)</li><li>Vice-Presidente: Manuel Silva (sócio 70)</li><li>Tesoureiro: Rui Guimarães (sócio 73)</li><li>Secretário: Rúben Leote Mendes (sócio 17)</li><li>Vogal: Pedro Lereno (sócio 88)</li></ul><p>Conselho Fiscal</p><ul><li>Presidente: Jaime Pereira (sócio 52)</li><li>1o Secretário: José Esteves (sócio 14)</li><li>2o Secretário: Rui Seabra (sócio 3)</li></ul><p>Mesa da Assembleia</p><ul><li>Presidente: André Esteves (sócio 5)</li><li>1o Secretário: Sandra Fernandes (sócio 82)</li><li>2o Secretário: Daniel Leite (sócio 20)</li></ul><p><em>A Acta desta Assembleia pode ser vista <a href="https://ansol.org/sites/ansol.org/files/Ata%208%20-%20ANSOL%20-%20Assembleia%20Geral.pdf">aqui</a>.</em></p>
