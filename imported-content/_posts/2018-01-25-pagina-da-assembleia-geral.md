---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 541
layout: page
title: Página da Assembleia Geral
created: 1516891005
date: 2018-01-25
---
<p><strong>Papel da Assembleia Geral.</strong></p><p>A Assembleia Geral tem o papel de definir a vontade colectiva dos sócios e eleger e monitorizar os corpos sociais da associação.</p><p>&nbsp;</p><p><strong>Atas da Assembleia Geral</strong></p><p>&nbsp;</p><p><strong>1º Livro de Atas</strong> - Acta nº1 á Ata nº9</p><hr><p>&nbsp;</p><p>Ata nº1 - Assembleia Geral da fundação da ANSOL</p><p>Ata nº2 -</p><p>Ata nº3 -</p><p>Ata nº4 -</p><p>Ata nº5 -</p><p>Ata nº6 -</p><p>Ata nº7 -</p><p>Ata nº8 -</p><p>Ata nº9 - Assembleia Extraordinária</p><hr><p>&nbsp;</p><p>&nbsp;</p><p><strong>2ºLivro de Atas</strong> - Ata nº10&nbsp; ao corrente, ainda aberto.</p><hr><p>&nbsp;</p><p>Ata nº10 - Assembleia</p><p>Ata nº11 - Assembleia Ordinária</p><p>...</p><hr><p>&nbsp;<strong>A acontecer:</strong></p><p>Ata nº12 - Assembleia Eleitoral de 2018 - Casa das Associações a 27/01/2018</p><p>&nbsp;</p><p><strong>&nbsp;</strong></p><p><strong>&nbsp;</strong></p>
