---
categories: []
metadata:
  event_location:
  - event_location_value: CIIMAR, Matosinhos
  event_site:
  - event_site_url: http://xv-enbe.campus.ciencias.ulisboa.pt/workshops/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-11-14 00:00:00.000000000 +00:00
    event_start_value2: 2019-11-14 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 697
layout: evento
title: Wikipedia for public scientific literacy and science teaching
created: 1569168154
date: 2019-09-22
---

