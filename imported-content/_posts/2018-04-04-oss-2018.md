---
categories:
- free libre software
- open source
- grécia
metadata:
  event_location:
  - event_location_value: Harokopio University,  Athens, Greece
  event_site:
  - event_site_url: https://www.oss2018.org/
    event_site_title: OSS2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-06-07 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-09 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 270
  - tags_tid: 127
  - tags_tid: 271
  node_id: 584
layout: evento
title: OSS 2018
created: 1522867540
date: 2018-04-04
---
<p><span style="font-weight: 400;">The goal of 14th International Conference on Open Source Systems, OSS 2018 is to provide an international forum where a diverse community of professionals from academia, industry, and the public sector, and diverse FLOSS initiatives can come together to share research findings and practical experiences. The conference is also a forum to provide information and education to practitioners, identify directions for further research, and to be an ongoing platform for technology transfer, no matter which form of FLOSS is being pursued.</span></p>
