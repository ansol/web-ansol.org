---
categories: []
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://app.parlamento.pt/BI2/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-03-09 15:30:00.000000000 +00:00
    event_start_value2: 2018-03-09 15:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 551
layout: evento
title: Audição Parlamentar "Uma Internet Aberta"
created: 1520520657
date: 2018-03-08
---
<p>Temos em conta que as comunicações digitais, e em particular a "internet", contribuíram de forma determinante para uma profunda transformação, não só do panorama das telecomunicações, mas em geral da forma como, em aspetos essenciais, se desenvolve a vida de pessoas, organizações e países.&nbsp;Assistimos, nas últimas décadas, a serem vertidos para a rede, prerrogativas de cidadania, numa nova realidade rica de potencialidades mas também de riscos e desafios.</p><p>Face a questões tão importantes como a da governação da Internet em Portugal, ou das ameaças à transparência da rede (net neutrality) ou&nbsp;das questões colocadas pelas diretivas europeias de segurança para a rede, ou para a sua organização e governo, não podemos relegar a reflexão sobre estes, e outros assuntos, somente para as instâncias internacionais, devendo tomar uma posição ativa na procura de soluções que assegurem os interesses dos cidadãos e da soberania nacional.</p><p>Com este intuito, o Grupo Parlamentar do Partido Comunista Português convidou a ANSOL a participar na audição pública entitulada "Uma Internet Aberta".</p>
