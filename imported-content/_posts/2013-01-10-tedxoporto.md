---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: http://tedxoporto.com/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-04-12 23:00:00.000000000 +01:00
    event_start_value2: 2013-04-12 23:00:00.000000000 +01:00
  node_id: 119
layout: evento
title: TEDxO'Porto
created: 1357818395
date: 2013-01-10
---
<p>&nbsp;</p>
<div>
	O tema deste evento &eacute; &quot;Em Fus&atilde;o&quot;. Para resolver novos problemas temos de ir al&eacute;m das&nbsp;solu&ccedil;&otilde;es do costume. Para criar valor verdadeiramente distintivo temos de&nbsp;procurar novas &aacute;reas e disciplinas de conhecimento e mexer, cruzar e&nbsp;misturar. Temos de ser mais ousados! Neste dia ir-se-&agrave; explorar a riqueza da&nbsp;cultura open-source e o seu efeito em diversas &aacute;reas tal como nas artes,&nbsp;cultura, ci&ecirc;ncia, sociedade e tecnologia. Vamos ver como a partilha e&nbsp;colabora&ccedil;&atilde;o pode potenciar novos conceitos e compreender como o passado nos&nbsp;projetar&aacute; no futuro atrav&eacute;s da reinven&ccedil;&atilde;o!</div>
