---
categories: []
metadata:
  event_location:
  - event_location_value: online
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/269089468
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-04-23 20:00:00.000000000 +01:00
    event_start_value2: 2020-04-23 20:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 739
layout: evento
title: Lançamento Focal Fossa // Duelos SuperTuxKart
created: 1583616276
date: 2020-03-07
---
<p>2 vezes por ano (em Abril e Outubro), numa quinta-feira, a comunidade Ubuntu Portugal reúne-se para assinalar/festejar o lançamento de uma nova versão Ubuntu.</p><div class="event-description runningText"><p>Para celebrar o lançamento da versão 20.04 do Ubuntu, que procura trazer o melhor do Software Livre ao público em geral, a Comunidade Ubuntu Portugal e a Associação Nacional para o Software Livre juntam esforços e organizam esta sessão de pura diversão utilizando um dos mais divertidos jogos Software Livre o SuperTuxKart, um jogo de corridas de karts.<br>Venham daí jogar e mostrar-nos as vossas fantásticas habilidades de gamer, na quinta feira dia 23 às 21h.<br><br>Vamos guiar-vos sobre na instalação do SuperTuxKart (idealmente em Ubuntu ;) e depois vamos todos jogar juntos no nosso servidor.<br><br>A conversa que antecede o jogo irá decorrer no Jitsi (só precisam de um browser/navegador para web) no endereço:<br><a href="https://meet.ubcasts.org/Ubuntu-20.04" target="__blank" title="https://meet.ubcasts.org/Ubuntu-20.04" class="link">https://meet.ubcasts.org/Ubuntu-20.04</a><br><br>Venham daí! Vamos jogar SuperTuxKart:<br><a href="https://www.youtube.com/watch?v=Lm1TFDBiIIg" target="__blank" title="https://www.youtube.com/watch?v=Lm1TFDBiIIg" class="link">https://www.youtube.com/watch?v=Lm1TFDBiIIg</a></p></div>
