---
categories: []
metadata:
  event_location:
  - event_location_value: Mérida - Extremadura
  event_site:
  - event_site_url: http://www.congresohispanoluso.com/es/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-09-27 23:00:00.000000000 +01:00
    event_start_value2: 2012-09-28 23:00:00.000000000 +01:00
  node_id: 86
layout: evento
title: Iº Congresso Hispano-Luso de Sofware Livre
created: 1348427720
date: 2012-09-23
---
<p>Decorre pela primeira vez este ano o Congresso Hispano-Luso de Software Livre, e a ANSOL vai estar presente.</p>
