---
categories:
- consultoria
- distribuição/venda
- formação
metadata:
  email:
  - email_email: info@moredata.pt
  servicos:
  - servicos_tid: 7
  - servicos_tid: 8
  - servicos_tid: 3
  site:
  - site_url: http://www.moredata.pt/
    site_title: http://www.moredata.pt/
    site_attributes: a:0:{}
  node_id: 50
layout: servicos
title: MoreData
created: 1334501450
date: 2012-04-15
---
<h2>
	Consultoria</h2>
<p>Consultoria em integra&ccedil;&atilde;o de sistemas com software open-source.</p>
<h2>
	Forma&ccedil;&atilde;o</h2>
<p>Forma&ccedil;&atilde;o em instala&ccedil;&atilde;o, configura&ccedil;&atilde;o, administra&ccedil;&atilde;o e utiliza&ccedil;&atilde;o de software open-source.</p>
<h2>
	Distribui&ccedil;&atilde;o/Venda</h2>
<p>Comercializa&ccedil;&atilde;o de computadores pessoais e servidores com Linux pr&eacute;-instalado e possibilidade de adicionar outro software open-source a pedido do cliente.</p>
