---
categories:
- imprensa
- '2001'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 20
  node_id: 147
layout: page
title: Presença na Imprensa de 2001
created: 1009539619
date: 2001-12-28
---
<ul><li><p class="line862">2001/10/12:&nbsp;<a href="https://web.archive.org/web/20011117083012/https://ansol.org/docs/lancamento.pt.html" class="http">Lançamento público de ANSOL</a></p></li><li><p class="line862">2001/11:&nbsp;<a href="https://ansol.org/sites/ansol.org/files/JUP-Nov2001.pdf" class="http">Porto Cidade Tecnológica 2001. Segunda edição do evento dedicado ao Software Livre</a>. Jaime Villate. Jornal Universitário do Porto, novembro 2001, pág. 14.</p></li></ul>
