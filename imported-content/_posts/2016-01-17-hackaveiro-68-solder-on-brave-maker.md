---
categories: []
metadata:
  event_location:
  - event_location_value: Rua dos Mercadores, 3 - Sotão da Associação Agor@, Aveiro,
      Portugal
  event_site:
  - event_site_url: https://plus.google.com/events/cvebah9h7dfvn8toedjasan6hgs
    event_site_title: 'Hack''Aveiro #68 - ♬!♬! Solder on, brave maker!!! ♬!! ♬!'
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-01-20 21:00:00.000000000 +00:00
    event_start_value2: 2016-01-20 23:45:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 391
layout: evento
title: 'Hack''Aveiro #68 - ♬!♬! Solder on, brave maker!!! ♬!! ♬!'
created: 1453074814
date: 2016-01-17
---
<p>Encontro semanal de Makers, Hackers e Artistas da Associação HackAveiro.&nbsp;Encontro aberto a todos.</p><p>&nbsp;</p><p><strong>Aonde?</strong></p><p>Rua dos Mercadores, 3 - Sotão da Associação Agor@</p><p><strong>Quando?</strong></p><p>20 de Janeiro, 21:00</p><p>&nbsp;</p><p>Traz as tuas ideias, invenções e novidades!!</p>
