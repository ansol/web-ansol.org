---
categories:
- consultoria
- desenvolvimento
- distribuição/venda
- suporte
metadata:
  email:
  - email_email: info@dri-global.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 5
  - servicos_tid: 8
  - servicos_tid: 2
  site:
  - site_url: http://www.dri-global.com
    site_title: Site DRI
    site_attributes: a:0:{}
  node_id: 40
layout: servicos
title: DRI
created: 1334499193
date: 2012-04-15
---
<p><img alt="" src="http://www.dri-global.com/sites/all/themes/custom/quimera/logo.png" style="width: 84px; height: 36px; " /></p>
<div>
	A DRI &eacute; uma empresa global de consultoria de TI. Com escrit&oacute;rios em cinco pa&iacute;ses (Portugal, Espanha, Su&eacute;cia, Dinamarca e EUA) e projectos entregues com &ecirc;xito pelo mundo inteiro, a empresa concentra o seu neg&oacute;cio em quatro &aacute;reas principais: Sites, plataformas e aplica&ccedil;&otilde;es; Mobile &amp; Media Emergentes; BI e Intelig&ecirc;ncia Social; CRM e Social CRM.</div>
<div>
	&nbsp;</div>
<div>
	A DRI constr&oacute;i solu&ccedil;&otilde;es inovadoras utilizando m&eacute;todos &aacute;geis que permitem uma converg&ecirc;ncia entre necessidades de neg&oacute;cio, requisitos funcionais e tecnologia.</div>
