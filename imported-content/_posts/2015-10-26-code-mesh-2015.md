---
categories:
- developers
- free software
- tools
metadata:
  event_location:
  - event_location_value: London, UK
  event_site:
  - event_site_url: http://www.codemesh.io/
    event_site_title: Codemesh
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-02 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-04 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 145
  - tags_tid: 122
  - tags_tid: 146
  node_id: 375
layout: evento
title: Code Mesh 2015
created: 1445828069
date: 2015-10-26
---
<h3 class="marketing-message">Code Mesh 2015:</h3><h3 class="marketing-message">Tutorials: 2 November</h3><h3 class="marketing-message">Conference: 3-4 November</h3><p>We bring together a wide range of alternative technologies and programming languages and the wonderful crazy people who use them to solve real-world problems in software industry. We promote “the right tools for the job", as opposed to automatically choosing the tools at hand. And by ‘tools’ we mean technologies, languages, libraries, databases, operating systems, hardware platforms, or more generally techniques, styles or paradigms.</p>
