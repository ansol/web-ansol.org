---
categories: []
metadata:
  image:
  - image_fid: 60
    image_alt: ''
    image_title: ''
    image_width: 599
    image_height: 399
  node_id: 754
layout: article
title: 'Conferência @fsf: LibrePlanet 2021 Call for Sessions #SoftwareLivre♥'
created: 1601491759
date: 2020-09-30
---
<p>A Free Software Foundation (FSF) abriu uma <a href="https://my.fsf.org/lp-call-for-sessions" target="_blank">chamada para apresentações na conferência LibrePlanet 2021</a>, a ter lugar na primavera de 2021 em data a anunciar. O tema é <strong>Empowering Users</strong> e a data limite para submeter uma proposta é 28 de outubro.</p><p>Ainda sem certeza se a conferência decorrerá em Boston ou online, a FSF está a considerar apresentações por via remota, pelo que é uma oportunidade de participarmos sem as restrições que possamos ter com viagens.</p><p>Mais informação no site da <a href="https://libreplanet.org/2021/" target="_blank">LibrePlanet</a>, onde também podemos ver ou ouvir as sessões da conferência deste ano.</p><p>É também na LibrePlanet que são entregues os Free Software Awards e as <a href="https://www.fsf.org/blogs/community/free-software-awards-recognize-those-who-advance-our-freedom-by-october-28th" target="_blank">nomeações</a> estão também abertas até 28 de outubro.</p><p>&nbsp;</p>
