---
categories:
- android
metadata:
  event_location:
  - event_location_value: Fartech, Rua do Instituto Industrial 7 - 3andar, Lisboa
  event_site:
  - event_site_url: https://www.meetup.com/Android-LX/events/249350145/
    event_site_title: " Android meetup at Farfetch"
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-04-11 18:00:00.000000000 +01:00
    event_start_value2: 2018-04-11 20:15:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 78
  node_id: 562
layout: evento
title: Android meetup at Farfetch
created: 1522800999
date: 2018-04-04
---
<p>Android LX is a dev-oriented meetup at the heart of Lisbon. We meet to share development experiences through technical presentations, healthy discussions and its also a great place to meet new people while sharing a beverage.</p><p>We are inviting the Android community to get together on the 11th of April at the Farfetch Lisbon office, starting at 19h00. We're excited to have on board two awesome speakers that will share a bit of their knowledge.<br><br>- "What makes Android tick" by Diogo Ferreira<br>The Android framework exposes the functionality of heterogeneous devices in a unified way, allowing us to reach billions of potential users. Behind that framework is a 1-billion piece puzzle, seemingly assembled in the dark with the help of a sledgehammer. This talk will shed some light into the several subsystems, how they tie together and why we should care as app developers.<br><br>- "How to use Dagger 2 in Android" by Marcelo Benites<br>Dagger is a complex and powerful library. Because of its flexibility Dagger can be integrated in many different ways in an Android application. This talk aims to describe a pattern on how to use Dagger avoiding common pitfalls and leveraging the main advantages of Dependency Injection (DI).<br><br>At the end we will have some pizzas and drinks kindly sponsored by Farfetch.<br><br>If you're not part of the AndroidLx Slack community already, we'd love to have you on board. Please fill in the following form to get the invite: <a href="https://goo.gl/XG7JrK" target="__blank" title="https://goo.gl/XG7JrK" class="link">https://goo.gl/XG7JrK</a><br><br>See you all there!</p>
