---
categories: []
metadata:
  event_location:
  - event_location_value: Parque das Nações, Lisboa
  event_site:
  - event_site_url: http://evento.portugal-a-programar.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-05-25 08:00:00.000000000 +01:00
    event_start_value2: 2013-05-25 17:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 162
layout: evento
title: Programar 2013 Lisboa
created: 1368467177
date: 2013-05-13
---
<p>Inclui apresentação "Desenvolvimento rápido de sítios web com personalização de Joomla" por Rui Guimarães, membro da Direcção da ANSOL.</p>
