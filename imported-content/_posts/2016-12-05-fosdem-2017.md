---
categories: []
metadata:
  event_location:
  - event_location_value: Bruxelas
  event_site:
  - event_site_url: https://fosdem.org/2017/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-02-04 00:00:00.000000000 +00:00
    event_start_value2: 2017-02-05 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 474
layout: evento
title: FOSDEM 2017
created: 1480946512
date: 2016-12-05
---
<p>A ANSOL vai estar pela FOSDEM! Encontem-nos por lá (ou combinem connosco: marcos.marado@ansol.org )</p>
