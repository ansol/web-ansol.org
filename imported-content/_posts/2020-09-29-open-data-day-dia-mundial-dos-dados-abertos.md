---
categories: []
metadata:
  event_site:
  - event_site_url: https://opendataday.org/
    event_site_title: Open Data Day
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-03-06 00:00:00.000000000 +00:00
    event_start_value2: 2021-03-06 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 750
layout: evento
title: Open Data Day / Dia Mundial dos Dados Abertos
created: 1601391079
date: 2020-09-29
---

