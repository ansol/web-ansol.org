---
categories:
- normas
- normas europeias
- normas abertas
- eu
- europa
- política
- imprensa
metadata:
  tags:
  - tags_tid: 37
  - tags_tid: 191
  - tags_tid: 87
  - tags_tid: 94
  - tags_tid: 35
  - tags_tid: 192
  - tags_tid: 19
  node_id: 485
layout: article
title: Carta Aberta aos Eurodeputados sobre Normas Europeias
created: 1487104078
date: 2017-02-14
---
<div align="center"><img src="https://ansol.org/sites/ansol.org/files/open-standards.png" alt="Normas Abertas"></div><p>A ANSOL - Associação Nacional para o Software Livre enviou uma Carta Aberta aos Eurodeputados do "<a href="http://www.europarl.europa.eu/committees/en/imco/home.html">Internal Market &amp; Consumer Protection</a>", analizando o seu <a href="http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-%2f%2fEP%2f%2fNONSGML%2bCOMPARL%2bPE-595.559%2b01%2bDOC%2bPDF%2bV0%2f%2fEN">relatório preliminar sobre Normas Europeias</a>.</p><p>Este relatório preocupa a ANSOL em particular no sentido que está a ser dado a que se vincule às Normas Europeias a necessidade destas estarem disponíveis em termos "FRAND" (Fair, Reasonable and Non-Discriminatory, ou seja, Justos, Razoáveis e Não-discriminatórios). Apesar de esta nomenculatura aparentar dizer respeito a termos que deveriam ser parabenizáveis, a ANSOL considera-a enganosa. Usando as palavras da <a href="https://fsfe.org/activities/os/why-frand-is-bad-for-free-software.en.html">Free Software Fondation Europe</a>,</p><blockquote>Termos de licenciamento FRAND são tipicamente negociados em segredo e mantidos confidenciais pelas partes envolvidas. Contudo, os termos FRAND aparentam frequentemente obrigar a um pagamento de <em>royalties</em> baseado no volume de distribuição (tal como o número de cópias distribuídas). Além disso, elas raramente permitem o sub-licenciamento a terceiras-partes para obterem os mesmos direitos de implementar a norma. É um facto bem estabelecido que tais requisitos são incompatíveis com algumas das licenças mais comuns com as quais o Software Livre é desenvolvido e distribuído.</blockquote><p>A carta enviada pode ser lida na íntegra <a href="https://ansol.org/sites/ansol.org/files/RelatorioNormasEuropeias.pdf">aqui</a>.</p>
