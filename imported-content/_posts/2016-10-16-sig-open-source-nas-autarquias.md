---
categories:
- gis
- osgeo pt
- osgeo
- autarquias
metadata:
  event_location:
  - event_location_value: Albergaria-a-Velha (Biblioteca Municipal)
  event_site:
  - event_site_url: http://osgeopt.pt/meetup/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-10-26 23:00:00.000000000 +01:00
    event_start_value2: 2016-10-26 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 180
  - tags_tid: 181
  - tags_tid: 182
  - tags_tid: 183
  node_id: 467
layout: evento
title: SIG Open-Source nas Autarquias
created: 1476629220
date: 2016-10-16
---

