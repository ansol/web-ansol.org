---
categories:
- foss
- business
- expo
metadata:
  event_location:
  - event_location_value: Palacio Euskalduna, Bilbao, Espanha
  event_site:
  - event_site_url: https://www.librecon.io
    event_site_title: Librecon
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-11-21 00:00:00.000000000 +00:00
    event_start_value2: 2018-11-22 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 160
  - tags_tid: 287
  - tags_tid: 288
  node_id: 603
layout: evento
title: Librecon 2018
created: 1522941933
date: 2018-04-05
---
<div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="wpb_text_column wpb_content_element "><div class="wpb_wrapper"><p>Organizado por ESLE, Asociación de Empresas de Tecnologías Libres y Conocimiento Abierto de Euskadi, y Deutsche Messe, prestigiosa entidad alemana de organización de congresos y ferias profesionales, LIBRECON powered by CEBIT nace como <strong>la unión de las dos grandes citas de innovación tecnológica nacional y europea en open source</strong>: LibreCon, cuya última edición celebrada en Bilbao congregó a más de 1.500 asistentes y CEBIT, principal barómetro mundial de las tecnologías de la información y el conocimiento abierto.</p><p>LIBRECON powered by CEBIT tendrá su sede en el Palacio Euskalduna, el principal centro de congresos de Euskadi con más de 2.000 m² de espacio expositivo, y reunirá en la capital vizcaína a profesionales de las open technologies en campos como Internet of Things (IoT), Máquinas Inteligentes, Movilidad Futura, Transformación Digital y Ciberseguridad. El evento explorará, desde un enfoque práctico, su aplicación en <strong>sectores estratégicos de la economía como la industria, el sector financiero o la administración pública</strong>.</p><p>Para estos sectores, el evento busca ser el punto de encuentro y referencia para conocer las últimas tendencias en tecnologías abiertas aplicadas a sus ámbitos de trabajo y para intercambiar conocimientos que fomenten el desarrollo de negocios.</p><p>LIBRECON powered by CEBIT representa <strong>una cita ineludible para profesionales de la Industria 4.0, del sector público, el financiero y el de la energía</strong>. De aquellos que, en definitiva, avanzan en la construcción de la denominada Sociedad 5.0, más inteligente e interconectada, adscrita al concepto de Internet of Everything (IoE). Una realidad que se escribe hoy en código abierto.</p></div></div></div></div></div>
