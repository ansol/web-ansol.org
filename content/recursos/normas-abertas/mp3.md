---
categories:
- normas abertas
layout: article
title: É o MP3 uma norma aberta?
date: 2024-05-13
---

De acordo com a Lei 36/2011, uma norma é aberta se cumprir cumulativamente os
seguintes requisitos:

<style type="text/css">
@counter-style alinea {
  system: cyclic;
  symbols: a b c d e f g h i j k l m n o p q r s t u v w x y z;
  suffix: ") ";
}
ol { list-style-type: alinea; }
</style>

1. A sua adopção decorra de um processo de decisão aberto e disponível à participação de todas as partes interessadas;
1. O respectivo documento de especificações tenha sido publicado e livremente disponibilizado, sendo permitida a sua cópia, distribuição e utilização, sem restrições;
1. O respectivo documento de especificações não incida sobre acções ou processos não documentados;
1. Os direitos de propriedade intelectual que lhe sejam aplicáveis, incluindo patentes, tenham sido disponibilizados de forma integral, irrevogável e irreversível ao Estado Português;
1. Não existam restrições à sua implementação.

Tendo os dois primeiros pontos em consideração, observa-se que:

* O MP3 foi aprovado como norma pela ISO, pelo que à partida, o ponto (a) estará cumprido;
* O documento de especificações não é livremente disponibilizado.

O mp3 é constituído por duas especificações. Os documentos de especificações são pagos:

* https://www.iso.org/standard/22412.html
* https://webstore.iec.ch/publication/9821

De onde se conclui que há uma restrição económica na disponibilização. Para
além disto, há também restrições à “cópia, distribuição e utilização”, que não
é permitida, e que se pode confirmar no site da ISO, página das FAQ, na
resposta à questão “What can I do with the standard I have purchased”.

Assim, o ponto b) não é cumprido, pelo que **a norma MP3 não é considerável
aberta**, segundo a Legislação Nacional.
