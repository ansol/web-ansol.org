---
excerpt: "<p>O que t&ecirc;m em comum O Banco de Portugal, a Presidente da C.M. Almada,
  o Chefe de Divis&atilde;o da Gest&atilde;o do Aprovisionamento e Patrim&oacute;nio
  da C.M. Sesimbra, o Presidente do Instituto Polit&eacute;cnico de Leiria e a &nbsp;Reitoria
  da Universidade de Coimbra? S&atilde;o no nosso entender, respons&aacute;veis por
  concursos ilegais, publicados pelas respectivas entidades, tendo contas a prestar
  sobre a aplica&ccedil;&atilde;o dos dinheiros p&uacute;blicos em TIC.</p>\r\n<p>Como
  em muitos outros casos, estes v&ecirc;m de m&atilde;o dada, com um inaceit&aacute;vel
  desperd&iacute;cio de dinheiro p&uacute;blico.</p>\r\n"
categories: []
metadata:
  node_id: 94
layout: page
title: Manifesto do Campo das Cebolas
created: 1350431674
date: 2012-10-17
aliases:
- "/manifesto/campo-das-cebolas/"
- "/node/94/"
- "/page/94/"
---
<p class="rtecenter"><img height="410px;" src="http://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Red_Onion_on_White.JPG/480px-Red_Onion_on_White.JPG" width="410px;" /></p>
<p><strong>O que t&ecirc;m em comum O Banco de Portugal, a Presidente da C.M. Almada, o Chefe de Divis&atilde;o da Gest&atilde;o do Aprovisionamento e Patrim&oacute;nio da C.M. Sesimbra, o Presidente do Instituto Polit&eacute;cnico de Leiria e a &nbsp;Reitoria da Universidade de Coimbra? S&atilde;o no nosso entender, respons&aacute;veis por concursos ilegais, publicados pelas respectivas entidades, tendo contas a prestar sobre a aplica&ccedil;&atilde;o dos dinheiros p&uacute;blicos em TIC.</strong></p>
<p><strong>Como em muitos outros casos, estes v&ecirc;m de m&atilde;o dada, com um inaceit&aacute;vel desperd&iacute;cio de dinheiro p&uacute;blico.</strong></p>
<h3>
	1 - Responsabilidade pol&iacute;tica</h3>
<p>A responsabilidade pol&iacute;tica, tal como as cebolas, funciona por camadas. Camadas que uma sobre a outra recobrem, escondem, protegem de qualquer inc&oacute;modo os n&uacute;cleos em que se tomam as &ldquo;micro-decis&otilde;es&rdquo;.</p>
<p>Ainda que as decis&otilde;es macrosc&oacute;picas sejam da autoria dos Governos, muitas vezes o sucesso ou insucesso de um pa&iacute;s est&aacute; ligado a um gigantesco somat&oacute;rio &agrave; escala micro, intimamente relacionado com o grau de autonomia existente, ou seja o grau de dispers&atilde;o das decis&otilde;es.</p>
<p>Mas o resultado &eacute; sempre atribu&iacute;vel ao poder executivo que &eacute; a pel&iacute;cula exterior de uma silenciosa sobreposi&ccedil;&atilde;o de interesses e vontades. &Eacute; o desconhecimento desta mec&acirc;nica do tipo mil-folhas que por vezes leva os cidad&atilde;os a culparem por tudo e mais alguma coisa o Governo central ou mesmo diretamente o Primeiro Ministro: o d&eacute;fice, os impostos, as inunda&ccedil;&otilde;es e as pragas de escaravelho.</p>
<h3>
	2 - Arquitetura</h3>
<p>As cebolas tecnol&oacute;gicas do Estado t&ecirc;m um grav&iacute;ssimo problema de arquitetura</p>
<ul>
	<li>
		<p>a camada que paga pouco sabe</p>
	</li>
	<li>
		<p>a camada que decide n&atilde;o &eacute; a que paga</p>
	</li>
</ul>
<p>Como a camada que paga &eacute; (era?) rica, n&atilde;o educou aquela que decide a ser poupada. Confundiu-se despesa com investimento. E agora que acabou o dinheiro &ndash; que nunca existiu mas que a quem decide se deu a entender que existia - &nbsp;aquela que decide n&atilde;o quer deixar de decidir, nem decidir passando a usar a cabe&ccedil;a.</p>
<h3>
	3 - O Plano</h3>
<p>Para resolver isto e por imposi&ccedil;&atilde;o do Memorando, o Governo definiu e aprovou em Conselho de Ministros um plano denominado PGERRTIC (Plano Global Estrat&eacute;gico de Racionaliza&ccedil;&atilde;o e Redu&ccedil;&atilde;o de Custos nas Tecnologias de Informa&ccedil;&atilde;o e Comunica&ccedil;&atilde;o). A este plano est&atilde;o vinculados, para j&aacute;, todos os organismos dependentes diretamente da Administra&ccedil;&atilde;o Central. A AMA ficou incumbida da fiscaliza&ccedil;&atilde;o pr&eacute;via de despesas relativas a projetos TIC.</p>
<p><a href="http://tek.sapo.pt/noticias/negocios/avaliacao_previa_de_projetos_tic_no_estado_ja_1270178.html">http://tek.sapo.pt/noticias/negocios/avaliacao_previa_de_projetos_tic_no_estado_ja_1270178.html</a></p>
<p>As despesas em TIC n&atilde;o s&atilde;o uma das maiores componentes do OE, mas s&atilde;o porventura o espelho do que se passa noutros sectores.</p>
<h3>
	4 - Emerg&ecirc;ncia Social</h3>
<p>Neste contexto, e sempre sem menosprezar o facto de o pa&iacute;s estar na fal&ecirc;ncia e a pedir, por favor, algum dinheiro emprestado, era de bom tom que todos os organismos p&uacute;blicos come&ccedil;assem a efetuar um controle rigoroso da despesa. Todos os organismos p&uacute;blicos, mesmo aqueles ainda n&atilde;o sujeitos &agrave; fiscaliza&ccedil;&atilde;o pr&eacute;via, deveriam compreender a situa&ccedil;&atilde;o de emerg&ecirc;ncia social e parar, digamos, de desperdi&ccedil;ar o dinheiro dos cidad&atilde;os.</p>
<h3>
	5 - A Ilegalidade</h3>
<p>Mesmo que o pa&iacute;s n&atilde;o estivesse falido, se pelo contr&aacute;rio tivesse um permanente superavit or&ccedil;amental, determinado tipo de despesas poderia ainda ser vista como pouco inteligente. Mas acima de tudo procedimentos de aquisi&ccedil;&atilde;o ilegais s&atilde;o ilegais em qualquer situa&ccedil;&atilde;o.</p>
<h3>
	6 - Concursos que n&atilde;o s&atilde;o concursos</h3>
<p>Joaquim Ant&oacute;nio Alves Duarte Maria Neto de Sousa, Leonildo Cach&atilde;o, Nuno Pereira, Jo&atilde;o Gabriel Silva e Margarida Marques de Almeida s&atilde;o os respons&aacute;veis macrosc&oacute;picos por seis procedimentos de aquisi&ccedil;&atilde;o ilegais de cerca de 4 milh&otilde;es de EUR. Trata-se de <strong>procedimentos com refer&ecirc;ncias a empresas, marcas e <em>part numbers</em></strong>, algo explicitamente proibido na contrata&ccedil;&atilde;o p&uacute;blica e contr&aacute;rio &agrave; livre de concorr&ecirc;ncia no mercado. S&atilde;o concursos p&uacute;blicos para se comprar algo pr&eacute;-escolhido, que a lei do menor esfor&ccedil;o eternizou <em>by default</em>. S&atilde;o concursos que n&atilde;o s&atilde;o concursos:</p>
<ul>
	<li>
		<a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=405445203">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=405445203</a></li>
	<li>
		<a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406414417">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406414417</a></li>
	<li>
		<a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406402307">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406402307</a></li>
	<li>
		<a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406400736">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406400736</a></li>
	<li>
		<a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=405339645">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=405339645</a> e <a href="http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406392418">http://dre.pt/sug/2s/cp/gettxt.asp?s=udr&amp;iddip=406392418</a></li>
</ul>
<p>Nestes, a culpa n&atilde;o &eacute; do Governo, nem do Primeiro Ministro.</p>
<h3>
	7 - A culpa n&atilde;o morre solteira</h3>
<p class="rtecenter"><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Cortando_cebolla.jpg/640px-Cortando_cebolla.jpg" width="410px;" /></p>
<p>&nbsp;</p>
<p>Continuando a disseca&ccedil;&atilde;o da cebola certamente nos aperceber&iacute;amos que o Exmo. Sr. Coordenador de N&uacute;cleo, a Exma Sr&ordf; Presidente da C.M. Almada, o Chefe de Divis&atilde;o da Gest&atilde;o do Aprovisionamento e Patrim&oacute;nio da C.M. Sesimbra, o Exmo Sr Presidente do Instituto Polit&eacute;cnico de Leiria e a Magn&iacute;fica &nbsp;Reitoria da Universidade de Coimbra (reincidente nesta mat&eacute;ria...) pouco ou nada sabem de programa&ccedil;&atilde;o, administra&ccedil;&atilde;o de redes e sistemas.</p>
<p>A culpa n&atilde;o &eacute; deles.</p>
<h3>
	8 - Spaghetti Dipendenza</h3>
<p>Sob a tutela destas pessoas de inten&ccedil;&otilde;es que, at&eacute; prova em contr&aacute;rio, ser&atilde;o as melhores encontram-se os respetivos Departamentos de Inform&aacute;tica. Estes certamente garantiram aos seus superiores que tais despesas eram absolutamente necess&aacute;rias e que n&atilde;o existem alternativas no mercado.</p>
<p>Estes departamentos, uma vez confrontados com a situa&ccedil;&atilde;o, provavelmente dir&atilde;o que n&atilde;o t&ecirc;m conhecimento de alternativas ou que t&ecirc;m conhecimento mas n&atilde;o t&ecirc;m capacidade para as implementar e gerir. Ou pior, dir&atilde;o que devido &agrave;s decis&otilde;es que eles mesmo tomaram no passado - que os deixaram enredados num esparguete de depend&ecirc;ncias inform&aacute;ticas - n&atilde;o &eacute; tecnicamente poss&iacute;vel mudar de solu&ccedil;&otilde;es. Talvez se atrevam a dizer que t&ecirc;m pouco pessoal e falta de tempo.</p>
<h3>
	9 - Quem paga somos n&oacute;s</h3>
<p>E aqui chegamos ao ponto central: os especialistas de inform&aacute;tica dispersos pelas milhares de entidades dependentes direta ou indiretamente do Estado n&atilde;o s&atilde;o afinal especialistas. S&atilde;o algumas vezes meros montadores de caixas gentilmente vendidas pelas fornecedores habituais, com total incapacidade de adapta&ccedil;&atilde;o &agrave; realidade atual.</p>
<p>Montadores com hor&aacute;rio e emprego garantido, que convencem as tutelas da absoluta necessidade de se responsabilizarem por procedimentos de compra t&atilde;o ruinosos quanto ilegais. Montadores cuja fraca performance financeira n&atilde;o p&otilde;e em causa o posto de trabalho. Pelo contr&aacute;rio, o custo da sua fraca performance &eacute; suavemente distribu&iacute;do por todos os cidad&atilde;os Portugueses.</p>
<h3>
	10 - A Alternativa</h3>
<p>Isto salta &agrave; vista quando se considera que uma alternativa baseada em Software Livre (que j&aacute; t&ecirc;m provas dadas na &aacute;rea econ&oacute;mica) nunca ver&aacute; a luz do dia devido &agrave; ilegalidade destes concursos.</p>
<p>Recentemente foram anunciadas poupan&ccedil;as importantes na implementa&ccedil;&atilde;o de solu&ccedil;&otilde;es assentes em Software Livre numa empresa privada de grande dimens&atilde;o, onde houve um corte nos custos de licenciamento na ordem dos 80%. &Eacute; um facto que n&atilde;o se deve ignorar. Mas n&atilde;o &eacute; nada que n&atilde;o se soubesse anteriormente pois informa&ccedil;&atilde;o sobre projetos deste g&eacute;nero existe aos quatro cantos da Internet.</p>
<p><a href="http://tek.sapo.pt/tek_expert/linux_na_tranquilidade_ao_detalhe_1274617.html">http://tek.sapo.pt/tek_expert/linux_na_tranquilidade_ao_detalhe_1274617.html</a></p>
<p>Projetos desta dimens&atilde;o j&aacute; existem h&aacute; v&aacute;rios anos em muitos dos Estados Europeus at&eacute; agora sem necessidade de interven&ccedil;&atilde;o externa.</p>
<p><a href="http://arstechnica.com/information-technology/2009/03/french-police-saves-millions-of-euros-by-adopting-ubuntu">http://arstechnica.com/information-technology/2009/03/french-police-saves-millions-of-euros-by-adopting-ubuntu</a></p>
<p><a href="http://www.h-online.com/open/news/item/LiMux-project-exceeds-annual-target-1397238.html">http://www.h-online.com/open/news/item/LiMux-project-exceeds-annual-target-1397238.html</a></p>
<h3>
	Conclus&atilde;o</h3>
<p>A responsabilidade pol&iacute;tica, tal como as cebolas funciona por camadas. Camadas que uma sobre a outra recobrem, escondem, protegem de qualquer inc&oacute;modo os n&uacute;cleos em que se tomam as &ldquo;micro-decis&otilde;es&rdquo;.</p>
<p>A culpa n&atilde;o &eacute; (sempre) do Governo.</p>
<p>Est&aacute; na altura das camadas internas responderem pelas suas decis&otilde;es, ou alternativamente, perderem o poder de decis&atilde;o. Estes decisores devem justificar a op&ccedil;&atilde;o pela exclus&atilde;o de produtos concorrentes nos seus procedimentos de aquisi&ccedil;&atilde;o ou o Governo deve passar a tutelar as compras TIC das respetivas entidades.</p>
<p>H&aacute; cidad&atilde;os em situa&ccedil;&atilde;o de pobreza e estudantes a desistirem dos estudos por falta de capacidade financeira. O que dizem disto as Universidades e C&acirc;maras Municipais?</p>
<p>4 milh&otilde;es de EUR s&atilde;o mais de 8000 ordenados m&iacute;nimos e Portugal desperdi&ccedil;a desta forma muitos milh&otilde;es de EUR. Os concursos p&uacute;blicos feitos nestes moldes t&ecirc;m que parar de imediato, por raz&otilde;es legais e por raz&otilde;es morais.</p>
<p>Os seus respons&aacute;veis s&atilde;o, em conjunto, c&uacute;mplices do d&eacute;fice.</p>
