---
layout: page
title: Como contribuir para projectos de Software Livre
date: 2023-10-24
---

## Como usar git para contribuir

Apresentação feita por Marcos Marado, no contexto do Hacktoberfest 2022, sobre
como fazer contribuições para projectos git via interface web do gitlab,
interface web do github, e linha de comandos.

<iframe title="Hacktoberfest 2022 - Breve Introdução ao git, gitlab e github"
        src="https://viste.pt/videos/embed/789d5d3b-6b37-4852-8f2d-80097848c642"
        width="560" height="315"
        sandbox="allow-same-origin allow-scripts allow-popups"
        allowfullscreen=""
        frameborder="0"></iframe>


## Boas prácticas de colaboração

A sessão da ANSOL, liderada por Joana Simões, foca-se em boas práticas para
colaborar em projetos de Software Livre. Ela abordará tópicos como quando e
como apresentar um PR e comunicação com maintainers, usando o projeto pygeoapi
como exemplo.

<iframe title="Hacktoberfest 2023 - Boas prácticas para a colaboração em projectos de Software Livre"
        src="https://viste.pt/videos/embed/bf0ec6f4-bd37-4a4d-a957-f3070aa972d4"
        width="560" height="315"
        sandbox="allow-same-origin allow-scripts allow-popups"
        allowfullscreen=""
        frameborder="0"></iframe>
