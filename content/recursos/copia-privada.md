---
categories: 
- cópia privada
metadata:
  slide:
  - slide_value: 0
  node_id: 10
layout: page
title: Cópia Privada
created: 1332885825
date: 2012-03-27
aliases:
- "/node/10/"
- "/page/10/"
- "/politica/copiaprivada/"
---
<p>A <em>Cópia Privada</em> é um regime jurídico onde por atos legítimos de cópia privada realizados por cidadãos se presume que existe um prejuízo para os titulares de direitos de autor. Desde a sua formalização há várias décadas, nunca ninguém conseguiu demonstrar que existisse qualquer prejuízo, e os poucos estudos feitos concluem que não só não há prejuízo como oferece mais oportunidades de divulgação.</p><p>A forma de <em>compensar</em> esse alegado prejuízo normalmente concretiza-se em taxas aplicadas a equipamentos de armazenamento.</p><p>A última vez que tal foi introduzido foi com a implementação da <a href="http://www.dre.pt/util/getpdf.asp?s=dip&amp;serie=1&amp;iddr=2004.199A&amp;iddip=20042861">lei 50/2004</a>, onde se taxaram pela primeira vez equipamentos de armazenamento digital, nomeadamente nos Compact Disco <strong>Digital</strong> Audio Discs (vulgo CDs) e nos <strong>Digital</strong> Versatile Discs. (vulgo DVDs).</p><p>Publicamente conhecido e combatido pela ANSOL desde 2009, surgiu o <a href="https://www.google.pt/search?q=pl118">projeto de lei 118/XII, proposto pelo Partido Socialista</a>, que não só acrescentaria injustas taxas a todos os equipamentos de suporte digital, como introduziria a perda do direito que os autores têm de não ter expetativas de lucro com alguma obra sua.</p><p>Felizmente toda oposição levantada pela comunidade portuguesa, <a href="https://ansol.org/politica/copiaprivada/prescecc20120208/">incluindo a ANSOL</a>, levou ao esclarecimento da maioria dos grupos parlamentares, levando a que o <a href="http://jonasnuts.com/438326.html">Partido Socialista tenha optado por retirar o projeto</a> para que não fosse imediatamente chumbado. Contudo <strong>prometeram que voltariam a tentar em breve</strong>, regresso esse que ocorreu em 2014, pela mão do Governo. A <strong>ANSOL</strong><strong>&nbsp;</strong>combaterá também o novo <a href="https://web.archive.org/web/20140906134333/https://c.ansol.org/node/33">pacote legislativo</a>, tendo inclusivé criado uma petição nesse sentido. </p><p>Sendo um tópico em constante evolução, aconselhamos a leitura das <a href="https://ansol.org/category/copia-privada">últimas notícias</a> sobre o assunto.</p>
