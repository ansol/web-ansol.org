---
title: Código de Conduta para eventos em Linha
---

Este é o Código de Conduta para eventos em Linha da ANSOL ou a decorrer na infraestrutura da ANSOL.

Um dos objectivos da ANSOL é ser inclusiva para o maior número possível de participantes, com a maior variedade possível de diversidade e *backgrounds*. Como tal, estamos empenhados a providenciar um ambiente amigável, seguro e acolhedor para todos, independentemente do seu género, orientação sexual, abilidade, etnia, estado socioeconómico ou religão.

Os seguintes comportamentos são esperados e pedidos a todos os participantes nos eventos em linha:

* Participe de forma autêntica e activa. Ao fazê-lo, pode contribuir para a saúde e longevidade desta comunidade;
* Exercite consideração e respeito no seu discurso e acções;
* Tente colaboração em detrimento de conflito;
* Abstenha-se de comportamentos ou discursos humilhantes, discriminatórios ou assediantes;
* Tenha em consideração o seu ambiente e o dos outros participantes. Alerte a organização se notar alguma situação perigosa, alguém com problemas ou violações a este Código de Conduta, ainda que pareçam inconsequenciais.

Para reportar algum problema imediatamente, ou partilhar algum feedback, por favor contacte os [actuais órgãos sociais da ANSOL](https://ansol.org/orgaos-sociais/), por email.

*Este código de conduta foi adaptado [de um outro](https://viewsourceconf.org/berlin-2016/code-of-conduct/), e está licenciado com uma [licença Creative Commons Attribution-ShareAlike](http://creativecommons.org/licenses/by-sa/3.0/).*
