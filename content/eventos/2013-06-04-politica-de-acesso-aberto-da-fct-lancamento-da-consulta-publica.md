---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 173
  event:
    location: Teatro Thalia, Lisboa
    site:
      title: 
      url: 
    date:
      start: 2013-06-17 14:00:00.000000000 +01:00
      finish: 2013-06-17 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: Política de Acesso Aberto da FCT | Lançamento da consulta pública
created: 1370381377
date: 2013-06-04
aliases:
- "/evento/173/"
- "/node/173/"
---
<p>A FCT -- Fundação para a Ciência e Tecnologia, I.P., publicará em breve a sua Política de Acesso Aberto para os resultados da Investigação Científica que financia, instrumento essencial para a modernização e abertura do sistema de comunicação científica português e a sua aproximação à vanguarda europeia e mundial, no seguimento do impulso neste capítulo promovido pela Comissão Europeia durante o ano transato.</p><p>Será lançada uma consulta pública destinada à auscultação da comunidade científica nacional acerca da atual proposta de política e que precederá a sua entrada em vigor.</p><p>A participação no evento é gratuita mas, para a devida organização logística e atendendo ao número&nbsp;limitado de lugares, será necessário efetuar a devida inscrição, fazendo referência ao nome e à entidade a&nbsp;que está associado, até ao próximo dia 12 de junho para o seguinte endereço electrónico: Vasco.Vaz@fct.pt .</p><p>&nbsp;</p>
