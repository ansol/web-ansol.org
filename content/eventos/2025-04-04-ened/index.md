---
layout: evento
title: 10° Encontro Nacional de Estudantes De Design
metadata:
  event:
    location: Coimbra
    site:
      url: https://ened.pt/
    date:
      start: 2025-04-04
      finish: 2025-04-06
---

O Encontro Nacional de Estudantes de Design (ENED) nasceu em 2012, em Coimbra. É um evento que, tal como o nome indica, reúne estudantes, profissionais e entusiastas pela área do design, vindos de todas as partes do país.

Tendo começado como uma pequena reunião de estudantes, o ENED tem vindo a crescer e a tornar-se um dos maiores eventos de design a nível nacional, chegando a reunir cerca de 700 pessoas na sua maior edição, e somando mais de 2000 participantes e 100 oradores ao longo das suas 9 edições.

Fazem parte do programa workshops e palestras dadas por oradores de renome nacional e internacional, acrescendo aos diversos momentos lúdicos e culturais que dão, efetivamente, o nome de Encontro ao evento.

Em abril de 2025, o ENED regressa à sua cidade natal para acolher a que será a 10ª edição do evento. Sob o mote “Design Everywhere”, ao longo de três dias será explorado o impacto que o design tem na nossa sociedade, por vezes de maneiras invisíveis e inesperadas, mostrando o impacto e a relevância que a área tem, teve e terá, no passado, presente e futuro
