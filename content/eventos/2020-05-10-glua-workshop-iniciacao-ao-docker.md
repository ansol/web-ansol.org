---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 743
  event:
    location: https://youtu.be/FOBkdK0rCho
    site:
      title: 
      url: 
    date:
      start: 2020-05-13 17:00:00.000000000 +01:00
      finish: 2020-05-13 18:45:00.000000000 +01:00
    map: {}
layout: evento
title: GLUA - Workshop  INICIAÇÃO  AO DOCKER
created: 1589133322
date: 2020-05-10
aliases:
- "/evento/743/"
- "/node/743/"
---
<p style="--original-color: #000000; --original-background-color: #ffffff;">O que é o Docker?<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">O Docker é uma plataforma que permite criar e executar containers. Um container é um unidade standard de software que encapsula todo o código, bem como as suas dependências, para que a aplicação corra de uma forma mais rápida e mais confiável, sempre que é transferida de um ambiente para outro.<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Assim, podemos colocar as aplicações dentro de um container, que possui todos os recursos necessários para a testar e publicar. <br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Isto acaba com o velho problema de um developer criar uma aplicação na sua máquina mas, no momento de a publicar num ambiente de produção, ela não se comporta da maneira que era esperada, devido a alterações no ambiente.</p><p style="--original-color: #000000; --original-background-color: #ffffff;">Mais informações em: https://docker.aettua.pt/</p>
