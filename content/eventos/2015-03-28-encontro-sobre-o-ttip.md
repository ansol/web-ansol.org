---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 302
  event:
    location: Portalegre
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/encontro-dia-14-de-abril-portalegre/
    date:
      start: 2015-04-14 00:00:00.000000000 +01:00
      finish: 2015-04-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro sobre o TTIP
created: 1427561044
date: 2015-03-28
aliases:
- "/evento/302/"
- "/node/302/"
---

