---
categories:
- art
- free software
- radical openness
- áustria
- meeting
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 263
  - tags_tid: 122
  - tags_tid: 264
  - tags_tid: 250
  - tags_tid: 129
  node_id: 580
  event:
    location: afo architekturforum, Linz, Austria
    site:
      title: AMRO18 site
      url: http://www.radical-openness.org/en
    date:
      start: 2018-05-16 00:00:00.000000000 +01:00
      finish: 2018-05-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: AMRO18 - Art Meets Radical Openness
created: 1522861895
date: 2018-04-04
aliases:
- "/evento/580/"
- "/node/580/"
---
<div class="region region-content"><div class="row bs-1col node node--type-page node--view-mode-full"><div class="col-sm-12 bs-region bs-region--main"><div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><h2><strong>2018 - Art Meets Radical Openness (#AMRO18)</strong></h2><h5>AMRO is a biennial community festival in Linz that explores and discusses new challenges between digital culture, art, everyday life, education, politics and active action.</h5><p>&nbsp;</p><p><strong>Event: Wed. 16.05.- Sat. 19.05.2018<br> Location: Linz, Austria<br> Venue: <a href="http://www.afo.at" target="_blank" rel="noopener noreferrer">afo architekturforum oö</a></strong></p><p>&nbsp;</p><p><strong>Call for Participation over<br> Thank you for your proposals! Please be patient with us. we will get back to you until 6 of April!</strong></p><h3>Unmapping Infrastructures</h3><p>The current issue “Unmapping Infrastructures” deals with the idea of “mapping” as a process of becoming aware and then acquiring a critical position about the current landscape of technological infrastructures.</p><p>This conglomerate of machines, human and non-human actors, nation-states and borderless companies is increasingly complex to observe and describe. Nevertheless, we believe that there is more to be seen than a hyper-commercialized structure of interlaced technological layers. Cartographic mapping consists of a series of practices of observing, analyzing and representing a territory to be able to move through it. How can art and activism appropriate the methods of cartographic mapping to produce new, critical and alternative views of the current landscape shaped by different players?</p><p>The festival aims at deepening the thematic areas of digital geopolitics, alternative design methods, activist practices and autonomous infrastructures, themes that offer directions for localizing areas of intervention. Throughout the festival, these topics will be further explored through discussion panels, workshops, and performances.</p><p>The Open Call is addressed to artists, hacktivists, cultural workers, journalists, F/LOSS developers and “improvers of the world” who want to make a contribution (exhibition, workshop, lecture, performance) to this topic.</p><h4>Lectures</h4><p>How can we gain a better understanding of the surrounding infrastructure of autonomous machines, large corporations, post-state nations and commercialized networks? Can we influence the current leading tendencies? How can one find places for critical intervention?</p><h4>Workshop, Hands-on, Presentations</h4><p>Which experiments, alternative tools, and infrastructures can be used to “map the empire” and act upon its weakness? How can one build an ethical infrastructure that allows autonomy and independence from the “big players”?</p><h4>Showcase</h4><p>Since its beginnings, AMRO is a place for exchanging ideas and sharing discussions about on-going processes. Within the framework of the showcase we are able to provide a possibility to display your work within the festival locations or outdoors. You can propose projects that are still in development or that need some external point of view. Installations / objects / interventions … should be accompanied by a short presentation or a skill-sharing workshop.</p><h4>Performances, Nightline</h4><p>If a 3D-rendered tree falls into a virtual forest, and there is no-thing that listens to it, will it produce sound? What is the squawk of the fauna living at the borders of the corporate web? Do bots cry when one is rude to them? We are “simply” looking for the sonic-apparatus that can hack data-capitalism. Got some?\\</p><p>We especially appreciate contributions that float around algorave, live coding, device hackings or other experimental approaches towards machines and sound. You presence at the festival should also include&nbsp; an active skill-sharing part with the other participants.</p><p><em>AMRO Policy </em></p><p><strong>Support </strong><br> We are aware of the fact that nobody is able to work for free. During our last events we were able to provide participants with travel support, private accomodation, food and an appreciative fee. We will do our best to make this possible again in 2018. We have to warn you, though, that no one will get rich by being part of this event!<br> &nbsp;<br><strong>How to support us? </strong></p><p>If it is possible for you to finance your travel and/or accomodation on your own or through a 3rd party (university, grant, etc.), this would be a great help for us. In case you need a letter of invitation please mailto: <a href="mailto:amro@servus.at">amro@servus.at</a><br> &nbsp;</p><p><strong>Sharing Documents &amp; Files </strong><br> Please note that we support Open Standards. 3rd Party (Google, DropBox, etc): We try to avoid using 3rd party services as much as possible. If you want to share something with us it must be publicly accessible.<br> &nbsp;<br><strong>Open Source</strong><br> AMRO (Art Meets Radical Openness) is shaped by the spirit of the Free Software movement. This understanding is an essential basis for the selection of contributions using software and hardware.</p><p><strong>Application deadline: Sunday, 25th of March 2018</strong></p></div></div></div></div>
