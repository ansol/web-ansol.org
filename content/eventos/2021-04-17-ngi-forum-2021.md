---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 788
  event:
    location: Online
    site:
      title: ''
      url: https://2021.ngiforum.eu/
    date:
      start: 2021-05-18 00:00:00.000000000 +01:00
      finish: 2021-05-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: NGI Forum 2021
created: 1618678458
date: 2021-04-17
aliases:
- "/evento/788/"
- "/node/788/"
---
<p>O NGI Forum 2021 é o evento principal da iniciativa "Next Generation Internet", juntando os inovadores Europeus que estão a trabalhar para construir uma Internet para Humanos, que garanta confiança, privacidade e inclusão.</p>
