---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 494
  event:
    location: Lisboa
    site:
      title: 
      url: 
    date:
      start: 2017-04-01 00:00:00.000000000 +01:00
      finish: 2017-04-01 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Arduino Day 2017
created: 1490784068
date: 2017-03-29
aliases:
- "/evento/494/"
- "/node/494/"
---
<p>Seja no ISCTE ou no MILL, o Arduino Day, no primeiro dia de Abril, vai ser celebrado em grande em Lisboa.</p><p><a href="http://iscte.acm.org/arduino-day-2017/">No ISCTE</a>, será das 09h às 18h, com talks, workshops, demos e showcases.</p><p><a href="http://mill.pt/agenda/arduino-afternoon/">No MILL</a>, começa-se às 15h e vai-se até às 20h, com uma workshop, uma sessão de show and tell e convívio.</p>
