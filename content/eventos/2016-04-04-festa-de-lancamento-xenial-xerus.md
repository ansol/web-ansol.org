---
categories:
- ubuntu
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 162
  node_id: 411
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.facebook.com/events/1716547571955307/
    date:
      start: 2016-04-21 00:00:00.000000000 +01:00
      finish: 2016-04-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Festa de lançamento Xenial Xerus
created: 1459778431
date: 2016-04-04
aliases:
- "/evento/411/"
- "/node/411/"
---

