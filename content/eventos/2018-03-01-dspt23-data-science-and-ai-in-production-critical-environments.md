---
categories:
- porto
- uptec
- data
- ia
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 142
  - tags_tid: 211
  - tags_tid: 212
  - tags_tid: 213
  node_id: 548
  event:
    location: UPTEC - Polo Tecnologico, 461 Rua Alfredo Allen · Porto
    site:
      title: DSPT#23 - Data Science and AI in production-critical environments
      url: https://www.meetup.com/datascienceportugal/events/248206465/
    date:
      start: 2018-03-08 18:30:00.000000000 +00:00
      finish: 2018-03-08 21:00:00.000000000 +00:00
    map: {}
layout: evento
title: DSPT#23 - Data Science and AI in production-critical environments
created: 1519935366
date: 2018-03-01
aliases:
- "/evento/548/"
- "/node/548/"
---
<p>Hey everyone!<br>Time for another meetup in Porto. Let's meet to discuss Data Science and complain about the bad weather while having some beers :)<br><br>This time we will have two new speakers talking about deploying AI systems in production-ready and critical environments.<br><br>Luís Matias is coming all the way from NEC Laboratories Europe (Heidelberg, Germany) to speak about the challenges of deploying AI-based software in domains like transportation and logistics.<br><br>We will also have Tiago Baptista from Critical Software presenting how they deploy AI and Machine Learning systems in critical projects. The company provides systems and software services for safety, mission and business-critical applications ranging from aerospace, defense and security, government, finance and many others domains.<br><br>The 23rd meetup of Data Science Portugal (<a href="https://www.facebook.com/datascienceportugal" target="__blank" title="https://www.facebook.com/datascienceportugal" class="link">https://www.facebook.com/datascienceportugal</a>) is going to take place on Thursday, 8th March, 2018 around 18h30, at UPTEC - main auditorium - Allen 455, R. Alfredo Allen, Porto.<br><br>This meetup will be sponsored by Critical Software.<br><br>=== SCHEDULE ===<br>The preliminary agenda for the meetup is the following:<br><br>• 18:30-19:00: Welcome and get together.<br>• 19:00-19:30: Talk 1: “Data Science NOW! (and other not so deep thoughts about AI)”, presented by Luis Matias , NEC Laboratories Europe (Heidelberg, Germany)<br>• 19:40-19:45: Group photo.<br>• 19:45-20:15: Networking / Coffee Break.<br>• 20:15-20:45: Talk 2: AI&amp;ML @Critical Software: A Guided Tour, presented by Tiago Baptista , Critical Software<br>• 20:50: Closing, hanging out and some beers<br>• 21:00: Dinner is optional but it might be an excellent opportunity for networking. You can register on the following list.<br><br>Dinner<br><a href="https://doodle.com/poll/5rkew4nc43zrxqp8" target="__blank" title="https://doodle.com/poll/5rkew4nc43zrxqp8" class="link">https://doodle.com/poll/5rkew4nc43zrxqp8</a><br><br>Do you want to be a sponsor in future meetups? Please contact us to info@datascienceportugal.com<br><br>See you there!<br>-------------------------------------<br><br>Talk 1: “Data Science NOW! (and other not so deep thoughts about AI)”, Luis Matias from NEC Laboratories Europe (Heidelberg, Germany)<br><br>In this presentation, we approach the issue of defining Data Science<br>as the ability to transform raw data into information in an (almost…)<br>automated fashion. We highlight the different stages that may precede<br>the deployment of AI-systems in production-ready environments that<br>actually enclose a good value proposition by using concrete examples<br>of accomplished projects from Transport and Logistics (with open<br>discussion for other verticals). We will also highlight the power of underestimated DS stages nowadays such as data wrangling, exploratory data analysis, visualization and interpretability on providing<br>insights on how valuable your data and models are for your business. I<br>hope to minimize the amount of throws of rotten tomatoes by making the<br>audience laugh now and then with not so deep stuff.<br><br>Short Bio: Dr. Luis Moreira-Matias received his Ms.c. degree in Informatics<br>Engineering and Ph.d. degree in Computer Science (major in Machine<br>Learning) from the University of Porto, in 2009 and 2015,<br>respectively. Luis served in the Program Committee and/or as invited<br>reviewer of multiple high-impact research venues such as KDD, AAAI,<br>IEEE TKDE, ESWA, ECML/PKDD and KAIS, among others. Moreover, he<br>encloses a successful track record of real-world deployment of<br>AI-based software products across EMEA and APAC. Currently, he is Senior Researcher at NEC Laboratories Europe (Heidelberg, Germany), where he is fortunate to lead R&amp;D of AI-based software for Transport, Retail and Fintech industries. He was fortunate to be invited to give keynotes around the globe, ranging locations from Brisbane (Australia) to Las Palmas (Spain). Academically, Luis authored 40+ high-impact peer-reviewer publications on related topics.<br><br>Talk 2: Talk 1: AI&amp;ML @Critical Software: A Guided Tour, Tiago Baptista , Critical Software<br><br>This talk will give a general overview of the Artificial Intelligence and Machine Learning area at Critical Software. Within the company, the department has formally started in mid 2017 as an effort to organize internal knowledge and provide new AI focused products and services. We will give an overview of how this area is seen at Critical and provide a general description of some our projects in AI&amp;ML, and, whenever possible, discuss some of the approaches used.<br><br>Bio:<br><br>Tiago Baptista is a Senior Engineer at Critical Software, working as a member of Artificial Intelligence team. He holds a B.Sc. and a Ph.D., both in Informatics Engineering from that same university. He is a member of the Evolutionary and Complex Systems research group (ECOS) of the Centre for Informatics and Systems of the University of Coimbra (CISUC), where he conducts research mostly on evolutionary computation, artificial life, and complex systems. Previously, he was an Assistant Professor at the Department of Informatics Engineering of the University of Coimbra, where he taught courses such as Introduction to Artificial Intelligence, Introduction to Programming and Problem Solving, or Complex Systems. During his Ph.D. work he created a software multi-agent framework, called BitBang, focused on the simulation of complex systems and artificial life. He authored several peer-reviewed articles published in international journals and conference proceedings.</p>
