---
layout: evento
title: Wikimania 2022
metadata:
  event:
    date:
      start: 2022-08-11
      finish: 2022-08-14
    location: online
    site:
      url: https://wikimania.wikimedia.org/wiki/Wikimania
---

