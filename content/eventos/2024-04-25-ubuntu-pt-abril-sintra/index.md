---
layout: evento
title: Encontro Ubuntu-pt Abril @ Sintra & Release Party
metadata:
  event:
    date:
      start: 2024-04-25 20:00:00.000000000 +01:00
      finish: 2024-04-25 23:00:00.000000000 +01:00
    location: Bar Saloon
    site:
      url: https://discourse.ubuntu.com/t/2024-04-25-encontro-ubuntu-pt-de-abril-sintra-numbat-release-party/44349
---

[![Cartaz](twitterv2.png)](https://discourse.ubuntu.com/t/2024-04-25-encontro-ubuntu-pt-de-abril-sintra-numbat-release-party/44349)

Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal encontra-se no Saloon, em Sintra.

Vem, traz um amigo ou um familiar e vem conviver com a comunidade portuguesa…

Nesta quinta-feira assinalamos também o lançamento do Ubuntu 24.04 Noble Numbat!

## Local:

## Morada:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra

(2 min a pé da estação de comboios da portela de Sintra)


## Mapa:
<iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=-9.38313961029053%2C38.79927673038549%2C-9.37713146209717%2C38.80176631581731&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/38.80052/-9.38014">Ver mapa maior</a></small>
