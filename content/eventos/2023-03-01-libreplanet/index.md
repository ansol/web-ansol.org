---
layout: evento
title: LibrePlanet 2023
metadata:
  event:
    location: Boston e Online
    site:
      url: https://libreplanet.org/2023/
    date:
      start: 2023-03-18
      finish: 2023-03-19
---

LibrePlanet is the annual conference hosted by the Free Software Foundation (FSF). LibrePlanet provides an opportunity for community activists, domain experts, and people seeking solutions for themselves to come together in order to discuss current issues in technology and ethics.

Newcomers are always welcome, and LibrePlanet 2023 will feature programming for all ages and experience levels. The theme for LibrePlanet 2023 is “Charting the Course.”

The free software movement has produced so much more than just a set of common tools used by millions around the world. It also points the way to a freer and autonomous digital existence for all of us: one where it’s the user and not corporate monopolies that control our ways of communicating, how children are educated, and how we monitor information as crucial as our own health. Ingenuity and moral dedication are two important qualities of the free software movement, and we use these for the benefit of all.

This year, LibrePlanet speakers will show ways of progressing the free software community’s understanding of new opportunities and new threats to the movement. We’ll learn about the impact artificial intelligence (AI) has upon the free software community, and the role software freedom has to play in working for ecological sustainability.

Together, we have helped ourselves and others find their freedom in the current digital landscape. Now, it’s time for us to lead the way – or chart a course – through to full digital freedom.
