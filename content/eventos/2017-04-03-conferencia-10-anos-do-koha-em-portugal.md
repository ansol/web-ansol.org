---
categories:
- "#bibliotecas #koha #"
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 195
  node_id: 497
  event:
    location: Instituto Politécnico de Lisboa
    site:
      title: Koha Utilizadores de Portugal
      url: http://www.koha.pt/
    date:
      start: 2017-05-18 00:00:00.000000000 +01:00
      finish: 2017-05-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Conferência 10 anos do KOHA em Portugal
created: 1491237557
date: 2017-04-03
aliases:
- "/evento/497/"
- "/node/497/"
---
Inscrições: https://www.eventbrite.pt/e/bilhetes-conferencia-10-anos-do-koha-em-portugal-18-e-19-de-maio-de-2017-31317781327?aff=es2

O KOHA encontra-se instalado nas Bibliotecas do Instituto Politécnico de Lisboa (IPL) desde 2009.
Em 2007, o KOHA foi instalado no Instituto de Informática do Ministério das Finanças (versão 2.9), tendo passado assim 10 anos do início da sua implementação em Portugal.
A sua disseminação em Portugal tem sido crescente, sendo utilizado presentemente em bibliotecas da Administração Pública Central (8), Autarquias e Rede Escolar (17), Universidades (11), Politécnicos (11) e ainda outras organizações (6).
As Bibliotecas do IPL organizam esta conferência com o objetivo de fazer uma retrospetiva dos 10 anos da implementação do KOHA em Portugal.
Este evento será um local de partilha das experiências atuais e de debate sobre as expetativas futuras em relação ao KOHA.
A conferência irá ter lugar nos dias 18 e 19 de Maio no Auditório Vítor Macieira, na Escola Superior de Comunicação Social em Benfica.
 
CONFERÊNCIA 10 ANOS DO KOHA EM PORTUGAL

PROGRAMA

18 DE MAIO DE 2017

  9h00 – Receção dos participantes
  9h30 – Cerimónia de abertura Presidente do Instituto Politécnico de Lisboa, Presidente da Escola Superior de Comunicação Social, Secretário de Estado das Autarquias Locais (sujeito a confirmação), Secretária de Estado da Ciência Tecnologia e Ensino Superior (sujeito a confirmação), Secretário de Estado da Cultura (sujeito a confirmação)
10h30 – Sistema KOHA: uma retrospectiva
 Moderador Rafael António (Consultor)
2007: a odisseia da biblioteca - Ricardo Marques (ESPAP) (sujeito a confirmação)
O que mudou na minha profissão - Ângelo Fonseca (ESEV)
11h15 – Pausa para café
11h30 – O KOHA no  Ensino Superior
Moderador Eugénia Vasques (ESTC)
Universidade Aveiro - Ana Bela de Jesus Martins
Universidade Beira Interior - José Rosa
Universidade de Lisboa - Ana Rigueiro
Instituto Politécnico de Lisboa - Paula Carvalho
Discussão

13h00 - Intervalo para almoço
14h30 – O KOHA na Administração Pública
Aplicação do sistema em Bibliotecas da Administração Central e Local
16h00 - Pausa para café
16h30 – Implementações do sistema no mercado
Apresentação de empresas de serviços
18h00 – Encerramento
18h30 – Happy hour
Convívio
 

19 DE MAIO DE 2017

9h30 – Abertura dos trabalhos
9h45 – Tecnologias e normalização
A migração e a interoperabilidade na rede
11h00 – Pausa para café
11h30 – Comunicação principal
Comunicação apresentada por um especialista, membro da comunidade internacional KOHA e responsável pela gestão de várias das versões do programa
13h00 – Intervalo para almoço
14h30 –Uma nova gestão da biblioteca
O que se altera  na gestão da biblioteca através do KOHA
16h00 – Pausa para café
16h15 – Debate sobre o futuro da comunidade
Partilha de reflexões sobre a evolução do KOHA nas bibliotecas portuguesas
17h30 – Encerramento
