---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 798
  event:
    location: Online
    site:
      title: ''
      url: https://meta.wikimedia.org/wiki/Festa_da_Wiki-Lusofonia/Eventos/Sess%C3%A3o_principal
    date:
      start: 2021-05-08 00:00:00.000000000 +01:00
      finish: 2021-05-09 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Mesa redonda: O impacto da Wikipédia no mundo da cultura livre | Festa da
  Wiki-Lusofonia'
created: 1619637877
date: 2021-04-28
aliases:
- "/evento/798/"
- "/node/798/"
---

A ANSOL participou nesta Mesa Redonda, que pode ser agora vista em diferido:

<iframe src="https://www.youtube.com/embed/pevyfo9Rwhg"
        width="560" height="315"
        frameborder="0"></iframe>
