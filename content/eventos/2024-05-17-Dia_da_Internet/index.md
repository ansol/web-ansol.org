---
layout: evento
title: Dia da Internet 2024
showcover: false
metadata:
  event:
    date:
      start: 2024-05-17 18:00:00.000000000 +01:00
      finish: 2024-05-17 21:00:00.000000000 +01:00
    location: Sede Nacional da Ordem dos Engenheiros, Lisboa
    site:
      url: https://2024.diadainternet.pt
---
[![Logótipo do Dia da Internet](Dia_da_internet_pt_logo.png)](https://2024.diadainternet.pt)

Em 1969, a ONU declarou o 17 de Maio como o Dia Mundial das Telecomunicações e da Sociedade da Informação. A partir de Janeiro de 2006, esta mesma data também passou a ser conhecida pelo Dia da Internet, com o objectivo de promover a inclusão digital, e nós vamos celebrar a data!

# Programa

Dia 17 de maio de 2024:

* **18h00** Apresentação do Dia Mundial da Internet e do ciclo de debates
* **18h15** Projecção do documentário "Viva Cyclades, The French Engineers who didn’t invent the Internet" de Davide Morandini
* **19h20** Entrevista remota e directa com o realizador Davide Morandini
* **19h35** Mesa Redonda
* **20h30** Encerramento da sessão

# Local

Sede Nacional da Ordem dos Engenheiros

Avenida António Augusto de Aguiar, N.º 3-D, 1069-030 - Lisboa
