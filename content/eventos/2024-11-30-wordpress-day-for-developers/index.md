---
layout: evento
title: WordPress Day for Developers 
showcover: false
metadata:
  event:
    date:
      start: 2024-11-30
      finish: 2024-11-30
    location: ISCTE-IUL, Lisboa
    site:
      url: https://events.wordpress.org/lisboa/2024/wordpress-day-developers/
---

![Logotipo do evento](cover.png)
