---
layout: evento
title: Encontro de Comunidades de Tecnologias Livres
metadata:
  event:
    date:
      start: 2025-01-25
      finish: 2025-01-25
    location: LCD - Laboratório de Criação Digital, Matosinhos, Porto
    site:
      url: https://ectl.pt/
---

![cartaz de anúncio da data do evento](cover.jpg)

Irá decorrer no dia 25 de Janeiro no makerspace LCD Porto (em Matosinho) o primeiro dos "Encontros de Comunidades de Tecnologias Livres" (ECTL).

Trata-se do primeiro de uma série de eventos inspirados no maior evento multi-comunidades em Portugal, a Festa do Software Livre, que é organizado por uma das comunidades fundadoras dos ECTL, a [ANSOL](https://ansol.org), e é também inspirado numa iniciativa que tem a intenção de partilha de conhecimento em um makerspace, o Centro Linux, que é também organizado por uma das comunidade fundadoras dos ECTL, a [Ubuntu-pt](https://ubuntu-pt.org) .

A entrada e a participação são livres e gratuitas para todos!

