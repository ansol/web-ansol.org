---
layout: evento
title: Python Conference in Portugal
metadata:
  event:
    date:
      start: 2024-10-17
      finish: 2024-10-19
    location: Altice Forum, Braga
    site:
      url: https://2024.pycon.pt
---

A PyCon Portuguesa regressa em 2024, desta vez em Braga.
