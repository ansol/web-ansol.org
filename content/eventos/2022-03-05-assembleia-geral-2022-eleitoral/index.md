---
categories:
- assembleia geral
layout: evento
title: Assembleia Geral 2022 Eleitoral
metadata:
  event:
    date:
      start: 2022-03-05 14:00:00
      finish: 2022-03-05 20:00:00
    location: FAJDP - Casa das Associações
---

Informamos todos os sócios da realização da 1º Assembleia Geral Eleitoral de
2022. A mesma terá lugar na Casa das Associações no Porto, no dia 5 de Março de
2022, com início às 14h00 da tarde, com a seguinte ordem de trabalhos:

1. Apresentação e aprovação do relatório e contas de 2021
2. Apresentação da proposta de plano de actividades 2022 das listas;
3. Eleições dos Órgãos Sociais 2022/2023;
4. Apresentação do programa da lista vencedora;
5. Alteração Estatutos;
6. Alteração Regulamento Interno;
7. Outros assuntos.

A apresentação das candidaturas deverá ser feita em carta, que deverá dar
entrada na sede social da Associação até dia 5 de Fevereiro de 2022.

Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a
Assembleia Geral reunirá, em segunda convocatória, no mesmo local e passados 30
minutos, com qualquer número de presenças.

Localização: <https://osm.org/go/b8boBVf8T?layers=N&m=>

Coordenadas GPS: geo:41.14378,-8.61263?z=19

<https://fajdp.pt/contactos/>

A FAJDP - Casa das Associações situa-se na zona do centro histórico do Porto.

Data e hora: Sábado, 5 Março, 2022 - 14:00

## Listas candidatas

### Lista A

#### Direcção

* Presidente - Tiago Miguel Feiteiro Carrondo
* Vice-Presidente - Rúben Jaime Alegria Leote Mendes
* Tesoureiro - Octávio Filipe da Mota Pereira Gonçalves
* Secretário - Hugo Miguel Pereira Peixoto
* Vogal - André Isidoro Fernandes Esteves

#### Mesa da Assembleia

* Presidente - Diogo Miguel Constantino dos Santos
* 1° Secretário - Rui Miguel Silva Seabra
* 2° Secretário - Rômulo Luis Corrêa Sellani

#### Conselho Fiscal

* Presidente - Jaime dos Santos Pereira
* 1° Secretário - Sandra Maria Oliveira Esteves Fernandes
* 2° Secretário - Diogo Miguel Conceição Figueira
