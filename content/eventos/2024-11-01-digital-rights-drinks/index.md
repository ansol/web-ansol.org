---
layout: evento
title: Digital Rights Drinks - novembro
metadata:
  event:
    location: Julio's, Avenida Elias Garcia 19B, Lisboa
    date:
      start: 2024-11-01 18:30:00
      finish: 2024-11-01 20:00:00
---

![Cartaz com a informação que também aparece abaixo](cartaz.jpg)

> Digital Rights Drinks: Monthly Meet Up on Human Rights in the Digital Age
>
> subjects: privacy, copyleft, net neutrality, law & tech, open data, open... everything, internet freedom, etc
>
> When: November 1st 6:30PM (Every 1st friday of the month)
>
> Where: Julio's (Avenida Elias Garcia 19B, Lisboa)
