---
categories:
- direitos de autor
- drm
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 96
  - tags_tid: 10
  node_id: 310
  event:
    location: Parlamento Europeu
    site:
      title: ''
      url: https://twitter.com/Senficon/status/595890865329037312
    date:
      start: 2015-06-16 00:00:00.000000000 +01:00
      finish: 2015-06-16 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Voto no JURI das emendas ao relatório Reda
created: 1429223025
date: 2015-04-16
aliases:
- "/evento/310/"
- "/node/310/"
---

