---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 684
  event:
    location: 
    site:
      title: ''
      url: https://www.lisbon.droidcon.com/
    date:
      start: 2019-09-09 00:00:00.000000000 +01:00
      finish: 2019-09-10 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: droidcon Lisbon
created: 1564571291
date: 2019-07-31
aliases:
- "/evento/684/"
- "/node/684/"
---

