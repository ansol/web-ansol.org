---
layout: evento
title: ENEI - Encontro Nacional de Estudantes de Informática
metadata:
  event:
    location: FEUP & ISEP, Porto
    site:
      url: https://eneiconf.pt/
    date:
      start: 2025-04-11
      finish: 2025-04-14
    
---

O Encontro Nacional de Estudantes de Informática regressa à vibrante cidade do **Porto**!

De **11 a 14 de abril**, marca no teu calendário este evento imperdível, que vai juntar estudantes de Informática de todo o país numa celebração única de conhecimento, partilha de experiências e construção de relações.
