---
layout: evento
title: Encontro de Comunidades de Tecnologias Livres
metadata:
  event:
    date:
      start: 2025-02-22 09:00:00
      finish: 2025-02-22 23:00:00
    location: LCD - Laboratório de Criação Digital, Matosinhos, Porto
    site:
      url: https://ectl.pt/
---

![cartaz de dos eventos](cover.jpg)


#### LCD Porto

| **Horário** | **Atividade**                              | **Apresentador(a/s)**           | **Descrição**                                                                                                                                                                                                                                                                                     |
|-------------|--------------------------------------------|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 09:00       | Pequeno Almoço na Pastelaria Santo António |                                 | A 1 min do LCD                                                                                                                                                                                                                                                                                    |
| 09:30       | Recepção                                   |                                 |                                                                                                                                                                                                                                                                                                   |
| 10:00       | Workshop Drupal 1.0                        | António Aragão / Filipe Pereira | Como instalar, configurar e gerar conteúdo                                                                                                                                                                                                                                                        |
| 12:30       | Almoço                                     |                                 |                                                                                                                                                                                                                                                                                                   |
| 14:00       | 1º Grande Wiki Rally da Calçada Portuguesa | WIkimedia Portugal              | Sessão introdutória ao desafio da tarde, e de seguida os participantes irão explorar percursos predefinidos, mapeando, fotografando e registando o máximo de informação sobre a calçada portuguesa existente no percurso, para posterior registo no OSM, Wikidata, Wikimedia Commons e Wikipédia. |
| ??:??       | Encerramento                               |                                 |                                                                                                                                                                                                                                                                                                   |

O evento terminará a hora incerta, em local indeterminado (que é como quem diz, depois do sol se pôr vamos todos para um espaço com internet carregar os dados, e depois conviver enquanto for possível).

---

Irá decorrer no dia 22 de Fevereiro no LCD - Laboratório de Criação Digital, Matosinhos, Porto mais um dos "Encontros de Comunidades de Tecnologias Livres" (ECTL).

Trata-se de uma série de eventos inspirados no maior evento multi-comunidades em Portugal, a Festa do Software Livre, que é organizado por uma das comunidades fundadoras dos ECTL, a [ANSOL](https://ansol.org), e é também inspirado numa iniciativa que tem a intenção de partilha de conhecimento em um makerspace, o Centro Linux, que é também organizado por uma das comunidade fundadoras dos ECTL, a [Ubuntu-pt](https://ubuntu-pt.org) .

A entrada e a participação são livres e gratuitas para todos!
