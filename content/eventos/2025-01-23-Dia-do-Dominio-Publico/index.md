---
layout: evento
title: Celebração do Dia do Domínio Público 2025
metadata:
  event:
    location: Biblioteca Nacional de Portugal, Lisboa
    date:
      start: 2025-01-23 09:30:00
      finish: 2025-01-23 16:00:00
    site:
      url: https://pt.wikimedia.org/wiki/Celebra%C3%A7%C3%A3o_do_Dia_do_Dom%C3%ADnio_P%C3%BAblico_2025_na_BNP
---
O Dia do Domínio Público comemora o momento em que os direitos autorais expiram no primeiro dia de cada ano. Os filmes, fotos, livros e sinfonias cujo termo de direitos autorais terminou tornam-se livres para o uso comum. O fim dos direitos autorais sobre esses trabalhos significa que eles entram no domínio público. Educadores, estudantes, artistas e fãs podem usá-los sem permissão nem pagamento. Os arquivos on-line podem digitalizar e torná-los totalmente disponíveis sem a ameaça de ações judiciais ou exigências de licenciamento.

No primeiro dia do ano, a Wikimédia Portugal, com o apoio da ANSOL, publica a Lista de autores portugueses que entram em domínio público em 2025, com os dados provenientes do wikidata, e a Biblioteca Nacional de Portugal publica igualmente uma listagem de autores, a partir dos registos das suas bases de dados. Com isto, durante o mês de janeiro, convidamos todos os interessados a criar, melhorar e desenvolver as informações sobre os autores que entram em domínio público e as suas obras.

## Programa

### Manhã: Auditório da Biblioteca Nacional:
* 9.30 - 10h: Receção aos participantes;
* 10 - 10.30h: Apresentação "Dia do Domínio Público em Portugal e sua relevância" - Wikimedia Portugal;
* 10.30 - 11h: Apresentação "Licenças abertas Creative Commons" - Connor Benedict;
* 11 - 11.15h: Pausa para café;
* 11.15 - 12h: Apresentação "Digitalização de obras em domínio público - exemplo prático de uma obra de Aristides de Sousa Mendes" - Jorge Pires Gomes;
* 12 - 13h: Questões e debate;
* 13h: Almoço.

### Tarde: Sala de formação da Biblioteca Nacional:
* 14.30 - 16h: Editatona dos autores em domínio público em Wikipédia, Wikidata, Wikisource e Commons: Carregamento de obras digitalizadas, inserção de fotografias dos autores, etc.

## Organização

* [Wikimédia Portugal](https://wikimedia.pt)
* [Associação Nacional para o Software Livre (ANSOL)](https://ansol.org/)
* [Biblioteca Nacional de Portugal](https://www.bnportugal.gov.pt/)
