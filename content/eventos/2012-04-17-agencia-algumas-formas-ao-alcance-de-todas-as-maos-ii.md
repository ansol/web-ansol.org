---
categories: []
metadata:
  node_id: 56
  event:
    location: The Barber Shop, Lisboa
    site:
      title: http://www.thisisthebarbershop-agencia.com/
      url: http://www.thisisthebarbershop-agencia.com/
    date:
      start: 2012-04-05 18:30:00.000000000 +01:00
      finish: 2012-04-05 20:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Agência: Algumas Formas ao Alcance de Todas as Mãos II'
created: 1334620491
date: 2012-04-17
aliases:
- "/evento/56/"
- "/node/56/"
---
<p class="documentDescription" style="margin-top: 0em; margin-right: 0em; margin-bottom: 0.5em; margin-left: 0em; line-height: 1.5em; font-weight: bold; display: block; ">Ciclo de conversas. Nesta edi&ccedil;&atilde;o: &quot;P&uacute;blico, Privado e Comum&quot; por Ricardo Noronha (Unipop) e &quot;Copyright Policies | Free/Open Culture Movements&quot; por Teresa Nobre (Creative Commons)</p>
<div>
	<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0.75em; margin-left: 0px; line-height: 1.5em; "><i>Tun&iacute;sia. Um s&oacute; pa&iacute;s foi suficiente para desencadear uma avalanche de protestos e revolu&ccedil;&otilde;es, do Egipto &agrave; S&iacute;ria, pelo Norte de &Aacute;frica e M&eacute;dio Oriente, com resultados ainda amb&iacute;guos e por definir. Entretanto, as margens a norte do Mediterr&acirc;neo s&atilde;o banhadas pela&nbsp;especula&ccedil;&atilde;o dos mercados financeiros, que sa&iacute;dos&nbsp;da bolha imobili&aacute;ria da d&eacute;cada de 2000 exploram agora a d&iacute;vida externa. Enquanto a Troika (BCE-FMI-CE) volta a ceder empr&eacute;stimos a pa&iacute;ses europeus, a Uni&atilde;o Europeia questiona discretamente a perenidade da zona Euro.&nbsp;A esta din&acirc;mica acrescenta-se o vocabul&aacute;rio da ocupa&ccedil;&atilde;o, agora re-descoberto entre as ruas e o espa&ccedil;o virtual da internet.</i></p>
	<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0.75em; margin-left: 0px; line-height: 1.5em; ">Embora n&atilde;o pretendamos abordar a totalidade e complexidade de 2011, &eacute;-nos dif&iacute;cil sintetizar aqui um ano t&atilde;o intenso sem sentir que se perderia algo da urg&ecirc;ncia que justifica fazer&nbsp;<i>Ag&ecirc;ncia: Algumas Formas ao Alcance de Todas as M&atilde;os</i>,&nbsp;uma&nbsp;segunda vez.&nbsp;Foram os eventos deste mesmo ano que&nbsp;nos convidaram a&nbsp;abrir o campo especulativo da edi&ccedil;&atilde;o anterior,&nbsp;ampliando o campo de ac&ccedil;&atilde;o para temas como a media&ccedil;&atilde;o p&uacute;blica da propriedade intelectual, e a exemplaridade do debate sobre a sua jurisdi&ccedil;&atilde;o para l&aacute; do virtual, bem como a intrus&atilde;o do financeiro no quotidiano.</p>
	<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0.75em; margin-left: 0px; line-height: 1.5em; "><i>Da persegui&ccedil;&atilde;o do Wikileaks ao encerramento do Megaupload, estaremos porventura a testemunhar uma viragem no potencial da internet, quer pelo seu crescente policiamento, quer pela&nbsp;sua capacidade regeneradora e produtora de novos modelos de partilha? No mesmo nano-segundo da transac&ccedil;&atilde;o inform&aacute;tica&nbsp;coexistem a liberdade de partilha como prometida pela internet e a acumula&ccedil;&atilde;o de capital permitida pela financializa&ccedil;&atilde;o.&nbsp;Como poderemos compreender a ciclicidade das crises do capitalismo, ou&nbsp;a topologia das interac&ccedil;&otilde;es de uma sociedade financializada, face aos modelos de feedback propostos pela cibern&eacute;tica estruturais &agrave; economia contempor&acirc;nea? Quest&otilde;es como o regime de risco e de d&iacute;vidocracia, refor&ccedil;ado pela crise europeia, bem como as suas consequ&ecirc;ncias para uma acrescida precariedade laboral e para a mercantiliza&ccedil;&atilde;o da propriedade intelectual, estar&atilde;o em debate. De modo a abordar esta din&acirc;mica ser&aacute; necess&aacute;rio, em &uacute;ltima inst&acirc;ncia, fazer um levantamento das discuss&otilde;es sobre o Comum tidas ao longo da &uacute;ltima d&eacute;cada; qual a sua capacidade de resposta e quais as suas transforma&ccedil;&otilde;es actuais.</i></p>
</div>
<br class="Apple-interchange-newline" />
