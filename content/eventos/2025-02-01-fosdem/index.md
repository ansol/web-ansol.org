---
layout: evento
title: FOSDEM 2025
metadata:
  event:
    location: ULB Campus Solbosch, Bruxelas, Bélgica
    site:
      url: https://fosdem.org/2025/
    date:
      start: 2025-02-01
      finish: 2025-02-02
---
FOSDEM is a free event for software developers to meet, share ideas and
collaborate.

Every year, thousands of developers of free and open source software from all
over the world gather at the event in Brussels.
