---
layout: evento
title: "Oficina de Democracia e Ecologia Tecnológica: primeiro encontro"
metadata:
  event:
    date:
      start: 2024-06-30 17:00:00.000000000 +01:00
      finish: 2024-06-30 18:30:00.000000000 +01:00
    location: "União Setubalense, Av. Luisa Todi 235, Setúbal"
    site:
      url: https://odet.pt
---
![](cartaz.png)

A ODET pretende ser um espaço aberto a pessoas com ou sem conhecimentos de informática, para pensar e agir sobre as tecnologias digitais que utilizamos quotidianamente. Queremos consciencializar para os problemas da *big tech*, a importância do software livre e dos comuns digitais, os direitos digitais, a privacidade e segurança online, etc, e eventualmente fazer alguns projectos, consoante os interesses das pessoas que aparecerem. Haverá também tempo em cada encontro para, com entre-ajuda e o conhecimento do grupo, resolvermos problemas dos nossos computadores, telemóveis e outros dispositivos. 

Para quaisquer dúvidas podem contactar-nos pelo e-mail [odet.setubal@proton.me](mailto:odet.setubal@proton.me).
