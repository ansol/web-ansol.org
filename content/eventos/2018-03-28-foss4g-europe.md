---
categories:
- foss
- opengeo
- congresso europeu
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 160
  - tags_tid: 234
  - tags_tid: 235
  node_id: 561
  event:
    location: Pousada de Guimarães, Guimarães, Portugal
    site:
      title: FOSS4G-Europe site
      url: https://foss4g-europe.osgeopt.pt/
    date:
      start: 2018-07-17 00:00:00.000000000 +01:00
      finish: 2018-07-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: FOSS4G-Europe
created: 1522256936
date: 2018-03-28
aliases:
- "/evento/561/"
- "/node/561/"
---
<p>The FOSS4G-Europe Conference will be held in <a href="https://en.wikipedia.org/wiki/Guimar%C3%A3es" target="_blank" rel="noopener">Guimarães</a>, Portugal, from July 17 to 19th, 2018. Two days for workshops are scheduled. On 16th, just before the conference and on 20th, after the conference. We also support a code sprint for two days, 20 and 21th of July.</p><p>The Conference aims to bring together the European FOSS4G community. It is not an academic neither a business event. It is a community event, to support face to face meetings and discussions, to foster interactions, to share knowledge and passion.</p><p>The Conference is organized by <a href="http://osgeopt.pt/" target="_blank" rel="noopener">Portugal OSGeo Chapter</a>, on behalf of the <a href="https://wiki.osgeo.org/wiki/Europe" target="_blank" rel="noopener">Europe OSGeo Chapter</a>.</p><p>We will be publishing as many information as possible in the next few days. Meanwhile, if you have some urgent question, please email our local team using the address: <a href="mailto:foss4g-europe@osgeopt.pt">foss4g-europe@osgeopt.pt</a>.</p>
