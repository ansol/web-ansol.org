---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 705
  event:
    location: Leiria
    site:
      title: ''
      url: https://devfest.gdgleiria.xyz/
    date:
      start: 2019-12-14 00:00:00.000000000 +00:00
      finish: 2019-12-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: DevFest Leiria 2019
created: 1572646726
date: 2019-11-01
aliases:
- "/evento/705/"
- "/node/705/"
---

