---
categories:
- hacker
- camp
- denmark
- floss
- makers
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 137
  - tags_tid: 254
  - tags_tid: 283
  - tags_tid: 273
  - tags_tid: 115
  node_id: 596
  event:
    location: ilha de Bornholm, Dinamarca
    site:
      title: Bornhack2018
      url: https://bornhack.dk/bornhack-2018/
    date:
      start: 2018-08-16 00:00:00.000000000 +01:00
      finish: 2018-08-23 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: BornHack 2018
created: 1522934454
date: 2018-04-05
aliases:
- "/evento/596/"
- "/node/596/"
---
<p>BornHack is a 7 day outdoor tent camp where hackers, makers and people with an interest in technology or security come together to celebrate technology, socialise, learn and have fun.<br>Bornhack 2018 will be the third BornHack. It will take place from August 16th to August 23rd 2018 on the Danish island of Bornholm.<br><br>The BornHack team looks forward to organising another great event for the hacker community. We still need volunteers, so please let us know if you want to help!<br>We want to encourage hackers, makers, politicians, activists, developers, artists, sysadmins, engineers with something to say to read our call for speakers.<br><br>BornHack aims to keep ticket prices affordable for everyone and to that end we need sponsors. Please see our call for sponsors if you want to sponsor us, or if you work for a company you think might be able to help.<br><br>You are very welcome to ask questions and show your interest on our different channels:<br><br>&nbsp;&nbsp;&nbsp; IRC: #bornhack @ 6nbtgccn5nbcodn3.onion or irc.baconsvin.org (TLS port 6697)<br>&nbsp;&nbsp;&nbsp; Email: info@bornhack.dk<br>&nbsp;&nbsp;&nbsp; Twitter: @BornHax<br>&nbsp;&nbsp;&nbsp; Facebook: @BornHax<br>&nbsp;&nbsp;&nbsp; Instagram: @BornHax<br>&nbsp;&nbsp;&nbsp; Flickr: @BornHax<br><br>Happy organisers welcoming people at the entrance to BornHack 2016 A bus full of hackers arrive at BornHack 2016 Late night hacking at Baconsvin village at BornHack 2016 #irl_bar by night at BornHack 2016 Soldering the BornHack 2016 badge Colored lights at night BornHack Colored light in the grass Working on decorations Sitting around the campfire at BornHack 2016<br><br></p>
