---
layout: evento
title: Festival dos Direitos Digitais 2024
metadata:
  event:
    location: Casa das Histórias Paula Rego
    site:
      url: https://digitalrightsfestival.com/pt
    date:
      start: 2024-12-09 08:30:00
      finish: 2024-12-09 17:45:00
---

![Cartaz Digital Rights Festival](cover.jpeg)

Um festival criado espontaneamente por cidadãos e apoiado pela NOVA School of Law.

Este é um Festival onde reguladores, defensores dos direitos civis, líderes empresariais e cientistas se sentam juntos em debates civis.

Com a implementação do DSA e do AI Act em curso, este é um evento que não vai querer perder.
