---
layout: evento
title: Drupal Iberia 2025 Cáceres
metadata:
  event:
    location: Complejo Cultural San Francisco, Ronda de San Francisco 15, 10002 Cáceres Cáceres, Espanha
    site:
      url: https://www.drupal.org/community/events/drupal-iberia-2025-2025-04-04
    date:
      start: 2025-04-04
      finish: 2025-04-05
---

Drupal Iberia é uma conferência de dois dias realizada na nossa península, organizada pelas associações Drupal espanhola e portuguesa.
Reúne programadores, designers, potenciais clientes, potenciais parceiros e outros profissionais interessados ​​em Drupal para partilhar conhecimento, colaborar em projetos, aprender sobre as últimas tendências e avanços em Drupal e fazer networking.
Convidamo-lo a Cáceres, uma encantadora cidade espanhola onde muralhas antigas, ruas de paralelepípedos e arquitetura medieval transportam os visitantes de volta ao passado.

As sessões serão em inglês, teremos eventos sociais agradáveis, atividades turísticas, boa comida e muita alegria.

Vemo-nos em Cáceres!!
