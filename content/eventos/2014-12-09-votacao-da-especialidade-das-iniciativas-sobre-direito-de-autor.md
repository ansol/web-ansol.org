---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 252
  event:
    location: Parlamento
    site:
      title: ''
      url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d52793942636e463161585a765132397461584e7a5957387654334a6b5a57357a4947526c4946527959574a68624768764c304e425130524d52313878587a49334e5335775a47593d&Fich=CACDLG_1_275.pdf&Inline=true
    date:
      start: 2014-12-10 10:00:00.000000000 +00:00
      finish: 2014-12-10 12:00:00.000000000 +00:00
    map: {}
layout: evento
title: Votação da Especialidade das Iniciativas sobre Direito de Autor
created: 1418124967
date: 2014-12-09
aliases:
- "/evento/252/"
- "/node/252/"
---
<p>Nesta reunião da 1ª Comissão, decorrerá a discussão e votação na especialidade das seguintes iniciativas legislativas:</p><ul><li>Proposta de Lei n.º 245/XII/3.ª (GOV) - Regula as entidades de gestão coletiva do direito de autor e dos direitos conexos, inclusive quanto ao estabelecimento em território nacional e à livre prestação de serviços das entidades previamente estabelecidas noutro Estado-Membro da União Europeia ou do Espaço Económico Europeu;</li><li>Proposta de Lei n.º 246/XII/3.ª (GOV) - Procede à segunda alteração à Lei n.º 62/98, de 1 de setembro, que regula o disposto no artigo 82.º do Código do Direito de Autor e dos Direitos Conexos, sobre a compensação equitativa relativa à cópia privada;</li><li>Proposta de Lei n.º 247/XII/3.ª (GOV) - Transpõe a Diretiva n.º 2012/28/UE, do Parlamento Europeu e do Conselho, de 25 de outubro, relativa a determinadas utilizações permitidas de obras órfãs, e procede à décima alteração ao Código do Direito de Autor e dos Direitos Conexos, aprovado pelo Decreto-Lei n.º 63/85, de 14 de março</li></ul>
