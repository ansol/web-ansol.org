---
layout: evento
title: LisboaJS 2 - SQL from the frontend?
metadata:
  event:
    location: Fintech House · Lisbon
    site:
      url: https://lu.ma/c78icq9o
    date:
      start: 2024-12-12 18:30:00
      finish: 2024-12-12 21:30:00
---

![Cartaz](cartaz.png)

Please [sign up here](https://lu.ma/c78icq9o) to join the event, seats are limited.

Is SQL from the client viable? David has worked on apps with a “SQL from the client” architecture and the development velocity was simply superb. At the same time, it comes with a lot of challenges in terms of security and scalability. So, what’s the deal? Is it viable or not?
The tl;dr is that the technology is perhaps not ready yet, but it makes a lot of sense for a large swath of apps – especially prototypes. But this is a complex topic, and he'll go deeper into the security problems in this talk.

LisboaJS partnered with multiple restaurants across town and booked tables for 6 people after the talks, so you can get to know the speakers, their team and other developers, and have more meaningful conversations that go beyond programming.

AGENDA:

* 18:30 - Doors open: registration. Snacks & Board games.
* 19:05 - Intro to the LisboaJS Community
* 19:15 - David Gomes (Lead Engineer @Neon) - SQL from the Frontend
* 19:40 - Quick break
* 19:50 - Q&A + feedback
* 21:00 - Developer Dinners. You'll be matched with 5 other developers
