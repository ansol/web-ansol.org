---
layout: evento
title: Encontro de Comunidades de Tecnologias Livres
metadata:
  event:
    date:
      start: 2025-02-20 19:00:00
      finish: 2025-02-20 21:00:00
    location: Innovation Hub, Matosinhos, Porto
    site:
      url: https://ectl.pt/
---

![cartaz de anúncio da data do evento](cover.jpg)


#### Innovation Hub

| **Horário** | **Atividade**                                                                         | **Apresentador(a/s)** | **Descrição**                                                                                                      |
|-------------|---------------------------------------------------------------------------------------|-----------------------|--------------------------------------------------------------------------------------------------------------------|
| 19:00       | Recepção                                                                              |                       |                                                                                                                    |
| 19:30       | Drupal CMS 1.0                                                                        |                       | O que é o Drupal CMS ? Qual a diferença entre o Drupal Core e Drupal CMS ? A quem é direccionado ? Como funciona ? |
| 20:00       | Explore o mundo num mapa: uma rede social inovadora e em código livre helpbuttons.org | A. Jeremias           |                                                                                                                    |
| 20:30       | Sessão de Encerramento                                                                |                       |                                                                                                                    |

---

Irá decorrer no dia 20 de Fevereiro no Innovation Hub Porto mais um dos "Encontros de Comunidades de Tecnologias Livres" (ECTL).

Trata-se de uma série de eventos inspirados no maior evento multi-comunidades em Portugal, a Festa do Software Livre, que é organizado por uma das comunidades fundadoras dos ECTL, a [ANSOL](https://ansol.org), e é também inspirado numa iniciativa que tem a intenção de partilha de conhecimento em um makerspace, o Centro Linux, que é também organizado por uma das comunidade fundadoras dos ECTL, a [Ubuntu-pt](https://ubuntu-pt.org) .

A entrada e a participação são livres e gratuitas para todos!
