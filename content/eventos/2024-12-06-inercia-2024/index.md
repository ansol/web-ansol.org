---
layout: evento
title: Inércia 2024 - Festival de Arte Digital
metadata:
  event:
    location: Salão de Festas da Sociedade Filarmónica Incrível Almadense, Almada
    site:
      url: https://2024.inercia.pt/pt/
    date:
      start: 2024-12-06 13:37:00
      finish: 2024-12-08 20:00:00
---

![Cartaz com a informação do evento](cartaz.png)

Inércia 2024, a 16ª edição deste evento, terá lugar de 6 a 8 de Dezembro 2024 no Salão de Festas Incrível Almadense, uma sala centenária no coração de Almada, do outro lado do rio de Lisboa.

Este salão mítico oferece-nos espaço para até 150 visitantes (incluindo lugares dedicados em mesas com assentos confortáveis para até 50 pessoas). O evento decorre de sexta-feira à tarde até domingo de manhã, durante o qual haverá muitas oportunidades para apresentar o seu trabalho e conviver com outros artistas, demosceners e criadores.
