---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAD/OwA0BUsiwASp9lIDXENA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38718851442756e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9146524071721e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9146524071721e1
    mapa_top: !ruby/object:BigDecimal 27:0.38718851442756e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9146524071721e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38718851442756e2
    mapa_geohash: eyckrbe9wt136stm
  slide:
  - slide_value: 0
  node_id: 642
  event:
    location: Lisboa, Cineteatro Capitólio
    site:
      title: ''
      url: https://summit.creativecommons.org/
    date:
      start: 2019-05-09 00:00:00.000000000 +01:00
      finish: 2019-05-11 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Creative Commons Global Summit 2019
created: 1545851947
date: 2018-12-26
aliases:
- "/ccgs-2019/"
- "/evento/642/"
- "/node/642/"
---
<h3 style="text-align: center;"><strong><a href="https://summit.creativecommons.org/register2019/">Registration is now open!</a>&nbsp;</strong></h3><p><span>Join us for three days of dynamic programming at Museu do Oriente, with&nbsp;a special keynote evening event held&nbsp;at the historic Cineteatro Capitolio.</span></p><p>We’ve grown the CC Global Summit every year as hundreds of leading activists, advocates, librarians, educators, lawyers, technologists, and more have joined us for discussion and debate, workshops and planning, talks and community building. It’s a can’t-miss event for anyone interested in the global movement for the commons.</p><p>All attendees, speakers, sponsors and volunteers at our conference are expected to adhere to our&nbsp;<a href="https://wiki.creativecommons.org/images/a/a4/The_CC_Code_of_Conduct_%28GS%29.pdf" target="_blank" rel="noopener">Code of Conduct&nbsp;</a>to help ensure a safe environment for everyone.</p>
