---
layout: evento
title: I Love Free Software Day 2025
metadata:
  event:
    location: online
    date:
      start: 2025-02-14 22:00:00
      finish: 2025-02-14 23:00:00
---

Este ano a ANSOL celebra o [dia do amor ao Software Livre](https://fsfe.org/activities/ilovefs/) festejando não os projectos mas sim a comunidade. Assim, vamos fazer uma "festa-convívio" virtual, um encontro sem palestras ou regras, em que nos juntamos todos virtualmente para celebrar a nossa paixão pelo Software Livre.

**O encontro vai decorrer na seguinte sala Jitsi: [https://jitsi.ansol.org/ilovefs](https://jitsi.ansol.org/ilovefs)**

[Código de Conduta](https://ansol.org/coc-online/)
