---
layout: evento
title: "Geek Girls Portugal Conference 2025"
metadata:
  event:
    date:
      start: 2025-04-05
      finish: 2025-04-05
    location: Porto Business School
    site:
      url: https://conference.geekgirlsportugal.pt/
---


On April 5, we will once again host the conference solely aimed at empowering women in the tech industry. And you know what we definitely need to make this a truly successful event for all? YOU.

That's why today, we open our call for speakers!
We are looking forward to receiving your submissions for talks or workshops on tech topics that you love and are eager to share with our community.

Go to this form to apply your talk or workshop to our conference: https://forms.gle/ieTk86usuPp328A87

We need your voice on our stage. Speak up! 📣

#GeekGirlsPortugal #G2PTConference2025 #womenintech #tech #conference

![Cartaz do evento, com a mensagem save the date](cover.jpg)
