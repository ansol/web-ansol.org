---
layout: evento
title: Dia do Software Livre 2024
metadata:
  event:
    date:
      start: 2024-09-21 16:00:00
      finish: 2024-09-21 23:00:00
    location: Cafetaria & Gelataria Mozart, Lamego
    site:
      url: https://digitalfreedoms.org/sfd/events/dia-do-software-livre-2024
---
Um encontro-convívio, uma celebração: este ano a celebração do Dia do Software Livre da ANSOL vai acontecer em Lamego.

Apesar da chuva que se prevê para o início da tarde, há muito como celebrar o Dia do Sofware Livre. Depois do encontro na Cafetaria & Gelataria Mozart, localizada a dois minutos a pé da Sé Catedral de Lamego, os participantes serão desafiados a fazer uso das novas medidas referentes às entradas gratuitas em museus, visitando o Museu de Lamego, onde entre outras coisas se encontra um dos monumentos que vai estar em concurso no "Wiki Loves Monuments", em Outubro. Prevê-se melhor tempo após a visita ao museu, pelo que se propõe um pequeno passeio e um exercício de mapeamento, contribuindo com dados para o OpenStreetMap, Wikimedia ou outros, tal como explorar um pouco da cidade. No final da tarde, os presentes voltar-se-ão a reunir para um jantar festivo, oferta da Wikimedia Portugal.

## Agenda
* 16:00 - encontro na Cafetaria & Gelataria Mozart
* 16:30 - visita gratuita ao Museu de Lamego
* 18:00 - actividades de contribuição
* 20:00 - Jantar, oferta da Wikimedia Portugal

## Mais detalhes

* **Deslocação:** As actividades vão decorrer na zona da cidade perto da central de camionagem. A circulação na Linha do Douro pode estar a ser afectada pelos incêndios. Para quem se desloca em viatura própria, Lamego é servida pela A24. Há um parque de estacionamento no largo do Turismo, perto da zona de actividades.
* **Ponto de Encontro:** Cafetaria & Gelataria Mozart, a dois minutos a pé da Sé Catedral de Lamego
* **Coordenação online e mais informação:** Matrix - #geral:ansol.org; telegram - https://t.me/ansolgeral
* **Código de Conduta:** https://digitalfreedoms.org/en/documentation/coc
