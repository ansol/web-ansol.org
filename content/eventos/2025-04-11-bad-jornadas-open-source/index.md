---
layout: evento
title: 5as Jornadas Open Source
metadata:
  event:
    location: Universidade Portucalense Infante D. Henrique, Porto
    site:
      url: https://eventos.bad.pt/event/5a-jornadas-open-source/
    date:
      start: 2025-04-11 09:00:00
      finish: 2025-04-11 18:00:00
    
---

![Cartaz](cartaz.png)

## Open Source e Inteligência Artificial na gestão da informação do conhecimento 

As **5as Jornadas Open Source** vão ter lugar na Aula Magna da [Universidade Portucalense Infante D. Henrique](https://www.upt.pt/), no **Porto**, a **11 de abril de 2025**. Trata-se de uma organização conjunta entre as Delegações [Norte](https://bad.pt/organizacao/delegacoes-regionais/delegacao-norte/) e [Centro](https://bad.pt/organizacao/delegacoes-regionais/delegacao-centro/) da BAD e a Universidade Portucalense.


Com o avanço das tecnologias de Inteligência Artificial (IA) e o uso recorrente das ferramentas Open Source procuramos responder à questão: de que forma podem estas duas dimensões coexistir?
 

As **5as Jornadas Open Source** terão como foco debater sobre as tecnologias Open Source e a Inteligência Artificial e de que forma estas podem coexistir na gestão da informação e do conhecimento em Arquivos, Bibliotecas, Museus e Centros de Documentação. Pretende-se também refletir sobre a realidade atual, as principais preocupações e desafios e, em simultâneo, partilhar experiências e boas práticas nestes domínios.

