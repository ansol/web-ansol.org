---
layout: evento
title: 1º Encontro de Tecnologia Popular de Setúbal
metadata:
  event:
    date:
      start: 2025-03-15 11:00:00.000000000 +01:00
      finish: 2025-03-16 18:30:00.000000000 +01:00
    location: União Setubalense
    site:
      url: https://odet.pt/festival/2025/
---

![Cartaz (frente)](cartaz.png)![Cartaz (verso), com o texto que também se encontra nesta página do evento](verso.png)

conversas / debates / filme / oficinas / bancas / convívio

Dois dias para descobrir como libertar a tecnologia do sistema económico capitalista e pô-la ao serviço do povo. Vem aprender a usar alternativas, proteger a tua privacidade e reparar dispositivos digitais e outros objectos.

### sábado 15 março

* **11h** Porquê um Festival de Tecnologia Popular? *— Sessão de Abertura*
* **11h-18h30** Oficina de privacidade digital *— Coletivos.org e outras pessoas voluntárias*
* **11h-18h30** Oficina de experimentação e instalação de programas livres *— Centro Linux, ODET e outras pessoas voluntárias*
* **12h30-14h30** Almoço e convívio
* **15h-16h30** Conversa-debate: Tecnologia, género e sociedade *— Beatriz Maciel (programadora) e Rute Correia (Wiki Editoras LX)*
* **17h-19h** Conversa-debate: O que é o capitalismo digital? *— Xullaji (Peles Negras Máscaras Negras) e João Gabriel Ribeiro (Shifter)*
* **21h** Filme e debate: The Internet's Own Boy

### domingo 16 março

* **11h-12h30** Oficina criativa: mapeando a tecnologia que usamos! *— CIDAC e Ana Luísa Silva (Oficina Global)*
* **11h-18h30** Oficina de reparação (repair café) *— Arranja-me um café e outras pessoas voluntárias*
* **12h30-14h30** Almoço e convívio
* **15h00-16h30** Conversa-debate: A transição energética é democrática? *— Gilherme Luz* 
* **17h00-18h30** Conversa-debate: A internet está a trazer a censura de volta? *— Dani Bento*

Entrada livre. Não é necessário ter conhecimentos prévios sobre tecnologia para participar.

**Local:** Av. Luisa Todi, 25, Setúbal

**Como o apoio de:** CIDAC, O.D.E.T., União Setubalense, Camões, I.P.
