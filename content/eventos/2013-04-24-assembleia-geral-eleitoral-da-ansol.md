---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 144
  event:
    location: Espaço 113, Matosinhos
    site:
      title: 
      url: 
    date:
      start: 2013-06-29 14:30:00.000000000 +01:00
      finish: 2013-06-29 14:30:00.000000000 +01:00
    map: {}
layout: evento
title: Assembleia Geral Eleitoral da ANSOL
created: 1366837612
date: 2013-04-24
aliases:
- "/AG2013/"
- "/evento/144/"
- "/node/144/"
---
<p>Consultar a ACTA desta AG <a href="https://ansol.org/AG2013/acta">aqui</a>.</p><p>Convocam-se todos os sócios para a Assembleia Eleitoral que terá lugar no dia 29 de Junho de 2013 pelas 14 horas e 30 minutos no <a href="http://indicium.dyndns.org/~centoetreze/Inicio.html">Espaço 113</a>, Porto, com a seguinte ordem de trabalhos:</p><ol><li>Eleição dos Orgãos Sociais 2013/2014;</li><li>Apresentação do programa da lista vencedora;</li><li>Outros assuntos.</li></ol><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p>A lista dos sócios efectivos no pleno gozo dos seus direitos sociais pode ser vista em <a href="https://ansol.org/socios/lista">https://ansol.org/socios/lista</a> , e uma indicação dos cargos que são actualmente exercidos pode ser vista em <a href="https://ansol.org/contato">https://ansol.org/contato</a> .</p><p>A apresentação das candidaturas deverá ser feita em carta, que deverá dar entrada na sede social da Associação até dia 30 de Maio.</p><p>Morada da sede social da Associação:</p><p>ANSOL - Associação Nacional para o Software Livre <br>Rua Jorge Barradas, N. 18 - 9. Dto <br>1500-370 Lisboa <br>Portugal</p><p>Como chegar ao Espaço 113:</p><p>O Espaço 113 situa-se na zona residencial do Monte dos Burgos, próximo de escolas do 1º ciclo ao secundário. <br>O acesso pode ser feito através da Rua de Diu ou pela Rua da Cooperação. É servido por dois autocarros dos STCP: a linha 508 (Boavista - Cabo do Mundo) e a linha 602 (Cordoaria - Aeroporto).</p><p><br>Espaço 113 <br>Praceta da Concórdia, 113 <br>4465-092 São Mamede de Infesta <br>Matosinhos</p><p>Coordenadas GPS: 41.181527 , -8.630244</p><p>Mapa: <a href="http://indicium.dyndns.org/~centoetreze/Contactos.html">http://indicium.dyndns.org/~centoetreze/Contactos.html</a></p><p>O 2º Secretário da Mesa da Assembleia, <br>Nuno Dantas</p>
