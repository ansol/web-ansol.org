---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 410
  event:
    location: 
    site:
      title: 
      url: 
    date:
      start: 2016-04-06 17:30:00.000000000 +01:00
      finish: 2016-04-06 17:30:00.000000000 +01:00
    map: {}
layout: evento
title: Reunião Aberta sobre as opções políticas e legislativas para a partilha de
  dados
created: 1459769085
date: 2016-04-04
aliases:
- "/evento/410/"
- "/node/410/"
---
<p>A ANSOL irá estar presente na "Reunião Aberta sobre as opções políticas e legislativas para a partilha de dados", a decorrer na Assembleia da República.</p>
