---
layout: evento
title: Party like a Noble Numbat
showcover: false
metadata:
  event:
    date:
      start: 2024-04-27 11:30:00.000000000 +01:00
      finish: 2024-04-27 19:00:00.000000000 +01:00
    location: Centro Linux
    site:
      url: https://centrolinux.pt/post/2024-abril-nobleparty/
---
[![Cartaz](cartaz_noble.png)](https://centrolinux.pt/post/2024-abril-nobleparty/)

# Ubuntu Noble Numbat release party
 
O Ubuntu é a distribuição de GNU/Linux mais popular. Baseado em Debian e com contribuições da comunidade e da Canonical, é muito estável e amigável a novos utilizadores de GNU/Linux.

Neste evento vamos celebrar mais um lançamento de uma nova versão de Ubuntu, o Ubuntu 24.04 Noble Numbat, e vamos também ter a oportunidade de instalar e experimentar esta versão de Ubuntu e dos vários sabores criados pela comunidade.

**O evento é de participação gratuita, mas a inscrição é obrigatória**.

Detalhes adicionais e link para o formulário de inscrição estão publicados [na página do evento](https://centrolinux.pt/post/2024-abril-nobleparty/), no [sítio web do Centro Linux](https://centrolinux.pt).

# Quando?

No Sábado dia 27 de Abril de 2024 a partir das 11:30

## Local:

No [Centro Linux/Makers In Little Lisbon](https://centrolinux.pt/ondeestamos/).
