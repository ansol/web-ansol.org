---
layout: evento
title: JNation 2025
showcover: false
metadata:
  event:
    date:
      start: 2025-05-27 08:30:00.000000000 +01:00
      finish: 2025-05-28 17:30:00.000000000 +01:00
    location: Convento São Francisco, Coimbra
    site:
      url: https://jnation.pt/
---
[![JNation 2025 - banner](banner.jpeg)](https://jnation.pt/)

JNation is an inclusive developer conference covering various topics relevant to the software development industry.

Attendees have the unique opportunity to get up to speed with the latest and greatest technology trends in programming languages like Java and Javascript, Cloud & Infrastructures, Machine Learning and Artificial Intelligence, or Security and Methodologies.

We bring the actual technology experts working in these areas, so attendees have the chance to learn with the best!
