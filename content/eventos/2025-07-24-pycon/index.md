---
layout: evento
title: PyCon Portugal 2025
metadata:
  event:
    location: Nova SBE Cascais
    date:
      start: 2025-07-24 09:00:00
      finish: 2025-07-26 18:00:00
    site:
      url: https://2025.pycon.pt/
---

![banner: PyCon Portugal 2025, Nova SBE Cascais, July 24-26](banner.png)

*Evento em Inglês.*

# PyCon Portugal 2025 July 24th – 26th, 2025

Whether you are an experenced programmer, a hobby hacker or an absolute beginner, we'd love to welcome you to the Python community. PyCons are hosted all around the world by volunteers from local Python communities.

All tickets include lunch, a morning snack, and an afternoon snack.

This is the 4th edition ever of PyCon Portugal - join us !
