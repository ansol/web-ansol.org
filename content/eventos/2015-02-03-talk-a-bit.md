---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 279
  event:
    location: Porto
    site:
      title: ''
      url: http://talkabit.org/
    date:
      start: 2015-02-07 00:00:00.000000000 +00:00
      finish: 2015-02-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Talk A Bit
created: 1422959653
date: 2015-02-03
aliases:
- "/evento/279/"
- "/node/279/"
---
<p>14:15 Opening <br>Ademar Aguiar</p><p>14:30 The process of a company creation | Processo de criação de uma empresa <br>Miguel Gonçalves</p><p>14:55 The impact of technology in daily life | O impacto da tecnologia no quotidiano <br>Sonae - Joel Silva</p><p>15:15 The impact of consulting in informatic chaos | O impacto da consultoria no caos informático <br>Deloitte - Nuno Cordeiro</p><p>15:35 How to sell an IT company? | Como vender uma empresa tecnológica? <br>Vitor Dinis</p><p>15:50 Break</p><p>16:05 The impact of technology in daily life - A psychological perspective | O impacto da tecnologia no quotidiano - Uma perspetiva psicológica <br>Clara Soares</p><p>16:25 Coding by kids | Programação para miúdos <br>Fernanda Ledesma</p><p>16:45 Porto, a city of the future | Porto, uma cidade do futuro <br>Filipe Araújo</p><p>17:05 Creative Computing | Computação criativa <br>Guilherme Martins</p><p>17:20 Coffee break</p><p>17:50 Copyright law | Lei da cópia privada <br>Pedro Veiga</p><p>18:10 Information chaos in health | Informação caótica na saúde <br>Ricardo Correia</p><p>18:25 Final break</p><p>18:50 Humor &amp; Technology | O humor e a tecnologia <br>Fernando Alvim</p><p>19:05 Social program</p><p>19:20 Closing <br>Raul Moreira Vidal</p>
