---
layout: evento
title: Exploração do Home Assistant
metadata:
  event:
    date:
      start: 2023-06-10 11:30:00.000000000 +01:00
      finish: 2023-06-10 19:00:00.000000000 +01:00
    location: Centro Linux, MILL - Makers In Little Lisbon, Lisboa
    site:
      url: https://centrolinux.pt/post/2023-junho-home-assistant/
---


[![Cartaz](https://centrolinux.pt/images/2023.06-Explorar-Home-Assistant/post.base.png )](https://centrolinux.pt/post/2023-junho-home-assistant/)


# Exploração colectiva do Home Assistant

De acordo com a [Wikipédia o Home Assistant](https://en.wikipedia.org/wiki/Home_Assistant) e um sistema de controlo central para dispositivos de casa inteligente/domótica, com um foco no controlo local (permitindo evitar cloud ou serviços de terceiros) e em privacidade.

Neste evento vamos como comunidade explorar o Home assistant para aprendermos a instalar e utilizar para tornarmos as nossas casas mais cómodas, mais acessíveis, mais divertidas e até explorar oportunidades baixar alguns custos como os de utilização da electricidade. Para isto alguns membros mais experientes da comunidade vêm partilhar connosco o seu conhecimento e experiências e vamos todos ter oportunidade de ter contacto directo com o Home Assistant e fazer algumas experiências para sairmos do Centro Linux com algum conhecimento elementar e muita inspiração sobre o que fazermos quando chegarmos a casa.

## Quando?

No **Sábado dia 10 de Junho de 2023 a partir das 11:30**
Haverá interrupção das actividades para almoço.

## Queres saber mais?

Todos os detalhes podem ser encontrados na [página oficial do evento](https://centrolinux.pt/post/2023-junho-home-assistant/).
