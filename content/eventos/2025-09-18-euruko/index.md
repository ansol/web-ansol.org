---
layout: evento
title: "EuRuKo (European Ruby Conference)"
metadata:
  event:
    date:
      start: 2025-09-18
      finish: 2025-09-19
    location: Viana do Castelo
    site:
      url: https://2025.euruko.org/
---

![Cartaz com subtítulo, em fundo roxo e logótipo com nome estilizado comm filigrana](cartaz.jpg)

# THE HE**ART** OF CODE

EuRuKo (European Ruby Conference) is an annual conference about the Ruby programming language with an informal atmosphere and lots of opportunities to listen, learn, and talk with developers from all around the world.

