---
layout: evento
title: Festa de lançamento do Debian Bookworm
metadata:
  event:
    date:
      start: 2023-06-17 12:00:00.000000000 +01:00
      finish: 2023-06-17 18:00:00.000000000 +01:00
    location: Restaurante Típico do Mezio, Castro Daire
    site:
      url: https://wiki.debian.org/ReleasePartyBookworm/Portugal/Lamego
---

Para celebrar o lançamento da mais recente versão de Debian (Bookworm) vamo-nos encontrar para um almoço convívio tradicional, num restaurante regional (famoso por servir o melhor arroz de salpicão do mundo).

Já fazia muito tempo que não havia um encontro de Software Livre no distrito de Viseu, e no próximo dia 17 o encontro é no [Restaurante Tipico do Mezio](http://aesmontemuro.pt/) ([mapa](https://www.openstreetmap.org/#map=19/40.97968/-7.88668)), seguido de uma visita ao Centro de Artesanato do Mezio para quem tiver interesse.

Apesar do prato mais típico neste restaurante ser o arroz de salpicão, o menu é variado e apresenta mais opções. Inclui prato vegetariano (pataniscas vegetarianas com arroz de feijão). O bilhete para o Centro de Artesanato é de 1€ por pessoa.

Para que possamos melhor organizar a refeição (e, em particular, saber com quantas pessoas contar), agradece-se confirmação a quem contar em ir, enviando um e-mail para marcos.marado(at)ansol.org.
