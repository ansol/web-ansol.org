---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 425
  event:
    location: Coimbra
    site:
      title: ''
      url: http://barcamppt.org/index.php/BarCamp_PT_X
    date:
      start: 2016-09-03 00:00:00.000000000 +01:00
      finish: 2016-09-03 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Barcamp PT X
created: 1464545774
date: 2016-05-29
aliases:
- "/evento/425/"
- "/node/425/"
---
<p>Exactamente 10 anos depois do primeiro, o BarCamp PT volta a acontecer em Coimbra.</p>
