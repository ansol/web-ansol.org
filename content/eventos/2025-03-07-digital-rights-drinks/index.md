---
layout: evento
title: Digital Rights Drinks - março
metadata:
  event:
    location: Julio's, Avenida Elias Garcia 19B, Lisboa
    date:
      start: 2025-03-07 18:30:00
      finish: 2025-03-07 20:00:00
---

![Cartaz com a informação destes encontros](cartaz.png)

> Digital Rights Drinks: Monthly Meet Up on Human Rights in the Digital Age
>
> subjects: privacy, copyleft, net neutrality, law & tech, open data, open... everything, internet freedom, etc
>
> When: Every 1st friday of the month, 6:30PM
>
> Where: Julio's (Avenida Elias Garcia 19B, Lisboa)
