---
layout: evento
title: "Como defender-me na internet?"
metadata:
  event:
    date:
      start: 2024-12-16 18:30:00.000000000 +00:00
      finish: 2024-12-16 20:30:00.000000000 +00:00
    location: "União Setubalense, Av. Luisa Todi 235, Setúbal"
    site:
      url: https://odet.pt/como-defender-me-na-internet/
---
![](cover.jpeg)

Workshop prático e gratuito sobre privacidade e segurança digital. Vem aprender como impedir que os teus dispositivos tecnológicos te espiem, como tomar posse dos teus dados e detectar ameaças.

Para quaisquer dúvidas podem contactar-nos pelo e-mail [odet.setubal@proton.me](mailto:odet.setubal@proton.me).
