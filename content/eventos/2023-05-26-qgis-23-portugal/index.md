---
layout: evento
title: 4º Encontro De Utilizadores QGIS Portugal
metadata:
  event:
    date:
      start: 2023-05-26
      finish: 2023-05-26
    location: Évora
    site:
      url: https://www.qgis.pt/encontro-utilizadores-qgis-2023/
---

> O 4º Encontro de Utilizadores QGIS Portugal é um encontro informal, que tem
> como tema central o uso do software SIG Open Source QGIS e irá realizar-se no
> dia 26 de Maio de 2023, no NÚCLEO EMPRESARIAL DA REGIÃO DE ÉVORA, em Évora.

> A manhã do dia 26 de Maio será destinada a apresentações, com uma duração
> aproximada de 20 (15+5) minutos. Assim, até ao próximo dia 25 de Maio,
> encontra-se aberto o período para inscrição.

> Durante a tarde do dia 26 de Maio serão ministrados três workshops
> demonstrativos, com a duração aproximada de 3 horas. Devido ao tempo limitado
> de cada workshop os mesmos serão puramente demonstrativos, não podendo ser
> encarados como formação. As vagas nos workshops são limitadas e portanto o
> critério de atribuição será a ordem de inscrição, sendo que serão criadas
> listas de espera caso haja desistências.
