---
title: Licenças e Atribuições
---

## Geral

O código deste website está disponível em [https://gitlab.com/ansol/web-ansol.org](https://gitlab.com/ansol/web-ansol.org), sob a licença [AGPLv3](https://gitlab.com/ansol/web-ansol.org/-/blob/master/LICENSE).

O tema do website foi baseado no [Academic](https://themes.gohugo.io/themes/hugo-academic/), disponível sob a [licença MIT](https://github.com/wowchemy/wowchemy-hugo-themes/blob/main/LICENSE.md)

Utilizamos as seguintes bibliotecas:
- [fuse.js](https://fusejs.io), disponível sob a [licença Apache 2.0](https://github.com/krisk/Fuse/blob/master/LICENSE)
- [mark.js](https://markjs.io), disponível sob a [licença MIT](https://github.com/julmot/mark.js/blob/master/LICENSE)
- [hugo-peertube-shortcode](https://codeberg.org/_aris/hugo-peertube-shortcode), disponível sob a [licença MIT](https://codeberg.org/_aris/hugo-peertube-shortcode/src/commit/10197c1b4bc15c375d51a5c9da76c6b2ee7055da/LICENSE)


## Ícones

["Search"](https://thenounproject.com/term/search/14173/), criado por Egor Culcea, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode).

["menu"](https://thenounproject.com/term/menu/45019/), criado por Kervin Markle, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode).

["report"](https://thenounproject.com/term/report/3889480/), criado por HideMaru, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode).


["campaign"](https://thenounproject.com/term/campaign/564798/) criado por romzicon, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.

["Newspaper"](https://thenounproject.com/term/news/1015824/) criado por Gregor Cresnar, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.

["event"](https://thenounproject.com/term/event/1445143/) criado por jngll, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.

["conference"](https://thenounproject.com/term/conference/4317456/) criado por  Zulfa Mahendra, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.


["Volunteer"](https://thenounproject.com/term/volunteer/4317084/) criado por Zulfa Mahendra, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.

["europe"](https://thenounproject.com/term/europe/2430585/) criado por Adrien Coquet, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode). Modificações: colorização.

["Staff Management"](https://thenounproject.com/term/staff-management/2013918/) criado por OCHA Visual, dedicado ao [domínio público](https://creativecommons.org/publicdomain/zero/1.0/).

["certificate"](https://thenounproject.com/term/certificate/2606035/) criado por Vectors Point, da colecção [The Noun Project](thenounproject.com), licenciado sob [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode).

["Laptop"](https://thenounproject.com/term/laptop/1388514/) criado por i cons, da coleccçãao [Noun Project](thenounproject.com), licenciado sob [CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/pt/legalcode)
