---
layout: article
categories:
- newsletter
- boletim
title: Boletim 2023 - 4º trimestre
date: 2024-01-02
---

O último trimestre de 2023 foi calmo, para recuperarmos da Festa do Software
Livre 2023 e tratarmos de assuntos internos (contabilidade, manutenção de
infraestrutura, etc).


## Festa do Software Livre 2023 - Contas

Passada a Festa do Software Livre, fizemos o balanço do evento e elaborámos um
resumo e o relatório de contas:

- [Resumo do evento (português)](/relatorios/2023-festa-do-software-livre-relatorio-resumo-pt.pdf)
- [Resumo do evento (inglês)](/relatorios/2023-festa-do-software-livre-relatorio-resumo-en.pdf)
- [Relatório de Contas](/relatorios/2023-festa-do-software-livre-relatorio-de-contas.pdf)

Também começámos o processo de edição e publicação de vídeos das apresentações.
Para já só está um disponível: [Chat Control por Paula
Simões](https://viste.pt/w/p/sK622q1Tr3zqWnFAePuUZc).


## Hacktoberfest - Boas prácticas de colaboração

Em outubro organizámos uma sessão onde a Joana Simões nos falou das boas
práticas para colaborar em projetos de Software Livre. Abordou tópicos como
quando e como apresentar um PR e comunicação com maintainers, usando o projeto
pygeoapi como exemplo.

O vídeo está disponível na página de Recursos do site da ANSOL, onde também se pode encontrar a apresentação do Marcos Marado no Hacktoberfest de 2022:

[Como contribuir para projectos de Software Livre](/recursos/como-contribuir/)


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
