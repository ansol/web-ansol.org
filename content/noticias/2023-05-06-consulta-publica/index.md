---
categories:
- direitos de autor
- copyright
- drm
- dsm
- eu
- Consulta Pública
layout: article
title: ANSOL participa em Consulta Pública sobre Direitos de Autor
date: 2023-05-06
---

No seguimento do [contributo enviado ao Parlamento](https://ansol.org/noticias/2022-10-10-contributo-direitos-de-autor-parlamento/) em Outubro passado, e depois do [apelo feito em carta aberta](https://direitosdigitais.pt/comunicacao/noticias/142-carta-aberta-aos-deputados-da-assembleia-da-republica-sobre-a-diretiva-do-direito-de-autor) em que a ANSOL apelou ao Parlamento que a transposição da Directiva Europeia que actualiza as matérias de Direito de Autor no âmbito do Mercado Único Digital, a ANSOL participou agora na [consulta pública ao Decreto-Lei que transpõe a directiva](https://www.consultalex.gov.pt/ConsultaPublica_Detail.aspx?Consulta_Id=294).

Neste contributo, que pode ser [lido na íntegra](ContributoConsultaDirectiva.pdf), a ANSOL focou-se nos nove aspectos mais críticos, em seu entender, na proposta apresentada. Destas, destacam-se:

* Um problema de definição: a proposta do Governo refere, "programas de computador de fonte aberta" sem apresentar uma definição deles. Apelamos ao uso da expressão "software livre ou de código aberto", já presente no corpo legislativo Português;
* Sem necessidade apresentada pela directiva, e até ao arrepio dela, a proposta apresenta uma alteração aos direitos dos utilizadores no uso de obras com DRM. [Publicámos sobre este tema](https://drm-pt.info/2017/04/08/parlamento-aprova-projeto-de-lei-que-resolve-drm-fixcopyright-publicdomain/) no nosse site de campanha contra o DRM;
* O famoso Artigo 13/Artigo 17 é transposto nesta proposta, e a ANSOL aponta três falhas na proposta de transposição deste artigo, à luz do Tribunal de Justiça da União Europeia.


Como sempre, a ANSOL demonstra a sua total disponibilidade para ajudar à boa transposição desta directiva, seguindo com atenção os desenvolvimentos nesta matéria, agora aguardando a revisão que o Governo fará à sua proposta, à luz deste e outros contributos que recebeu nesta consulta pública.
