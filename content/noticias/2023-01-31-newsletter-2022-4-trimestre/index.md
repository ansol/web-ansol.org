---
categories:
- newsletter
layout: article
title: Newsletter 2022 - 4º trimestre
date: 2023-01-31
---

No 4º trimestre de 2022 a ANSOL organizou um par de eventos no contexto do
Hacktoberfest e lançou a campanha [Fedigov](https://fedigov.pt). Também
estivemos presentes na conferência parlamentar ["Direitos de autor e direitos
conexos na era
digital"](https://app.parlamento.pt/comissoes/programaA4conferencia11out2022.pdf)
e no ["XIX Encontro regional BAD
Açores"](https://eventos.bad.pt/event/xix-encontro-regional-bad-acores/).

<!--more-->

## Eventos

Organizámos, no contexto do [Hacktoberfest](https://hacktoberfest.com), um par
de eventos:

- [Breve Introdução ao git, gitlab e github](https://ansol.org/eventos/2022-10-13-hacktoberfest/),
  uma apresentação de Marcos Marado;
- [Sessão de contribuição](https://www.meetup.com/portocodes/events/289194405/),
  em colaboração com o [Porto Codes](https://porto.codes) e [Significa](https://significa.co).


## Fedigov - Comunicação federada nas instituições públicas

Lançámos uma campanha para encorajar as instituições públicas a garantir a
soberania das suas comunicações.

- [comunicado de imprensa](https://ansol.org/noticias/2022-12-19-fedigov/)
- [campanha fedigov.pt](https://fedigov.pt)


## Conferência sobre Direitos de autor

A ANSOL foi convidada a enviar um contributo sobre Direitos de Autor para a
Comissão de Cultura, Comunicação, Juventude e Desporto da Assembleia da
República, no âmbito da conferência realizada dia 11 de outubro de 2022.

Além do contributo por escrito, a ANSOL esteve presente na conferência e a
nossa intervenção está disponível nos links abaixo.

- [Sumário do Contributo da ANSOL](https://ansol.org/noticias/2022-10-10-contributo-direitos-de-autor-parlamento/)
- [Contributo da ANSOL completo (pdf)](https://ansol.org/noticias/2022-10-10-contributo-direitos-de-autor-parlamento/contributo.pdf)
- [Intervenção do Tiago Carrondo pela ANSOL](https://viste.pt/w/uBx4qQtaYRsSVVGQScbTE8?start=1h44m3s)


## XIX Encontro Regional BAD Açores

Marcámos presença no [XIX encontro regional da BAD
Açores](https://eventos.bad.pt/event/xix-encontro-regional-bad-acores/).


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.



[sfd]: https://www.softwarefreedomday.org/
[musas]: http://musas.pegada.net/
[bad]: https://bad.pt
[jornadas]: https://eventos.bad.pt/event/iv-jornadas-de-open-source/
[pasc]: https://pasc.pt/
[ogp]: https://ogp.eportugal.gov.pt
[trad-matrix]: https://matrix.to/#/#traduções:ansol.org
