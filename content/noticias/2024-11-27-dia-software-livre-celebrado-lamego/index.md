---
layout: article
title: Dia do Software Livre celebrado em Lamego
date: 2024-11-27
image:
  caption: |
    Entrada do Museu de Lamego, por Paula Simões, sob a licença CC BY-SA 4.0
---

O Dia do Software Livre foi celebrado em Lamego no dia 21 de setembro pela
ANSOL, com o apoio da [Wikimedia Portugal](https://wikimedia.pt/) e da [Digital
Freedom Foundation](https://digitalfreedoms.org/en/).

O encontro-convívio juntou pessoas de Coimbra, Lamego, Seia, e Viseu, e contou
com uma visita ao Museu de Lamego, onde entre outras coisas se encontra um dos
monumentos que está em concurso no “[Wiki Loves
Monuments](https://wikilovesmonuments.org.pt/lista/)” durante o mês de outubro.
O evento terminou com um jantar e passeio pela cidade, que foi aproveitado para
fazer contribuições para o OpenStreetMap.

![Vista sobre a cidade de Lamego, com a Sé Catedral ao centro, rodeada de casas, tirada num dos passeios pela cidade](./castelo-lamego.jpg)
