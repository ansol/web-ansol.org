---
layout: article
categories:
- DRM
tags:
- DRM
- IDAD
- SemDRM
title: Escolhe SemDRM
showcover: false
date: 2024-12-20
---

No Dia Internacional Contra o DRM 2024, a ANSOL publica um vídeo com várias sugestões de conteúdos livres de DRM:

{{< peertube viste.pt 74QYu81a68hxsseijD5GDm >}}

[peertube (viste.pt)](https://viste.pt/w/74QYu81a68hxsseijD5GDm), [youtube](https://www.youtube.com/watch?v=HAAXuY1iesU)

**Links para os sites mencionados:**

* [Bandcamp](https://bandcamp.com)
* [Projecto Adamastor](https://projectoadamastor.org)
* [Cory Doctorow's Craphound](https://craphound.com/shop)
* [Libro.fm](https://libro.fm)
* [Good Old Game](https://www.gog.com/en)
* [Project Gutenberg](https://www.gutenberg.org)
* [Story Bundle](https://storybundle.com/fantasy)
* [Open Book Publishers](https://www.openbookpublishers.com)
* [Ubuntu](https://ubuntu.com/desktop)
* [Internet Archive](https://archive.org/details/texts)
* [Musopen](https://musopen.org)
* [Free Music Archive](https://freemusicarchive.org/home)
* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)
* [Biblioteca Nacional Digital](https://bndigital.bnportugal.gov.pt)
* [PeerTube](https://joinpeertube.org)

**Créditos música:**

[Kokori](https://kokori.grogue.org/)

**Mais informação sobre DRM:**

* [DRM Portugal](https://drm-pt.info)
* [Defective by Design](https://www.defectivebydesign.org)
