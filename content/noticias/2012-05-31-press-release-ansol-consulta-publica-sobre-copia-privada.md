---
categories:
- press release
- cópia privada
- imprensa
- Consulta Pública
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 11
  - tags_tid: 19
  node_id: 76
layout: article
title: PRESS RELEASE ANSOL - Consulta Pública sobre Cópia Privada
created: 1338452326
date: 2012-05-31
aliases:
- "/article/76/"
- "/node/76/"
- "/pr-20120531/"
---
<p>Lisboa, 31 de Maio de 2012: A Associação Nacional para o Software Livre <a href="https://ansol.org/politica/copiaprivada/ansol-a-ue-201205">participa na consulta pública europeia sobre taxação da cópia privada</a>, na continuidade do seu trabalho.<br> <br> A ANSOL lida com a temática do Direito de Autor desde a sua formação em 2001, altura em que começou a acompanhar a Diretiva Europeia do Direito de Autor na Sociedade de Informação, e desde há vários anos que tem tentado evitar a disseminação de taxas pela cópia privada no meio digital.<br> <br> "<em>Apesar do tom unilateral presente no <a href="http://ec.europa.eu/commission_2010-2014/barnier/headlines/speeches/2012/04/20120402_en.htm">questionário</a>, a ANSOL não podia deixar de participar no processo, tal como o fez numa <a href="https://circabc.europa.eu/faces/jsp/extension/wai/navigation/container.jsp">consulta pública anterior</a> e em torno do infame <a href="https://ansol.org/politica/copiaprivada/prescecc20120208">Projeto de Lei 118/XII</a></em>", diz Rui Seabra, presidente da Direção da ANSOL.<br> <br> As perguntas presentes no questionário[1] efetivamente partem do pressuposto que a taxação da cópia privada é um facto, e estão orientadas à harmonização das mesmas na União Europeia, evitando a pergunta de se devem ou não ser aplicadas.<br> <br> CONTACTOS <a href="https://ansol.org/contacto">http://ansol.org/contacto</a></p>
