---
layout: article
title: 10 ideias para a próxima Legislatura
date: 2022-01-03
image:
  caption: |
    Palácio de São Bento,
    por [Manuel Menal](https://www.flickr.com/photos/mmenal/9307363528),
    sob a licença [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)
---

Em Janeiro decorrerão as próximas eleições legislativas. Com a fase
pré-eleitoral a começar, a ANSOL apresenta aqui as suas 10 ideias para a
próxima Legislatura:

<!--more-->


## 1. Transposição da Directiva de Direitos de Autor

A proposta de transposição de Directiva de Direitos de Autor proposta pelo
Governo que agora cessa funções demonstra um processo falhado. Por um lado,
porque é feita uma proposta que sublinha a necessidade de ter o tema em
consulta pública, mas que aparece já texto-feito quando tal consulta nunca
aconteceu. Depois, porque a proposta peca na forma como transpõe, ao mesmo
tempo que insere outras alterações que não derivam da directiva, sem explicação
ou justificação.

É preciso que esta transposição seja feita de forma ponderada, justificada, e
tendo em consideração os impactos que cada uma das medidas terão na sociedade
civil, rejeitando o oportunismo de usar o momento de transposição para tomar
outras medidas legislativas sem ponderação própria.


## 2. Renovação RNID

O RNID - Regulamento Nacional de Interoperabilidade Digital - regula a Lei das
Normas Abertas. A sua revisão devia ter acontecido, por lei, o mais tardar até
ao passado dia 19 de outubro. Não só urge preparar esta renovação, como impera
que se use essa oportunidade não só para a necessária actualização tecnológica,
como também para munir a própria Lei dos mecanismos necessários (por exemplo,
mecanismos de queixa com tempos de resposta previstos e medidas sancionatórias)
para que ela se faça cumprir. Esta renovação deve acontecer o mais tardar até
12 de fevereiro de 2022, data para o prazo em que é obrigatório aos
Estados-Membro actualizar a versão da norma harmonizada de requisitos de
acessibilidade para sítios Web e aplicações móveis.


## 3. Revogação de Lei e Memorando para medidas extra-judiciais "anti-pirataria"

O memorando de entendimento que colocava entidades não judiciais a declarar
determinados websites como estando a cometer violações ao Direito de Autor,
censurando-os via DNS, já era uma inaceitável inversão ao ónus da prova, que
causou danos irreversíveis a alguns dos websites afectados. A expansão desse
memorando para Lei, como ocorreu na legislatura que agora termina, não só
reforça um erro, como o expande, agora com o bloqueio de IPs, o que aumentará
os danos colaterais desta medida, mesmo quando comprovado que o website em
questão esteja mesmo a violar direitos de autor, visto que o IP pode ser
partilhado. Acresce que a medida tem poucas consequências para quem quiser
mesmo estar do lado errado da Lei: levantar o mesmo serviço num novo IP é fácil
e rápido. A Lei aproveitou ainda para reduzir os efeitos da Lei que proteje os
cidadãos do DRM, sem justificação para tal. Tanto esta Lei como o anterior
Memorando devem ser revogados.


## 4. Dinheiro Público? Código Público!

Queremos legislação que obrigue software financiado com dinheiro de
contribuintes, desenvolvido para o setor público, seja publicado com uma
licença de Software Livre. Se se trata de dinheiro público, o código também
deve ser público. Muito se pode fazer para trabalhar neste caminho, em vários
casos capitalizando e reforçando aquilo que já tem sido feito.

Como exemplo, seria útil democratizar o acesso à informação retida na
plataforma de pareceres prévios, bem como o reforço ao Código dos Contratos
Públicos no sentido de impedir a práctica ilegal de procuração de licenças de
software de marcas ou fabricantes específicos, através de melhor fiscalização,
mais rigor na fase dos pareceres prévios, e melhores mecanismos de denúncia.


## 5. Poder e meios à CNPD

Temos uma Comissão Nacional de Protecção de Dados que, frequentemente, declara
que não faz mais, melhor e mais rapidamente o seu trabalho por falta de meios.
Por outro lado, temos visto recorrente menosprezo pela Comissão: pedidos de
parecer a projecto enviados quando o projecto já está aprovado, respostas aos
pareceres totalmente ignoradas, e até mesmo reduções ao âmbito de actuação da
CNPD.

Defendemos o oposto: os pareceres da CNPD merecem em muitos casos carácter
vinculativo, a sua existência deve ser em determinadas circunstâncias um
obrigatório pré-requisito, e, obviamente, devem ser providenciados os meios
necessários para que a CNPD possa cumprir o seu desígnio.


## 6. Contra a "nova" Patente Unitária

Debaixo do véu das teóricas vantagens de uma "Patente Unitária", que
normalizaria o sistema de registo de patentes para toda a Europa, está a
ocorrer uma reformulação e restruturação de todo o sistema de Patentes, com a
criação de um sistema de tribunais especializados de patentes que
propositadamente saem do âmbito da ordem legal Comunitária. Um dos efeitos
nefastos deste movimento é o regresso das patentes de software: apesar do
Parlamento Europeu ter explicitamente rejeitado a patenteabilidade do Software,
em 2005, com a Patente Unitária o European Patent Office tem agora autonomia
para decidir o que é patenteavel ou não, e já há registo do seu interesse em
abrir as portas a patentes de Software.


## 7. Software na Educação

Os nossos alunos e seus encarregados de educação não devem ser obrigados a usar
software proprietário. É este conceito de independência tecnológica que serve
de base para leis como a Lei das Normas Abertas. No entanto, a prática
conta-nos uma história diferente, e as queixas que chegam até nós têm abundado.
Obrigatoriedade do uso de plataformas terceiras como Zoom ou Teams, com a
obrigação da aceitação de termos de serviço e partilha de dados pessoais com
essas empresas; comunicação obrigatória através de plataformas como Whatsapp. É
preciso acabar com todos estes casos, capacitando as nossas escolas com
infraestrutura própria.


## 8. Correcção do "programa Escola Digital" na forma de atribuição de PCs a alunos

Computadores com software proprietário, em que os alunos não podem instalar
software livre, em que o computador é gerido por entidades privadas terceiras,
e que no final o computador tem de ser devolvido - o actual programa Escola
Digital, além da falta de transparência que marcou a sua implementação, peca
por não ter aprendido com as experiências anteriores. Temos agora nas mãos
equipamento que não pertence aos alunos, software que não é controlado por eles
nem pelo estado, e contratos de suporte ao processo que não dá o suporte que
realmente interessa às escolas que não têm os meios para proceder à parte que
lhes coube neste esquema. Será agora necessário gerir a corrente situação, de
forma estratégica e pensando no longo prazo.


## 9. Contra o experimentalismo não fundamentado de voto eletrónico ou online

Depois de um projecto-piloto nas eleições europeias, considerado pelo
Ministério da Administração Interna como um sucesso, ainda que depois o mesmo
projecto tenha sido "arrasado" pela CNPD, que concluiu que ficaram "feridos os
mais básicos princípios do Estado de Direito Democrático, com menosprezo pelos
princípios da previsibilidade e da transparência do processo eleitoral", em vez
de admitir o passo em falso, tem-se insistido nesse caminho, tentando até ir
mais longe, e propondo um ainda mais arriscado voto online, com um teste-piloto
na eleição ao Conselho das Comunidades Portuguesas, a ocorrer já no primeiro
semestre de 2022. Este tipo de experimentalismo, feito sem a análise e o
cuidado que o sistema democrático exige, deve acabar.


## 10. Recusa aos ataques à criptografia

Não é apenas uma mas várias as iniciativas, maioritariamente oriundas do espaço
comunitário, que, com o disfarce do combate contra o terrorismo, a pedofilia,
ou a violação dos direitos de autor, se promove a ideia da criação de cavalos
de tróia nos sistemas que se querem seguros e privados. Estas medidas metem em
risco a segurança e privacidade de todos os cidadãos - incluindo minorias,
crianças e os que mais interesse temos em proteger. Portugal deve recusar este
tipo de medidas.
