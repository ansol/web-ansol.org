---
categories:
- Parlamento Europeu
- Imprensa
- DRM
tags:
- DRM
- RNID
- Public Money? Public Code!
- Fedigov
- Chat Control
layout: article
title: ANSOL no Parlamento Europeu
date: 2025-02-07
image:
  caption: |
    Membros da ANSOL à frente do Parlamento Europeu, em Bruxelas

---

Na passada semana (mesmo antes da [FOSDEM 2025](https://fosdem.org/2025)), a ANSOL esteve novamente presente em Bruxelas, e aproveitou para reunir com alguns **Deputados do Parlamento Europeu**, que representam o nosso país na sede da União Europeia.
Entre os dias 29 e 31 de Janeiro de 2025, dois membros da direção da ANSOL participaram nos seguintes encontros:

<style>
  table tbody tr:nth-child(2n + 1) { background-color: #eee; }
</style>


| Deputado(a)              | Grupo Político no Parlamento Europeu                                             | Grupo Político em Portugal            |
|--------------------------|----------------------------------------------------------------------------------|---------------------------------------|
| **Ana Vasconcelos**      | Grupo Renew Europe                                                               | **IL** - Iniciativa Liberal           |
| **Bruno Gonçalves**\*    | Grupo da Aliança Progressista dos Socialistas e Democratas no Parlamento Europeu | **PS** - Partido Socialista           |
| **Catarina Martins**     | Grupo da Esquerda no Parlamento Europeu - GUE/NGL                                | **BE** - Bloco de Esquerda            |
| **Hélder Sousa Silva**\* | Grupo do Partido Popular Europeu (Democratas-Cristãos)                           | **PSD** - Partido Social Democrata    |
| **João Oliveira**        | Grupo da Esquerda no Parlamento Europeu - GUE/NGL                                | **PCP** - Partido Comunista Português |
| **Sebastião Bugalho**\*  | Grupo do Partido Popular Europeu (Democratas-Cristãos)                           | **PSD** - Partido Social Democrata    |
| **Sérgio Humberto**\*    | Grupo do Partido Popular Europeu (Democratas-Cristãos)                           | **PSD** - Partido Social Democrata    |

<small>\* representado pelo(a) assistente</small>

As reuniões tiveram como objetivos estreitar relações e relembrar as nossas preocupações e campanhas.
Estas conversas foram bastante positivas, e esperamos continuar a promover a colaboração futura e sempre que os temas envolvidos sejam relevantes para o Software Livre.

Deixamos aqui um resumo dos temas que levámos connosco a Bruxelas:
- [Dinheiro público? Código público!](#dinheiro-público-código-público)
- [CSA Regulation e Chat Control](#csa-regulation-e-chat-control)
- [DRM](#drm)
- [RNID - Regulamento Nacional de Interoperabilidade Digital](#rnid---regulamento-nacional-de-interoperabilidade-digital)
- [Redes sociais interoperáveis](#redes-sociais-interoperáveis)

---

## Dinheiro público? Código público!

As soluções tecnológicas desenvolvidas com dinheiro público, deveriam ser também elas públicas.

A situação atual promove o desperdício de fundos e investimento público, promove a obsolescência por incapacidade de manutenção e não permite assentar em projetos presentes para um maior crescimento futuro.

Entendemos no entanto que mudanças drásticas ao paradigma atual dificulte a adoção e consequente melhoria dos processos em causa.
Com base nestes pressupostos, sugerimos que seja implementada uma **diretiva ao nível da União Europeia** que obrigue a que **os concursos públicos têm que publicar abertamente as soluções tecnológicas desenvolvidas** (seguindo as 4 liberdades do Software Livre).

Esta simples alteração promove:
- melhor alocação de recursos (através do reaproveitamento e adaptação a fins não previstos no âmbito inicial),
- melhoria contínua das soluções existentes,
- mais facilidade de manutenção ao longo do tempo através da utilização de tecnologias públicas e abertas de conhecimento distribuído (contrastando com uma solução proprietária e com documentação privada),
- estimulação de uma concorrência mais justa
- avanço para a soberania digital.

links:
- https://publiccode.eu/pt/
- [Campanha da FSFE](https://fsfe.org/activities/publiccode/publiccode.en.html)
- [Brochura em português](https://download.fsfe.org/campaigns/pmpc/PMPC-Modernising-with-Free-Software.pt_br.pdf)
- [Definição de Software Livre](https://ansol.org/software-livre/)

## CSA Regulation e Chat Control

O Child Sexual Abuse Regulation (CSAR) tem um objetivo inequivocamente crítico e importante: a proteção da nossa sociedade, em especial os menores. Ninguém tem dúvida que a agressão sexual é um ato horrível e que todos devemos combater. No entanto a iniciativa **Chat Control é inequivocamente a opção errada** para atingir este objetivo.
Sabemos que a posição do parlamento é sensata, mas tem sido debatido no Conselho da União Europeia uma proposta de lei que vai contra os direitos fundamentais individuais (privacidade) e pouco ou nada faz para combater as atrocidades em causa.

Nomeadamente, **não podemos aceitar** as propostas em cima da mesa pelos seguintes motivos:
- qualquer tipo de análise (local ou não) de conversas privadas é um atentado direto a um dos direitos fundamentais individuais e também à própria democracia em si,
- sabe-se que a tecnologia a usar não é robusta, tendo uma alta taxa de falsos positivos. E como consequência, qualquer um de nós pode ser acusado de ser um agressor sexual (de crianças) sem ter feito nada de errado. Além disso, a possível existência de uma forma de quebra de privacidade e/ou sigilo é uma arma, ~~se~~ quando for usado nas mãos erradas (é inevitável que uma brecha existente seja explorada),
- impacto no desenvolvimento de jovens (que alegamos querer proteger)
- impacto no sigilo profissional
- não devemos desperdiçar os nossos recursos em métodos que sabemos que são ineficazes e prejudiciais para aqueles que estamos a tentar proteger.

Existem no entanto sugestões alternativas que visam atacar o problema de uma forma estrutural e com maior probabilidade de sucesso (sem delapidar as fundações de uma sociedade democrática):
1. Educação, consciencialização e capacitação das vítimas,
2. Mudanças sociais e estruturais (eg: combate à pobreza),
3. Reestruturar a polícia e outras instituições,
4. Investir nas linhas de apoio,
5. Aplicar a legislação já em vigor (a Diretiva 2011/93/EU obriga os Estados-Membros a tomar mais medidas para endereçar o abuso sexual de menores a nível nacional. Esta diretiva não foi implementada na sua totalidade apesar de estar em vigor há 11 anos),
6. Prevenção,
7. Trabalhar em conjunto com outras associações/instituições para proteger as crianças e todos os direitos fundamentais.

Reiteramos a necessidade e importância de **votar contra** qualquer tipo de medida que ponha em causa o direito à privacidade e à própria democracia.

Links:
- https://chatcontrol.pt/
- [Diretiva 2011/93/EU](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32011L0093)


## DRM

O DRM (Digital Rights Management) é um conjunto de tecnologias e políticas projetadas para controlar e proteger os direitos autorais de conteúdo digital, como música, filmes, livros e software.
Os autores devem ser pagos como qualquer trabalhador, e a pirataria é uma realidade que deve ser combatida.
Infelizmente, o DRM acaba por demonstrar uma eficácia muito reduzida face à pirataria, trazendo problemas e dificuldades acrescidas ao direito de utilização dessas obras para os efeitos previstos na lei.

Por outro lado, **a legislação portuguesa é um exemplo a ter em conta**, pois identifica como DRM as proteções contra atos não previstos na lei, o que abriga a quebra legal dos mecanismos de proteção,  quando usados para fins legais.

A nossa sugestão passa por identificar a necessidade de uma definição mais precisa do DRM, com enquadramento legal e tecnológico para as exceções previstas na lei. Tal definição deverá permitir o desenvolvimento de soluções de software (aberto) que permita a utilização das obras adquiridas para todos os fins legais previstos na lei.

Links:
- https://drm-pt.info/
- [Código de Direito de Autor e Direitos Conexos](https://www.pgdlisboa.pt/leis/lei_mostra_articulado.php?nid=484&tabela=leis)
- [artigo: Parlamento aprova projeto de lei que resolve DRM](https://drm-pt.info/2017/04/08/parlamento-aprova-projeto-de-lei-que-resolve-drm-fixcopyright-publicdomain/)


## RNID - Regulamento Nacional de Interoperabilidade Digital

O direito de acesso aos documentos digitais públicos, por qualquer cidadão, é fundamental. Não é admissível que seja necessário instalar um software proprietário (e muitas vezes pago) para aceder a um conteúdo público.

Em Portugal foi aprovada uma lei que obriga entidades ou serviços públicos à disponibilização de ditos documentos em formatos abertos. Tal medida foi elogiada por toda a Europa (e mundo).

Infelizmente essa realidade não é igual em toda a União Europeia.
Além disso, apesar da lei obrigar a disponibilização de documentos em formatos abertos, não existem nenhum mecanismo que garanta a aplicação dessa lei, havendo vários casos de incumprimento que estão por corrigir há vários anos.

Sugerimos duas iniciativas:
- Uma recomendação ao Conselho da União Europeia (ao abrigo da regra 47) para que seja criada uma proposta para a União Europeia que vise a obrigatoriedade da disponibilização de documentos em formatos com normas abertas para todos os serviços ou entidades públicas.
- Uma recomendação ao governo português acerca da necessidade de fiscalizar e auxiliar no cumprimento da lei, e aplicar prazos para correção das mesmas.

Links:
- https://rnid.ansol.org/
- [artigo: Monitorização do RNID](https://ansol.org/iniciativas/monitorizacao-rnid/)
- [Lista de formatos abertos](https://rnid.ansol.org/RNID.html)

## Redes sociais interoperáveis

A necessidade da presença digital de órgãos e instituições públicas junto da sociedade é fundamental.
No entanto, a maioria das vezes são usadas soluções proprietárias, que requerem quase sempre uma conta e/ou aceitação de termos e condições, que não está alinhado com a liberdade individual.

Seria desejável que entidades públicas pudessem chegar à sociedade através de meios, tecnologias e plataformas abertas e operáveis, respeitando as liberdades individuais e promovendo uma soberania digital de forma ética.

O projeto fedigov.pt sugere a criação de instâncias federadas, geridas por instituições públicas, que permitam o livre acesso à comunicação, sem a manipulação e controlo das plataformas geridas (de forma pouco transparente) por terceiros.
Seria interessante ver este assunto discutido no seio da União Europeia.

Links:
- https://fedigov.pt

---
