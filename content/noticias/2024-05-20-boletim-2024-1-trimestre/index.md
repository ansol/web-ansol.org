---
layout: article
categories:
- newsletter
- boletim
title: Boletim 2024 - 1º trimestre
date: 2024-05-20
aliases:
- /noticias/2024-05-2024-boletim-2024-1-trimestre/
---

No primeiro trimestre de 2024 preparámos a ida à FOSDEM, analisámos os
programas para as eleições legislativas, tivemos a nossa assembleia geral
eleitoral e começámos a publicar artigos na PCGuia.


## FOSDEM / Parlamento Europeu

A ANSOL esteve em Bruxelas para assistir à FOSDEM, e aproveitou a ocasião para
marcar algumas reuniões. Reunimos com os eurodeputados portugueses que
aceitaram o nosso convite (BE, PCP, PS, e Francisco Guerreiro) para os
relembrar das nossas preocupações e campanhas. Também reunimos com um
representante do Conselho Directivo do Instituto dos Registos e do Notariado
para assinar um protocolo de colaboração que será publicado em breve.
Aproveitámos a ocasião para conviver e trocar ideias com vário grupos de
Software Livre presentes na FOSDEM.


## Assembleia Geral Eleitoral 2024 da ANSOL

Dia 16 de março reunimos no MILL - Makers in Little Lisbon para proceder às
eleições dos órgãos sociais para o mandato de 2024-26. Os relatórios de contas
e actividades, assim como as planos para o próximo mandato, estão disponíveis
na página de transparência da ANSOL.

- <https://ansol.org/noticias/2024-01-16-assembleia-geral-eleitoral/>
- <https://ansol.org/noticias/2024-03-22-nova-direccao/>
- <https://ansol.org/transparencia/>

Em breve estará disponível a acta da assembleia na nextcloud da ANSOL,
acessível a sócios com conta. Quem não tiver acesso e quiser, pedimos que envie
um email para direccao@ansol.org.


## Análise dos candidatos à Legislatura 2024-28

No período anterior às eleições legislativas, a ANSOL propôs 6 ideias para a
nova legislatura e analisou os programas dos partidos no que toca a temas
relacionados com Software Livre:

- <https://ansol.org/noticias/2024-01-12-ideias-para-a-proxima-legislatura/>
- <https://ansol.org/noticias/2024-03-07-software-livre-nos-programas-eleitorais-2024/>

No seguimento desta análise, que foi enviada para os partidos analisados,
reunimos com o candidato da CDU para discutir as nossas propostas.


## Publicações na PCGuia

Este ano começamos a publicar mensalmente artigos na versão em papel da PC
Guia. Podem encontrar dois artigos da ANSOL na secção Linux nas edições a
partir de fevereiro: um tutorial e uma coluna de opinião. Alguns dos artigos
já se encontram disponíveis online:

- <https://www.pcguia.pt/2024/03/instale-o-pi-hole/>
- <https://www.pcguia.pt/2024/04/bem-vindo-a-matrix/>
- <https://www.pcguia.pt/2024/05/mastodon-a-rede-social-para-todos/>

Alguns dos artigos têm entrevistas a acompanhar, que podem ser vistas no
nosso canal de Peertube: <https://viste.pt/c/ansol/videos>


## Celebração do dia I love Free Software

A ESOP organizou um evento na PASC durante o dia "I love Free Software Day",
que contou com a participação da ANSOL. O principal tema discutido foi o voto
electrónico. A sessão foi gravada e está disponível no canal de Youtube da
PASC.

- <https://pasc.pt/2024/02/08/14-de-fevereiro-7o-debate-do-gt-pasc-democracia-cidadania-e-inclusao-social-com-gerardo-lisboa-esop-e-outros-convidados/>
- <https://www.youtube.com/watch?v=lCt_xe0wYX0>

## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
