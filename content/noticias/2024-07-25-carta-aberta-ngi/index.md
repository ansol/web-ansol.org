---
layout: article
title: A União Europeia deve manter o financiamento do Software Livre
date: 2024-07-25
---

# A União Europeia deve manter o financiamento do Software Livre

Inicialmente publicado por [petites singularités](https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/). Tradução inglesa por [OW2](https://www.ow2.org/view/Events/The_European_Union_must_keep_funding_free_software_open_letter). Tradução portuguesa por [ANSOL](https://ansol.org/noticias/2024-07-25-carta-aberta-ngi).


# Carta Aberta à Comissão Europeia

Desde 2020, os programas Next Generation Internet ([NGI](https://www.ngi.eu)), parte do programa Horizon da Comissão Europeia, financiam software livre na Europa utilizando um mecanismo de financiamento em cascata (ver, por exemplo, as [calls](https://www.nlnet.nl/commonsfund) da NLnet). Este ano, de acordo com o projeto de trabalho do Horizon Europe, que detalha os programas de financiamento para 2025, notamos que o Next Generation Internet já não é mencionada como parte do Cluster 4.

Os programas NGI demonstraram a sua força e importância no apoio à infraestrutura de software europeia, como um instrumento de financiamento genérico para financiar bens comuns digitais e garantir a sua sustentabilidade a longo prazo. Achamos esta mudança incompreensível, ainda mais quando o NGI se mostrou eficiente e económico para apoiar o software livre como um todo, desde as iniciativas mais pequenas até às mais estabelecidas. Esta diversidade de ecossistemas assegura a força da inovação tecnológica europeia, e manter a iniciativa NGI para fornecer apoio estrutural a projetos de software no centro da inovação mundial é fundamental para reforçar a soberania de uma infraestrutura europeia.
Contrariamente à perceção comum, as inovações técnicas têm frequentemente origem em comunidades de programação europeias e não norte-americanas, e são na sua maioria iniciadas por organizações de pequena escala.

O Cluster 4 anterior destinou 27 milhões de euros para:

 * "Internet centrada no ser humano alinhada com valores e princípios comummente partilhados na Europa" ;
 * "Uma Internet próspera, baseada em blocos de construção comuns criados no NGI, que permite um melhor controlo da nossa vida digital" ;
 * "Um ecossistema estruturado de colaboradores talentosos que impulsionam a criação de novos bens comuns da Internet e a evolução dos bens comuns da Internet existentes".

Em nome destes desafios, mais de 500 projetos receberam financiamento dos NGI nos primeiros 5 anos, apoiados por 18 organizações que gerem estes consórcios de financiamento europeus.

Os NGI contribuem para um vasto ecossistema, uma vez que a maior parte do seu orçamento é alocado para financiar terceiros, através de chamadas abertas, para estruturar bens comuns que cobrem todo o âmbito da Internet - desde hardware a aplicações, sistemas operativos, identidades digitais ou supervisão de tráfego de dados. Este financiamento de terceiros não é renovado no programa atual, deixando muitos projetos com poucos recursos para investigação e inovação na Europa.

Além disso, os programas NGI permitem intercâmbios e colaborações entre todos os países da Zona Euro, bem como "países em expansão"[^1], atualmente um sucesso e um progresso contínuo, tal como o programa Erasmus antes de nós. Os NGI também contribuem para abrir e apoiar relações mais longas do que o financiamento estrito de projetos. Incentiva a implementação de projetos financiados como pilotos, apoiando a colaboração, a identificação e a reutilização de elementos comuns entre projetos, a interoperabilidade em sistemas de identificação e muito mais, e a criação de modelos de desenvolvimento que combinem diversas escalas e tipos de regimes de financiamento europeus.

Embora os EUA, a China ou a Rússia utilizem enormes recursos públicos e privados para desenvolver software e infraestruturas que capturem massivamente dados de consumidores privados, a UE não pode permitir-se a esta renúncia.
O software livre e de código aberto, apoiado pelos NGI desde 2020, é, por definição, o oposto dos potenciais vetores de interferência estrangeira. Permite-nos manter os nossos dados locais e favorece uma economia e conhecimento em toda a comunidade, ao mesmo tempo que permite uma colaboração internacional.

Isto é ainda mais essencial no atual contexto geopolítico: o desafio da soberania tecnológica é central e o software livre permite enfrentá-lo ao mesmo tempo que atua pela paz e pela soberania no mundo digital como um todo.

Nesta perspectiva, instamo-la a reivindicar a preservação do programa NGI como parte do programa de financiamento para 2025.

[^1]: Tal como definido pelo Horizon Europe, os Estados-Membros em alargamento são a Bulgária, a Croácia, Chipre, a República Checa, a Estónia, a Grécia, a Hungria, a Letónia, a Lituânia, Malta, a Polónia, Portugal, a Roménia, a Eslováquia e a Eslovénia. Os países associados em alargamento (sob condição de acordo de associação) incluem a Albânia, a Arménia, a Bósnia, as Ilhas Faroé, a Geórgia, o Kosovo, a Moldávia, o Montenegro, Marrocos, a Macedónia do Norte, a Sérvia, a Tunísia, a Turquia e a Ucrânia. As regiões ultramarinas em expansão são: Guadalupe, Guiana Francesa, Martinica, Ilha da Reunião, Maiote, Ilha de São Martinho, Açores, Madeira, Ilhas Canárias.
