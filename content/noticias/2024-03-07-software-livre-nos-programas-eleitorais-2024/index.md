---
layout: article
title: "Software Livre nos programas eleitorais 2024"
date: 2024-03-07
image:
  caption: |
    ["Pálacio da Assembleia da Republica", por Chris Brown](https://commons.wikimedia.org/wiki/File:P%C3%A1lacio_da_Assembleia_da_Republica_\(2528472413\).jpg), sob a licença CC BY-SA 2.0, modificado pela ANSOL.
tags:
- Public Money? Public Code!

---

A alguns dias das  **eleições legislativas portuguesas de 2024**, analisámos os
programas eleitorais de todos os partidos que concorrem nas eleições de 10 de
março de 2024. Apresentamos aqui a análise no que toca a Software Livre, à
semelhança [do que fizemos em
2022](https://ansol.org/noticias/2022-01-19-software-livre-nos-programas-eleitorais-2022/).

Defendemos que todo o código financiado por dinheiro público deve ser Software
Livre (recomendamos que visitem a página [Public Money? Public
Code!](https://publiccode.eu/pt/)). Defendemos o direito à privacidade nas
comunicações dos cidadãos e somos contra o experimentalismo não fundamentado de
voto eletrónico ou online. Para conhecer melhor os ideais defendidos pela
ANSOL, sugerimos que leiam o nosso artigo "[Ideias para a legislatura
2024-2028](https://ansol.org/noticias/2024-01-12-ideias-para-a-proxima-legislatura/)".

Em caso de erros podem sugerir alterações no [nosso
repositório](https://gitlab.com/ansol/web-ansol.org) ou
[contactando-nos](https://ansol.org/contactos/).

## Glossário

* DRM: Digital Rights Management / Gestão de Direitos Digitais, ou, mais corretamente, Digital Restrictions Management / Gestão Digital de Restrições
* CNPD: Comissão Nacional de Proteção de Dados
* IA: Inteligência Artificial
* RNID: Regulamento Nacional de Interoperabilidade Digital
* portal BASE: portal dos contratos públicos
* CNCS: Centro Nacional de Cibersegurança
* API: Application Programming Interfaces / Interfaces para Programação de Aplicações

# Partidos

Os partidos apresentados estão ordenados pelo número de deputados eleitos em
2022 e, em caso de empate, por número de votos.

Os programas dos seguintes partidos não continham qualquer referência relevante aos assuntos da ANSOL:
[Reagir Incluir Reciclar](https://drive.google.com/file/d/1F0qJel3RcJQSrNvia1Fw6CQtMhfNqkZx/view),
[Nós Cidadãos](https://drive.google.com/file/d/1LmdQZ9sPanm48e6jPD6nup6J6NC06843/view),
[Nova Direita](https://novadireita.pt/wp-content/uploads/2023/12/PrioridadesParaPortugal_NOVADIREITA_compressed-1.pdf).

Os seguintes partidos não tinham disponível o programa à data de análise:
Partido Comunista dos Trabalhadores Portugueses,
Juntos Pelo Povo e
Partido Trabalhista Português.


## PS - Partido Socialista

Concorre em todos os círculos eleitorais.

[Programa Eleitoral](https://ps.pt/wp-content/uploads/2024/02/PS-Portugal_Inteiro_2024-6.pdf)

**Excertos relevantes do Programa:**

Na secção 1.2 do Capítulo "4.ª MISSÃO - UMA DEMORACIA DE QUALIDADE PARA TODOS",
o PS propõe "a desmaterialização dos cadernos eleitorais e o recurso alargado
às tecnologias de informação para simplificar os procedimentos eleitorais, com
manutenção de elevados padrões de segurança".

Na secção 5 do mesmo capítulo, o PS propõe "Garantir a total interoperabilidade
e partilha de dados entre as entidades da Administração Pública, reforçando a
adoção do princípio do only once". Na mesma secção, propõe também "Estabelecer
o acesso a uma cloud pública a que cada pessoa possa ter acesso, em condições
de proteção, privacidade e segurança, para garantir autonomia no acesso digital
numa perspetiva de soberania digital;"


**Considerações da ANSOL:**

**Não há referências a Software Livre no programa do PS.** Sobre o ponto 1.2
acima, já não fazem referência explícita ao voto eletrónico (ao contrário do
programa de 2022). Consideramos positiva a referência à interoperabilidade de
dados. Em relação ao ponto da cloud pública, os detalhes de implementação são
importantes: se for simplesmente subcontratado a uma empresa gigante
multinacional de tecnologia, o objectivo da soberania digital é derrotado.


## PPD/PSD.CDS-PP.PPM - ALIANÇA DEMOCRÁTICA

Concorre em todos os círculos eleitorais em coligação, excepto na Madeira. Na
Madeira o PPD/PSD concorre coligado com o CDS-PP sob o nome de "Madeira
Primeiro" e o PPM concorre separadamente. A coligação AD - Aliança Democrática
tem programa eleitoral conjunto, as outras candidaturas pela Madeira não
apresentam nenhum programa.

[Programa Eleitoral](https://ad2024.pt/pdf/ad-programa-eleitoral.pdf)

**Excertos relevantes do Programa:**

Ao longo do documento há referências para digitalizar o património cultural, a
administração pública, etc.

Na Parte I, no capítulo "Com Sentido de Estado", na secção "Transparência e
Combate à Corrupção", a AD defende "a disponibilização de ferramentas digitais
e práticas de **dados abertos** sobre os procedimentos de contratação e
decisões de despesa pública" no portal BASE.

Na secção "SISTEMA POLÍTICO E ELEITORAL" do mesmo capítulo, a AD defende um
teste ao **voto eletrónico não presencial** sobre os círculos das comunidades
portuguesas.

Na Parte II, capítulo "Apostar na Iniciativa Privada e na Produtividade", na
secção 2.2.4. "Inovação, Empreendedorismo e Digitalização", a AD defende que a
modernização e digitalização da administração pública implemente um plano de
serviços públicos digitais em que se apliquem os princípios dos **dados abertos
e da interoperabilidade**. Pretende também "Fortalecer a infraestrutura digital
e cibernética do país para garantir independência e soberania tecnológica, e
investir em tecnologias emergentes, assegurando a cibersegurança da informação,
a proteção das infraestruturas, a proteção de dados e a privacidade online."

**Considerações da ANSOL:**

**Não há referências a Software Livre no programa da AD.** Consideramos
positivo a aplicação de dados abertos e interoperabilidade na administração
pública, e a disponibilização de dados abertos no portal BASE. Sobre a
digitalização dos vários serviços que a AD refere, não é claro de que maneira
será feita, nem se será usado Software Livre ou que vá seguir as normas do
RNID. Sobre o teste ao voto eletrónico não presencial para as comunidades
portuguesas consideramos que deve se ter em conta o projeto-piloto em Évora,
[fortemente criticado pela CNPD][cnpd-voto].


## IL - Iniciativa Liberal

Concorre em todos os círculos eleitorais

[Programa Eleitoral](https://iniciativaliberal.pt/wp-content/uploads/2024/02/Por-um-Portugal-com-Futuro-Programa-Eleitoral-IL-2024.pdf)

**Excertos relevantes do Programa:**

Na secção 1.2 do capítulo C em "Defender a privacidade na era digital", a IL
defende **restringir o uso excessivo de videovigilância** e criar um
ecossistema digital seguro na Administração Pública, que reforça a
**encriptação e segurança dos dados pessoais**, e que garante a rastreabilidade
dos acessos a dados pessoais por parte dos trabalhados do setor público, e se
necessário rever o RNID. A IL ainda defende a garantia dos **meios necessários
à CNPD**.

**Considerações da ANSOL:**

**Não há referências a Software Livre no programa da IL.** Consideramos
positivo as medidas que tendem a garantir a **proteção de dados**. Também é
positivo considerarem a revisão do RNID, que devia ter acontecido há vários
anos.


## PCP-PEV - CDU - Coligação Democrática Unitária

Concorre em todos os círculos eleitorais como PCP-PEV.

### PCP - Partido Comunista Português

[Programa Eleitoral](https://www.cdu.pt/assets/docs/programa_eleitoral_pcp_2024.pdf)

**Excertos relevantes do Programa:**

Na [secção 2.3.4 do capítulo
2](https://www.cdu.pt/legislativas2024/programa-eleitoral-do-pcp#2-3-pol-ticas-chave),
o PCP apresenta as suas propostas para a transição digital. Nestas propostas
está incluída a **transparência e neutralidade da rede** e a **promoção do
software livre**. Do ponto de vista da inteligência artificial (IA), o PCP
propõe a proibição de **recolha invasiva de dados pessoais para IA**, como a
"vigilância permanente em contexto laboral, devassa automatizada da
privacidade, recolha de dados biométricos"; defende a limitação do uso de IA
para decisões com implicação directa na vida das pessoas (ex: "apoios sociais,
vistos, asilo, crédito bancário, acesso a um posto de trabalho, decisão
judicial"), e exige que "os parâmetros e as bases de dados usados numa tomada
de decisão sejam públicos e auditáveis"; e defende a "obrigatoriedade de
sinalização do uso de IA em produtos e serviços", sejam estes serviços públicos
ou privados.

**Considerações da ANSOL:**

**Há referências leves a Software Livre no programa do PCP**, dizendo que este
deve ser promovido, mas sem grandes detalhes. Vemos positivamente a defesa pela
**neutralidade da rede e sua transparência**, assim como a **defesa contra
vigilância invasiva**. Propõem muito fortemente uma regulação da IA no contexto
da defesa do trabalhador e do consumidor.

### PEV - Partido Ecologistas "Os Verdes"

[Manifesto Ecologista](https://www.cdu.pt/assets/docs/manifesto_ecologista_pev_2024.pdf)

**Excertos relevantes do Programa:**
Na [secção 7](https://www.cdu.pt/legislativas2024/manifesto-ecologista) do
manifesto ecologista, O PEV defende para o acesso à cultura que se deve
"desenvolver e qualificar a rede nacional de arquivos e bibliotecas, quer
através de meios físicos, quer de meios digitais, visando a facilitação da
disponibilização da informação arquivada".

**Considerações da ANSOL:**

**Não há referências a Software Livre no programa do PEV.** Consideramos
positivo que os arquivos e bibliotecas disponibilizem de maneira facilitada os
seus arquivos de formato digital, no entanto, consideramos que os sistemas
digitais de arquivos devem ser Software Livre.


## BE - Bloco de Esquerda

Concorre em todos os círculos eleitorais.

[Programa Eleitoral](https://www.bloco.org/media/PROGRAMA_BLOCO_2024.pdf)

**Excertos relevantes do Programa:**

Na [secção 17 do capítulo
C](https://programa2024.bloco.org/capitulo-3/17-comunidades-portuguesas-no-estrangeiro/),
o Bloco de Esquerda propõe para as comunidades portuguesas no estrangeiro um
teste de **voto eletrónico** à distância. Esse teste teria "a participação de
especialistas de segurança das Universidades portuguesas, utilização de código
aberto, e amplo escrutínio público".

Na [secção 18.2 do capítulo D](https://programa2024.bloco.org/capitulo-4/18-2-direitos-digitais/)
encontram-se as propostas do Bloco de Esquerda no âmbito do Digital. Defendem a
**neutralidade da Internet** e liberdade de expressão na mesma sem censura.
Propõem o **fim do DRM** e o fim da criminalização da partilha de conteúdos
para fins não comerciais. Defendem que a produção científica financiada por
dinheiros públicos seja depositada em repositórios abertos. Defendem ainda que
o **software criado ou comprado com dinheiro público, seja software livre ou de
código aberto.**

**Considerações da ANSOL:**

**Há referências positivas a Software Livre no programa do BE.** O programa
aborda muito positivamente várias questões que julgamos importantes, como a
**neutralidade da internet** e o **fim do DRM**, sendo de destacar a
**obrigatoriedade de Software Livre quando criado ou comprado com dinheiro
público**. Os requisitos para o teste ao voto eletrónico à distância são
correctos, mas o projecto deveria começar por um estudo sobre o desenho e
implementação do processo. Ao começar pelo teste, condiciona-se a
desvalorização das críticas que poderiam por em causa a sua exequibililidade,
como aconteceu no projeto-piloto de Évora, [fortemente criticado pela
CNPD][cnpd-voto].


## PAN - PESSOAS-ANIMAIS-NATUREZA

Concorre em todos os círculos eleitorais.

[Programa Eleitoral](https://2024.pan.com.pt/program)

**Excertos relevantes do Programa:**

Na secção de defesa dos direitos digitais, o PAN refere que deve ser avaliada a
**migração de todo o software** de agências governamentais e entidades públicas
para **Software Livre** e defende que todo o **software desenvolvido pelo
estado ou financiado por recursos públicos deve ser Software Livre**, salvo
situações devidamente justificadas. Propõem **reforçar os meios da CNPD e
CNCS**. Propõem reforçar o **investimento do Estado em infraestrutura
própria**.

**Considerações da ANSOL:**

**Há referências positivas a Software Livre no programa do PAN.** Consideramos
muito positivo as propostas de **migração para Software Livre** nas agências
governamentais e entidades públicas, tirando a parte das excepções.

## L - LIVRE

Concorre em todos os círculos eleitorais.

[Programa Eleitoral](https://partidolivre.pt/wp-content/uploads/2024/02/Programa2024_LIVRE.pdf)

**Excertos relevantes do Programa:**

No capítulo "Soberania Digital", o Livre defende a **neutralidade da
internet**, sem censura. Defendem que "os **roteadores e modems façam parte do
domínio das e dos consumidores**", o **direito ao esquecimento e privacidade
online**, e incentivam a adoção de **normas de acesso aberto**. Pretendem
**renovar o RNID**, prevenir a **vigilância em massa** e o abuso do **direito à
privacidade** assim como a utilização de dados pessoais sensíveis em segmento
de publicidade. Sobre o Software Livre, o Livre defende que o "**todo o código
desenvolvido com dinheiro público fique numa licença de código aberto**" e que
se introduza "**software livre e de código aberto em todos os níveis da
administração pública** e em instituições financiadas com recursos públicos,
com todos os registos públicos não confidenciais digitalizados e publicados num
banco de dados online aberto". O Livre defende ainda a "Obrigatoriedade de
**interoperabilidade de dados** para todos os serviços tecnológicos contratados
pelo Estado" e reforçar o orçamento e condições do CNCS.

No capítulo de prevenção e combate à corrupção, o Livre propõe "Assegurar a
transparência da contratação pública no Portal dos contratos públicos, através
da publicação de dados abertos". Para a IA, o Livre defende que se deve regular
criando um **orgão regulador de tecnologias de IA**, e que se previna a
**discriminação algorítmica de vencimentos**.

No capítulo "Democracria", o Livre defende uma revisão dos processoas
eleitorais que permita um investimento "no desenvolvimento e experimentação de
sistemas de **voto eletrónico não presencial**, particularmente nos círculos
eleitorais da emigração". Propõem a criação de livros escolares com licença de
autor aberta (Creative Commons) e sem DRM. Defendem o direito à fabricação e
reparação.

**Considerações da ANSOL:**

**Há referências positivas a Software Livre no programa do Livre**.
Consideramos muito positivo a defesa do Livre pelo **Software Livre** em toda a
administração pública e outras instituições financiadas com recursos públicos,
e a defesa pela **neutralidade da internet** e pelo direito à **privacidade**.
Consideramos também muito positiva a menção da liberdade de escolha de roteadores.
Em relação ao voto eletrónico propomos que se tenha em conta o projeto-piloto
de Évora, [fortemente criticado pela CNPD][cnpd-voto].


## MPT.A - ALTERNATIVA 21

Concorre em todo o país excepto Beja, Faro, Castelo Branco, Porto, Vila Real e
Açores (as duas primeiras rejeitadas pelo juiz).

[Programa Eleitoral](https://www.alternativa21.pt/docs/Programa_Eleitoral_2024_Alternativa_21.pdf)

**Excertos relevantes do Programa:**

A Coligação Alternativa 21 propõe "Promover e implementar o voto electrónico
(voto digital à distância ou remoto), como alternativa ao voto presencial, para
todos os nacionais, quer residam no país ou no estrangeiro".

**Considerações da ANSOL:**

**Não há referências a Software Livre no programa da Alternativa 21.**
Consideramos negativa a proposta de implementar o voto electrónico e à
distância para todos os nacionais. Deve ser tida em conta o projeto-piloto em
Évora, [fortemente criticado pela CNPD][cnpd-voto].


## VP - Volt Portugal

Concorre em todos os círculos eleitorais, excepto Bragança.

[Programa Eleitoral](https://voltportugal.org/storage/pdf/eleicoes/volt_programa_legislativas_2024.pdf)

**Excertos relevantes do Programa:**

Na secção 1.5 "Liberdades e direitos digitais", o VP defende a **neutralidade
da internet**, transformar a CNPD numa Secretaria de Estado **aumentando os
seus recursos** e tornando vinculativos os seus pareceres. Na secção 1.5.1
"Código aberto e literacia digital", o VP defende a substituição, de modo
faseado, do software usado em serviço público por alternativas de "**código
aberto**" de modo faseado e a promoção de software "não proprietário" na
educação, defende o desenvolvimento de APIs para acesso à população de **dados
públicos**. Na secção 1.5.2 "Proteção de dados, Direitos digitais e
Transparência", o VP defende o direito à **privacidade**, **encriptação** e
**esquecimento**.

Na seção 5.1 "Sistema Eleitoral e Político", o VP defende a adopção
generalizada do voto eletrónico à distância, "depois de testes com a
participação de especialistas de segurança de Universidades portuguesas, com o
objetivo de proporcionar um sistema seguro, anónimo, verificável, fiável e
acessível"

**Considerações da ANSOL:**

**Há referências positivas a Software Livre no programa do Volt**.
Consideramos positivo a defesa pela **neutralidade da internet**, o aumento de
recursos da CNPD e a defesa pela substituição do software em serviço público
para **Software Livre.** A promoção na educação de Software Livre pode ser
abrangidas a mais áreas e pode se basear na nossa [lista de Software Livre para
ensino e teletrabalho](https://covid-19.ansol.org/).  Vemos a
**disponibilização de dados públicos** via APIs como positiva, assim como o
**direito à privacidade e esquecimento**. Em relação ao voto eletrónico,
consideramos negativa a proposta de implementar o voto electrónico à distância.
Propomos que se tenha em conta o projeto-piloto de Évora, [fortemente criticado
pela CNPD][cnpd-voto].



## Tabela de resumo

Para tornar a análise aos programas dos vários partidos mais clara,
apresentamos uma tabela onde se reunem os pontos mais importantes para a ANSOL,
atribuindo pontos positivos ou negativos consoante a seguinte classificação:

-  **2**: Medidas são consideradas boas pela ANSOL
-  **1**: Medidas são consideradas razoáveis, mas podiam ser melhores ou não estão suficientemente explícitas
- **\-**: Não há referência ao tema
- **-1**: Medidas são consideradas perigosas, mas podiam ser piores
- **-2**: Medidas são consideradas perigosas

Foram omitidos os partidos sem pontos relevantes para a ANSOL ou cujo programa
não está disponível.

<style>
  table { max-width: 100%; margin-top: 2em; margin-bottom: 2em; }
  table thead th { text-align: center; }
  table tbody th { text-align: left; padding: 0 1em; }
  table tfoot th:first-child { text-align: left; padding: 0 1em; }
  table tbody td { text-align: center; min-width: 3em; }
  table tbody tr:nth-child(2n + 1) { background-color: #eee; }

  table td.n { background-color: #ffdddd; }
  table td.p { background-color: #cceecc; }
</style>

<table>
<thead>
<tr>
  <th></th>
  <th>PS</th>
  <th>AD</th>
  <th>IL</th>
  <th>CDU</th>
  <th>BE</th>
  <th>PAN</th>
  <th>Livre</th>
  <th>MPT.A</th>
  <th>Volt</th>
</tr>
</thead>
<tbody>
  <!--                                                         PS                   AD                   IL                   CDU                  BE                   PAN                  L                   MPT.A                Volt -->
  <tr><th>Dinheiro Público, Código Público!</th> <td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Software Livre</th>                    <td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 1</td><td class='p'> 2</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Neutralidade da Internet</th>          <td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Interoperabilidade/RNID</th>           <td class='p'> 1</td><td class='p'> 1</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Dados abertos</th>                     <td class='z'> -</td><td class='p'> 1</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 1</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>DRM</th>                               <td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 1</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Voto Electrónico</th>                  <td class='z'> -</td><td class='n'>-2</td><td class='z'> -</td><td class='z'> -</td><td class='n'>-1</td><td class='z'> -</td><td class='n'>-1</td><td class='n'>-2</td><td class='n'>-2</td></tr>
  <tr><th>Protecção de dados/Privacidade</th>    <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
</tbody>
<tfoot>
  <tr><th>Total</th>                                       <th> 1</th>          <th> 0</th>          <th> 4</th>          <th> 5</th>          <th> 7</th>          <th> 6</th>          <th>11</th>          <th>-2</th>          <th> 8</th></tr>
</tfoot>
</table>

**Legenda:**

**Dinheiro Público, Código Público:** Todo o código financiado por dinheiro
público deve ser disponibilizado ao público com uma licença de Software Livre.

**Software Livre:** Referências à utilização de Software Livre e os seus
benefícios.

**Neutralidade da Internet:** Garantia de que os operadores tratam de forma
igual todo o tráfego online e que não há limitação de equipamentos que podem
ser utilizados.

**Interoperabilidade/RNID:** O RNID deveria ter sido revisto até outubro de
2021. Deve ser feita uma actualização tecnológica e adicionados mecanismos para
que o regulamento seja cumprido.

**Dados abertos:** Disponibilização de dados públicos com licenças livres para
poderem ser reutilizados por cidadãos e entidades. Produção científica e/ou
educativa deve ser publicada em acesso aberto.

**DRM:** Eliminação do DRM.

**Voto electrónico:** Não devem ser feitos testes piloto sem estudos prévios.
Qualquer teste deve utilizar Software Livre e disponibilizado ao público.

**Protecção de dados/Privacidade:** Medidas que promovem a privacidade dos
cidadãos em ambiente digital relativamente às empresas tecnológicas.

[cnpd-voto]: https://www.cnpd.pt/comunicacao-publica/noticias/voto-eletronico-cnpd-defende-rigoroso-escrutinio/

