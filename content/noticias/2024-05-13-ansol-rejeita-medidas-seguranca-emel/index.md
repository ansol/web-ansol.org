---
layout: article
title: ANSOL rejeita "medidas de segurança" da EMEL
date: 2024-05-13
image:
  caption: |
    ["Lisbon", por Neil](https://commons.wikimedia.org/wiki/File:Lisbon_(45923876392).jpg), sob a licença CC BY-SA 2.0.
tags:
- Public Money? Public Code!

---

Dois grupos de estudantes criaram duas aplicações, a
[Gira+](https://github.com/rt-evil-inc/gira), que é Software Livre, e a
[mGira](https://mgira.pt/), para poderem utilizar as bicicletas GIRA, geridas
pela EMEL, uma vez que a aplicação da própria EMEL deixou de ter suporte e, por
consequência, passou a ter problemas de utilização. As duas aplicações foram
bem recebidas pela comunidade de utilizadores das bicicletas.

Recentemente, [a EMEL contratou uma empresa para actualizar a aplicação e a sua
infraestrutura](https://www.base.gov.pt/Base4/pt/detalhe/?type=contratos&id=10695934),
com o objectivo de impedir a utilização do serviço GIRA por parte de terceiros,
e assim bloquear o funcionamento daquelas aplicações.

O caderno de encargos apresentado pela EMEL menciona duas categorias de
actualizações: melhorias à experiência de utilização, e medidas de segurança.
Já a proposta apresentada pela empresa contratada apenas se foca nas medidas de
segurança, tendo as melhorias à experiência de utilização sido deixadas para
uma fase posterior. Esta ordem de prioridades fará com que os utilizadores das
bicicletas sejam prejudicados: não vêem a aplicação oficial melhorada, e passam
a ser impedidos de utilizar as alternativas. Além disso, as medidas de
segurança apresentadas são baseadas em estratégias que muito preocupam a ANSOL,
por irem contra os princípios do Software Livre e da liberdade de escolha dos
utilizadores. Por exemplo:

* SSL Pinning - Serve principalmente para impedir os utilizadores de
  inspeccionarem as comunicações que a aplicação faz com os servidores. A
  própria Google desaconselha esta prática nos seus guias de desenvolvimento de
  Android, e a [técnica equivalente nos navegadores de internet foi
  descontinuada há 4
  anos](https://en.wikipedia.org/wiki/HTTP_Public_Key_Pinning);
* Ofuscação de código - Vai contra a liberdade de estudarmos o software que
  corre nos nossos próprios dispositivos, e não traz benefícios de segurança
  para o utilizador, apenas escondendo quaisquer falhas de segurança que a
  aplicação possa ter;
* Validação de APP genuína - Serve para implementar DRM, que a ANSOL [tem
  combatido](https://drm-pt.info) desde a sua criação. É um mecanismo que
  impede a utilização da aplicação em telemóveis que tenham *root*, em
  telemóveis cujos utilizadores não queiram usar os serviços da loja da Google
  ou da Apple, ou em aparelhos com sistemas operativos que a EMEL não suporte
  explicitamente.

Tais medidas, que a EMEL classifica como *"de segurança"*, não trazem
benefícios de segurança para os utilizadores do serviço, e têm como único
objectivo impedir que os utilizadores estudem o comportamento das aplicações
que estão instaladas nos seus próprios dispositivos, o que vai contra o que a
ANSOL tem defendido e considera boas prácticas. Software desenvolvido com
dinheiro público, como é o caso do software desenvolvido pela EMEL, deve ser
ele [próprio público](https://publiccode.eu/pt/), com licenças livres.

Para além de querer bloquear as aplicações não oficiais, a EMEL prepara-se para
impedir os utilizadores de usarem a aplicação oficial quando estes escolhem
usar software livre nos seus equipamentos. O foco aparenta estar no bloqueio da
utilização legítima do serviço GIRA e não na melhoria da experiência e
usabilidade do serviço.

A ANSOL convida a EMEL a repensar a sua abordagem na actualização da aplicação,
e continuará a acompanhar os desenvolvimentos desta situação.


