---
layout: article
title: Software Livre nos programas eleitorais 2022
layout: article
date: 2022-01-19
image:
  caption: |
    Imagem: Public Money, Public Code,
    por [Free Software Foundation Europe](https://fsfe.org/contribute/spreadtheword#pmpc),
    sob a licença [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
tags:
- Public Money? Public Code!

---

Apenas a alguns dias das **eleições legislativas portuguesas de 2022**, é
importante perceber quais as posições dos partidos que contestarão os lugares
na Assembleia da República no que toca a Software Livre e Direitos Digitais.
Analisámos os programas disponíveis e apresentamos aqui um resumo dos pontos
mais relevantes.

<!--more-->

Algumas siglas utilizadas no artigo:

* CNPD: Comissão Nacional de Proteção de Dados
* RNID: Regulamento Nacional de Interoperabilidade Digital
* CNCS: Centro Nacional de Cibersegurança
* RGPD: Regulamento Geral de Proteção de Dados
* DRM: Digital Rights Management / Gestão de Direitos Digitais, ou, mais
  corretamente, Digital Restrictions Management / Gestão Digital de Restrições
* TJUE: Tribunal de Justiça da União Europeia

Defendemos que todo o código financiado por dinheiro público deve ser Software
Livre (recomendamos que visitem a página [Public Money? Public
Code!](https://publiccode.eu/pt/)). Defendemos o direito à privacidade nas
comunicações dos cidadãos e somos contra o experimentalismo não fundamentado de
voto eletrónico ou online. Para conhecer melhor os ideais defendidos pela
ANSOL, sugerimos que leiam o nosso artigo ["10 ideias para a próxima
Legislatura"](https://ansol.org/noticias/2022-01-03-dez-ideias-para-a-proxima-legislatura/).

**Actualização 21-01-2022:** Fizemos pequenas edições ao texto, no sentido de o
tornar mais claro. As versões anteriores estão acessíveis no [nosso
repositório](https://gitlab.com/ansol/web-ansol.org).

**Actualização 28-01-2022:** Adicionamos uma tabela de resumo ao fim do artigo
para tornar a análise dos programas mais clara.


## Partido Socialista

**Resumo do programa: Sem referências a Software Livre.** Propõem reforçar o
serviço Dados.Gov com mais oferta de dados, incluindo dados em tempo real.
Propõem generalizar a experiência de voto eletrónico presencial feita em Évora,
sem referência a Software Livre ou às críticas da CNPD. Propõem definir um
programa nacional de ensino da computação desde o ensino básico, referindo a
literacia e ética digitais mas sem referência a Software Livre. Propõem a
digitalização dos manuais escolares sem referência a licenças abertas.

**Comentário da ANSOL:** A generalização da experiência feita em Évora é
preocupante, especialmente dadas [as críticas feitas pela CNPD][cnpd-evora]. A
criação de um programa de ensino da computação pode prender várias gerações às
empresas responsáveis pelo software proprietário utilizado, sendo crucial a
inclusão de Software Livre nesta iniciativa. Além da utilização de Software
Livre neste programa, é importante que seja ensinado o papel dos direitos de
autor na criação de software e as diferenças entre software proprietário e
software livre.


## Partido Social Democrata

**Resumo do programa: Sem referências a Software Livre.** Propõem lançar uma
iniciativa nacional de dados abertos capaz de incluir a construção de modelos
de negócio para o sector privado.

**Comentário da ANSOL:** O programa não toca em direitos digitais para além da
cobertura nacional do acesso a comunicações. Não é claro qual a diferença entre
o programa “Portugal.Dados” que propõem criar e a plataforma existente de dados
abertos do governo (Dados.Gov).


## Bloco de Esquerda

**Resumo do programa: "O software criado ou comprado com dinheiro dos
contribuintes deve ser software livre ou de código aberto, permitindo a
reutilização pelas várias entidades da Administração Pública"**. Defendem a
neutralidade da internet. Propõem o fim do DRM e o fim da criminalização da
partilha de conteúdos para fins não comerciais. Defendem que a produção
científica com dinheiros públicos deve ser obrigatoriamente depositada em
repositórios abertos. Defendem o teste de voto eletrónico à distância com “a
participação de especialistas de segurança das Universidades portuguesas,
utilização de código aberto e amplo escrutínio público”. Propõem o fim da taxa
de cópia privada.

**Comentário da ANSOL:** O programa aborda positivamente várias questões que
julgamos importantes, sendo de destacar a obrigatoriedade de Software Livre
quando criado ou comprado com dinheiro público. Os requisitos para o teste ao
voto eletrónico à distância são correctos, mas o projecto deveria começar por
um estudo sobre o desenho e implementação do processo. Ao começar pelo teste,
condiciona-se a desvalorização das críticas que poderiam por em causa a sua
exequibililidade, como aconteceu no projeto-piloto de Évora, [fortemente
criticado pela CNPD][cnpd-evora].


## CDU (PCP-PEV)

**Resumo do programa: Sem referências a Software Livre.** São contra a censura
e a hipervigilância no contexto de direitos digitais, defendem a neutralidade
da internet e a não criminalização da partilha de conteúdos para fins não
comerciais.

**Comentário da ANSOL:** Não fazem qualquer referência a Software Livre, tendo
apenas algumas referências a direitos digitais.


## PAN

**Resumo do programa: "garantir que o software desenvolvido pelo Estado, ou
cujo desenvolvimento é financiado por recursos públicos, tem o seu código
público, ou seja, é software livre, excepcionando situações devidamente
justificadas"**. Propõem avaliar a migração de todo o software de agências
governamentais e entidades públicas para software livre. Propõem reforçar os
meios da CNPD e CNCS. Propõem garantir a neutralidade da Internet, proibindo as
ofertas de zero-rating. Propõem reforçar o investimento do Estado em
infraestrutura própria.

**Comentário da ANSOL:** Gostávamos de ver o primeiro ponto sem as exceções
“devidamente justificadas”, mas pode ser um bom primeiro passo.


## Iniciativa Liberal

**Resumo do programa: "Reduzir o custo total de propriedade recorrendo ao uso
de especificações abertas, como o software Open Source"**. Propõem publicar
todos os dados de compras públicas de bens e serviços num formato standard
único e aberto. Propõem a criação de um datacenter operado pelo Estado para
albergar a sua infraestrutura. Propõem implementar uma estratégia de gestão de
risco e cibersegurança em todas as iniciativas TIC para assegurar a segurança e
privacidade digital. Propõem a neutralização do impacto da taxa de cópia
privada, reduzindo a taxa em 50% e estreitando a base de incidência ao reduzir
a lista de tipos de equipamentos sujeitos, mas também propõem eliminar todas as
isenções existentes.

**Comentário da ANSOL:** A recomendação de utilização de Open Source é
positiva, mas parece haver alguma confusão técnica ao subentender que Open
Source é um tipo de especificação aberta. Em relação à taxa de cópia privada, a
redução da taxa e da base de tributação vão no sentido positivo, mas a
eliminação das isenções tem impacto negativo. A eliminação das isenções remove
os poucos mecanismos que as empresas têm para evitar a taxa, [apesar de haver
decisões pelo TJUE que indicam que a taxa não deve ser aplicada a
empresas][tjue-sgae].


## Livre

**Resumo do programa: "Construir Bens Digitais Comuns, garantindo que todo o
código desenvolvido com dinheiro público fique numa licença de código
aberto"**. Propõem limitar a utilização de software proprietário na educação
apenas a casos onde Software Livre não seja adequado. Propõem introduzir
Software Livre em todos os níveis da administração pública e instituições
financiadas com recursos públicos. Propõem a criação de livros escolares com
licença de autor aberta (Creative Commons). Defendem o direito à fabricação e
reparação. Propõem reforçar o papel da CNPD e promover a encriptação de todas
as comunicações. Propõem estender o RNID para que sítios oficiais não dependam
de serviços terceiros, aceitação de termos estranhos à finalidade do serviço, e
não tenham rastreamento durante a interação com o serviço. Propõem participar
na construção do Contract for the Web. Propõem melhorar as condições da votação
por correspondência e investir no desenvolvimento e experimentação de sistemas
de voto eletrónico à distância.

**Comentário da ANSOL:** O programa aborda positivamente várias questões que
julgamos importantes, sendo de destacar o licenciamento de código aberto quando
desenvolvido com dinheiro público e a utilização de Software Livre na
Administração Pública.


## Partido da Terra (MPT)

**Resumo do programa: Sem referências a Software Livre.** Propõem promover e
implementar o voto eletrónico à distância como alternativa ao voto presencial
para todos os nacionais, em todos os atos eleitorais, sem referência a Software
Livre.

**Comentário da ANSOL:** Não fazem qualquer referência a Software Livre.
Promover a implementação do voto eletrónico à distância sem mencionar a
necessidade de estudos preliminares é cair nos mesmos erros que aconteceram no
projeto-piloto em Évora, onde, segundo a CNPD, [ficaram "[feridos] os mais
básicos princípios do Estado de Direito Democrático, com menosprezo pelos
princípios da previsibilidade e da transparência do processo
eleitoral"][cnpd-evora].


## Volt Portugal

**Resumo do programa: "Projetos financiados com dinheiro público deverão ter o
código desenvolvido num local de acesso público, excepto em situações onde o
segredo de justiça/defesa nacional seja aplicável"**. Propõem, nos serviços
públicos, substituir faseadamente o software utilizado por Software Livre,
padronizar as ferramentas de produtividade e garantir ações de formação.
Propõem, na educação, substituir e promover Software Livre, incluindo uma
solução para videoconferências cifradas e seguras. Propõem tornar a CNPD numa
secretaria de estado com poderes de informação e fiscalização em matérias de
dados pessoais e direitos online, com pareceres vinculativos e sanções nos
incumprimentos das suas normas. Propõem a adoção do voto eletrónico à
distância, depois de testes com a participação de especialistas de segurança
das Universidades com o objetivo de ter um sistema seguro, anónimo,
verificável, fiável, e acessível, mas sem referência a Software Livre.

**Comentário da ANSOL:** Apesar de mencionarem que o código financiado com
dinheiro público deve ser disponibilizado ao público, não dizem explicitamente
que o código deve ser licenciado como Software Livre.


## Restantes partidos

Os restantes partidos ou não tinham disponível o programa eleitoral para 2022,
ou não faziam qualquer referência a Software Livre ou direitos digitais.

[cnpd-evora]: https://www.cnpd.pt/comunicacao-publica/noticias/voto-eletronico-cnpd-defende-rigoroso-escrutinio/
[tjue-sgae]: https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:62014CJ0470&from=en


## Tabela de resumo

Para tornar a análise aos programas dos vários partidos mais clara, decidimos
acrescentar uma tabela onde se reunem os pontos mais importantes para a ANSOL,
atribuindo pontos positivos ou negativos consoante a seguinte classificação:

-  **2**: Medidas são consideradas boas pela ANSOL
-  **1**: Medidas são consideradas razoáveis, mas podiam ser melhores ou não estão suficientemente explícitas
- **\-**: Não há referência ao tema
- **-1**: Medidas são consideradas perigosas, mas podiam ser piores
- **-2**: Medidas são consideradas perigosas

<style>
  table { max-width: 100%; margin-top: 2em; margin-bottom: 2em; }
  table thead th { text-align: center; }
  table tbody th { text-align: left; padding: 0 1em; }
  table tbody td { text-align: center; min-width: 3em; }
  table tbody tr:nth-child(2n + 1) { background-color: #eee; }

  table td.n { background-color: #ffdddd; }
  table td.p { background-color: #cceecc; }
</style>

<table>
<thead>
<tr>
  <th></th>
  <th>PS</th>
  <th>PSD</th>
  <th>BE</th>
  <th>CDU</th>
  <th>PAN</th>
  <th>IL</th>
  <th>Livre</th>
  <th>MPT</th>
  <th>Volt</th>
</tr>
</thead>
<tbody>
  <tr><th>Dinheiro Público, Código Público!</th><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 1</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Neutralidade da Internet</th>         <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Dados Abertos</th>                    <td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Acesso Aberto</th>                    <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>DRM</th>                              <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 1</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Partilha sem fins comerciais</th>     <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Voto Electrónico</th>                 <td class='n'>-2</td><td class='z'> -</td><td class='n'>-1</td><td class='z'> -</td><td class='z'> -</td><td class='n'>-1</td><td class='n'>-1</td><td class='n'>-2</td><td class='n'>-1</td></tr>
  <tr><th>Taxa da cópia privada</th>            <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 1</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>RNID</th>                             <td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td></tr>
  <tr><th>Bloqueio de sites</th>                <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td></tr>
  <tr><th>Hipervigilância/Privacidade</th>      <td class='z'> -</td><td class='z'> -</td><td class='p'> 2</td><td class='p'> 2</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td><td class='z'> -</td><td class='p'> 2</td></tr>
</tbody>
<tfoot>
  <tr><th>Total</th>                            <th> 0</th><th> 2</th><th>15</th><th> 8</th><th> 5</th><th> 4</th><th>12</th><th>-2</th><th> 7</th></tr>
</tfoot>
</table>

**Legenda:**

**Dinheiro Público, Código Público:** Todo o código financiado por dinheiro
público deve ser disponibilizado ao público com uma licença de Software Livre.

**Neutralidade da Internet:** Garantia de que os operadores tratam de forma
igual todo o tráfego online.

**Dados abertos:** Disponibilização de dados públicos com licenças livres para
poderem ser reutilizados por cidadãos e entidades

**Acesso aberto:** Produção científica e/ou educativa deve ser publicada em
acesso aberto

**DRM:** Eliminação do DRM

**Partilha sem fins comerciais:** Descriminalização ou legalização da partilha
de ficheiros sem fins comerciais

**Voto electrónico:** Não devem ser feitos testes piloto sem estudos prévios.
Qualquer teste deve utilizar Software Livre e disponibilizado ao público.

**Taxa da cópia privada:** Eliminação da taxa da cópia privada

**Regulamento Nacional de Interoperabilidade Digital:** Deveria ter sido
revisto até outubro de 2021. Deve ser feita uma actualização tecnológica e
adicionados mecanismos para que o regulamento seja cumprido.

**Contra o bloqueio extra-judicial de websites:** Devem ser revogadas a Lei e o
Memorando de entendimento que colocam entidades não judiciais a declarar
websites como infractores do Direito de Autor.

**Hipervigilância/Privacidade:** Medidas que promovem a privacidade dos
cidadãos em ambiente digital relativamente às empresas tecnológicas
