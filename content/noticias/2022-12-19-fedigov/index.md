---
categories:
- imprensa
layout: article
title: Fedigov - Comunicação federada nas instituições públicas
date: 2022-12-19
tags:
- Fedigov

---

*A ANSOL lança [uma campanha][fedigov] para encorajar as instituições públicas a garantir
a soberania das suas comunicações.*

<!--more-->

<div style="text-align: center">
<b>Comunicado de Imprensa</b>

Associação Nacional para o Software Livre - ANSOL

19 de dezembro de 2022
</div>

As redes sociais têm um papel cada vez mais importante nas instituições
públicas. Permitem que haja um canal de comunicação directo com o público e que
informação relevante seja disseminada em tempo útil.

Na maiora dos casos, são usadas as plataformas mais conhecidas, como o Twitter,
Facebook, YouTube ou Instagram. Pessoas que queiram usar a internet de forma
auto-determinada e privada ficam excluídas de aceder a estas comunicações. O
uso destes serviços por parte das instituições públicas serve indirectamente de
publicidade a estas empresas e incentiva a sua utilização.

Hoje em dia há alternativas, como o Fediverso, que estão a crescer em
popularidade e que já são utilizadas por várias instituições públicas
europeias. O Fediverso é uma rede de várias aplicações independentes, como o
Mastodon, que comunicam entre si usando um protocolo comum. As pessoas têm a
opção de operar o seu próprio servidor ou de se juntarem a servidores
existentes.

Ao se juntarem ao Fediverso, as instituições públicas passam a contribuir para
uma rede de comunicação independente e soberana, livre da influência e controlo
das megacorporações que gerem as redes sociais privadas. O público deixa de ser
obrigado a ceder a sua informação pessoal a estas entidades para poder
acompanhar as comunicações das instituições públicas.

Com a campanha FediGov, a ANSOL quer aumentar a sensibilização para este
problema e oferecer tanto ao público como às instituições oportunidades
concretas. O público é incentivado a contactar as instituições públicas para
pedir que divulguem as suas comunicações no Fediverso. Às instituições públicas
é oferecida informação sobre redes sociais descentralizadas baseadas em
Software Livre.

O website da campanha é [https://fedigov.pt][fedigov].

<br>
<br>
<br>

[fedigov]: https://fedigov.pt
