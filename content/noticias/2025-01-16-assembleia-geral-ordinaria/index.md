---
categories:
- assembleia geral
layout: article
title: Assembleia Geral 2025 Ordinária
date: 2025-01-16
---

A assembleia terá lugar na Sede FAJDP – Casa das Associações, no dia 18
de Janeiro de 2025, com início às 14h30 da tarde, com a seguinte ordem
de trabalhos:

1. Apresentação e aprovação do relatório e contas de 2024
2. Apresentação e aprovação do plano de atividades para 2025
3. Outros

Se à hora marcada não estiverem presentes, pelo menos, metade dos
associados a Assembleia Geral reunirá, em segunda convocatória, no mesmo
local e passados 30 minutos, com qualquer número de presenças.

Os documentos apresentados estão disponíveis na nossa página de [Documentação e
Transparência](/transparencia).
