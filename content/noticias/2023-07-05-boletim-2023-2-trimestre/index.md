---
categories:
- newsletter
- boletim
layout: article
title: Boletim 2023 - 2º trimestre
date: 2023-07-05
---

Este trimestre fizemos a nossa Assembleia Geral anual, participámos na consulta
pública sobre a transposição da directiva de direitos de autor, na consulta
pública sobre acesso aberto da FCT, e apoiámos a carta aberta para facilitar o
acesso às colecções museológicas nacionais.

Participámos na WikiCon, no Dia da Internet, e organizámos uma festa de
lançamento da nova versão de Debian.

Temos também trabalhado na Festa do Software Livre 2023, já com data e local
anunciados. Por último, o tesoureiro da direcção foi substituido.

<!--more-->

## Festa do Software Livre 2023

A data e o local da Festa do Software Livre 2023, um evento organizado pela ANSOL, já foram anunciados:

<p style="text-align: center">
  <strong>15 a 17 de setembro de 2023 na Universidade de Aveiro</strong>
</p>

Temos também já uma agenda provisória com as várias comunidades que vão estar presentes. Toda esta informação está disponível no site oficial do evento:

<div style="text-align: center">
  <a href="https://festa2023.softwarelivre.eu/">https://festa2023.softwarelivre.eu/</a>
</div>


## Assembleia Geral Ordinária 2023

Dia 29 de abril reunimos na Universidade de Aveiro. Apresentámos e discutimos o
relatório de actividades e contas de 2022 e aprovámos o orçamento para 2023.

Obrigado ao [GLUA](https://glua.ua.pt) e à [Universidade de
Aveiro](https://www.ua.pt) por nos ceder o espaço.


## Consulta pública sobre direitos de autor

Participámos na consulta pública ao Decreto-Lei que transpõe a Directiva
Europeia que actualia as matérias de Direito de Autor no âmbito do Mercado
Único Digital.

Temos um resumo do nosso contributo, assim como o documento completo, na
[notícia publicada no nosso
site](https://ansol.org/noticias/2023-05-06-consulta-publica/).


## Consulta pública sobre acesso aberto da FCT

A ANSOL participou na consulta pública sobre a Política de Acesso Aberto da
FCT. Neste contributo, que pode ser [lido na
íntegra](https://ansol.org/noticias/2023-05-28-consulta-publica-fct/ConsultaPublicaAcessoAberto.pdf),
a ANSOL focou-se em duas sugestões:

* Permitir explicitamente a utilização de licenças livres de software (desde
  que aprovadas pela FSF ou pela OSI) para os casos em que as publicações
  incluam código;
* Recomendar licenças Creative Commons copyleft (Share-Alike) em vez das suas
  alternativas permissivas.


## Carta aberta: Medidas para facilitar o acesso às colecções museológicas nacionais

A ANSOL participou na escrita e assinatura de uma carta aberta enviada no dia 1
de junho ao Ministério da Cultura a solicitar que se reconheça o domínio
público e se tomem medidas para abrir as colecções das instituições de
património cultural.

A [carta pode ser lida no site da Wikimedia Portugal](https://blog.wikimedia.pt/2023/06/02/carta-aberta-medidas-para-facilitar-o-acesso-as-coleccoes-museologicas-nacionais/).

O Ministério da Cultura respondeu agradecendo o interesse e informando que se
encontra a fazer uma revisão dos regulamentos, que "levará atempadamente ao
conhecimento público".


## WikiCon Portugal 2023

A ANSOL esteve na [WikiCon Portugal
2023](https://pt.wikimedia.org/wiki/WikiCon_Portugal_2023), onde se celebrou o
Dia da Liberdade Documental com uma conversa-debate com a participação da ANSOL
por parte de Marcos Marado, que fez uma apresentação sobre a festa do Software
Livre. Foram ainda realizados contactos no sentido de se estreitarem relações
em projetos futuros entre as duas associações.


## Dia da Internet 2023

A Wikimedia e a ANSOL organizaram uma conversa-convívio online e informal para
celebrar o [Dia da Internet no dia 17 de maio](https://pt.wikimedia.org/wiki/Dia_da_Internet_2023).


## Festa de lançamento do Debian Bookworm

Para celebrar o lançamento do Debian Bookworm,
[a ANSOL organizou um almoço no Restaurante Tipico do Mezio](https://wiki.debian.org/ReleasePartyBookworm/Portugal/Lamego),
seguido de uma visita ao Centro de Artesanato do Mezio.


## Mudança de Tesoureiro

O Octávio Gonçalves comunicou à direcção a sua rescisão como sócio da ANSOL.
Tendo o cargo de Tesoureiro ficado vago, a Direcção procurou voluntários para
preencher a vaga.

Dia 31 de maio a direcção reuniu e deliberou, de acordo com o ponto 4 do artigo
9.º dos Estatutos, nomear o sócio André Freitas como Tesoureiro, com efeitos
imediatos.

A direcção agradece ao Octávio todo o trabalho que desenvolveu como Tesoureiro
e ao André Freitas por se disponibilizar a preencher a vaga.


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
