---
layout: article
categories:
- newsletter
- boletim
title: Boletim 2024 - 2º trimestre
date: 2024-12-18
---


#### ANSOL rejeita "medidas de segurança" da EMEL

Acompanhámos e criticámos o processo de introdução de supostas medidas de
segurança no serviço Gira+. Alguns links relacionados:

- <https://ansol.org/noticias/2024-05-13-ansol-rejeita-medidas-seguranca-emel/>
- <https://lisboaparapessoas.pt/2024/05/15/emel-melhorar-aplicacao-gira-terceiros/>


#### Artigos da PC Guia

Continuamos a publicar dois artigos por mês na versão em papel da PCGuia. Três
dos artigos do trimestre (os tutoriais) estão disponíveis online:

- <https://www.pcguia.pt/2024/06/mais-que-uma-consola-um-ninho-de-software-livre/>
- <https://www.pcguia.pt/2024/07/jogos-de-software-livre/>
- <https://www.pcguia.pt/2024/08/usar-o-ardour-para-fazer-producao-de-audio/>

Alguns dos artigos têm entrevistas a acompanhar, que podem ser vistas no
nosso canal de Peertube: <https://viste.pt/c/ansol/videos>


#### Análise dos candidatos ao Parlamento Europeu

Como já é hábito, analisámos os programas dos partidos nos temas relacionados
com Software Livre e foi criado um conjunto de perguntas que foram enviadas aos
partidos candidatos sobre os temas mais prementes em discussão na União
Europeia, que incluiam a Regulação de Patentes Essenciais a Normas, a
Criptografia de Ponta-a-Ponta (E2EE), Dados Abertos, Acesso Aberto e Recursos
Educacionais Abertos, Soberania Digital, e Ecologia e Sustentabilidade. As
respostas foram publicadas aqui:

- <https://ansol.org/noticias/2024-06-04-software-livre-nas-eleicoes-europeias/>


#### WikiCon Portugal 2024

A ANSOL participou na WikiCon Portugal 2024, evento organizado pela Wikimedia
PT:

- <https://pt.wikimedia.org/wiki/WikiCon_Portugal_2024>


#### Dia da Internet

Co-organizámos um evento para celebrar a data:

- <https://pt.wikimedia.org/wiki/Dia_da_Internet_2024>


#### A ANSOL continua a acompanhar a proposta do ChatControl

Assinou uma carta aberta em resposta a desenvolvimentos pela Presidência Belga
no Conselho. A carta pode ser lida aqui:

- <https://edri.org/our-work/open-letter-mass-surveillance-and-undermining-encryption-still-on-table-in-eu-council/>


#### É o MP3 uma norma aberta?

A ANSOL recebeu uma pergunta sobre se o mp3 seria ou não uma norma aberta e
publicou um texto com a análise na secção de recursos. O site do RNID, mantido
pela ANSOL, também foi actualizado com essa informação:

- <https://ansol.org/recursos/>
- <https://rnid.ansol.org/>


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
