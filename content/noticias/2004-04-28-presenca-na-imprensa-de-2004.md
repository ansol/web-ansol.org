---
categories:
- imprensa
- '2004'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 23
  node_id: 150
layout: page
title: Presença na Imprensa de 2004
created: 1083152922
date: 2004-04-28
aliases:
- "/imprensa/2004/"
- "/node/150/"
- "/page/150/"
---
<ul><li><p class="line891"><em>O que é o software livre afinal?</em>, Artigo de João Miguel Neves, PCGuia, nº 101, Abril de 2004.</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/computadores/portugueses_juntam_se_a_protestos_europeus_co_871817.html" class="http">Portugueses juntam-se a protestos europeus contra a patenteabilidade de software</a></em>, Publicado por Casa dos Bits às 14.36h no dia 12 de Maio de 2004</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/computadores/ansol_quer_sensibilizar_governo_portugues_con_871818.html" class="http">Ansol quer sensibilizar Governo português contra patenteabilidade de software</a></em>, Publicado por Casa dos Bits às 14.29h no dia 13 de Maio de 2004</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/computadores/conselho_europeu_define_posicao_comum_relativ_871822.html" class="http">Conselho Europeu define posição comum relativa à patenteabilidade do software</a></em>, Publicado por Casa dos Bits às 09.51h no dia 19 de Maio de 2004</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/negocios/governo_holandes_recua_no_apoio_a_directiva_e_875453.html" class="http">Governo holandês recua no apoio à directiva europeia para a patenteabilidade de software</a></em>, Publicado por Casa dos Bits às 17.18h no dia 02 de Julho de 2004</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/computadores/presidencia_europeia_adia_adopcao_do_regime_d_871932.html" class="http">Presidência europeia adia adopção do regime de patentes para o software</a></em>, Publicado por Casa dos Bits às 11.08h no dia 28 de Setembro de 2004</p></li></ul>
