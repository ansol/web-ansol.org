---
categories:
- Consulta Pública
- Estratégia Digital Nacional
layout: article
title: ANSOL participa em Consulta Pública da Estratégia Digital Nacional
date: 2024-12-05
---

O Gabinete da Ministra da Juventude e Modernização abriu uma consulta pública sobre a Estratégia Digital Nacional de 21 de Novembro a 5 de Dezembro de 2024 disponível na plataforma [ConsultaLEX](https://www.consultalex.gov.pt/Portal_Consultas_Publicas_UI/ConsultaPublica_Detail.aspx?Consulta_Id=356). Sendo um tema do interesse público que deveria incluir a adopção de Software Livre, a ANSOL decidiu participar e submeter as seguintes considerações:

# Estratégia Digital Nacional

A ANSOL - Associação Nacional para o Software Livre considera que a adopção de Software Livre é essencial para a Estratégia Digital Nacional.

Software Livre é aquele que dá a todos os utilizadores o direito de usar, estudar, adaptar e partilhar o Software. Estes direitos são a base para atingirmos direitos fundamentais como a liberdade de expressão e privacidade no mundo digital. 

A utilização de Software Livre na Administração Pública é também necessário para atingir Soberania Digital, garantindo que não ficamos reféns de empresas Big Tech.


## Princípios orientadores

Uma Estratégia Digital Nacional que cumpra os princípios orientadores identificados tem necessariamente de identificar e abordar a temática de Software Livre.

Confiança e Transparência: Para haver confiança e transparência as ferramentas tecnológicas que adoptamos têm de ser disponibilizadas de forma a serem analisadas por qualquer entidade, seja cidadão, associação, empresa, academia, entre outros;
    
Inclusão e Igualdade: A utilização de Software Livre nas tecnologias permite também que vários grupos possam adaptar as tecnologias às suas necessidades particulares, descentralizando este processo. Para garantir o acesso universal, as tecnologias têm de ser acessíveis a todos, incluindo quem não utiliza software proprietário no dia a dia. O governo disponibiliza várias aplicações que apenas são instaláveis por quem depende dos serviços da Apple e da Google. Estas aplicações deveriam também ser disponibilizadas pelas entidades públicas, sem qualquer intermediário.

Sustentabilidade: O uso de Software Livre na transição digital garante maior sustentabilidade devido à reutilização do mesmo software e código para o software usado em diversas instituições. A sua utilização permite também que desenvolvedores em diferentes entidades possam corrigir bugs e adicionar funcionalidades que afetam positivamente utilizadores desse software noutras entidades. 

Segurança e Proteção: A utilização de Software Livre permite que sejam feitas análises de segurança independentes, permitindo que as falhas de segurança sejam detectadas e corrigidas mais rapidamente. 

Ética: Software Livre na Administração Pública é um requisito para que os direitos dos cidadãos sejam respeitados, podendo estes verificar que os seus dados são devidamente tratados, assim como resultados de decisões automáticas usando os mesmos. A disponibilização aberta dos dados e métodos usados para treinar modelos de linguagem em grande escala (LLMs) garante que se respeite os dados cidadãos e evita potenciais enviesamentos dos seus resultados. 

Eficiência: Ao adoptar Software Livre, garantimos que não ficamos presos a um único fornecedor de Software, evitando monopólios e permitindo que haja mais concorrência. O Software também permite que a mesma solução seja adaptada e utilizada por várias entidades. Tudo isto leva a uma maior optimização dos recursos investidos no processo de digitalização do estado.

Colaboração: A criação de Software Livre é pela sua natureza um processo colaborativo. Como pode ser modificado e utilizado sem restrições, várias entidades de diversos sectores podem colaborar no desenvolvimento das soluções para problemas comuns.

## Análise às iniciativas identificadas nos documentos

> Na área da educação, será feita uma avaliação das competências digitais dos alunos e reforçada a formação em competências digitais desde o ensino básico até ao ensino secundário.

É crucial que a formação em competências digitais não se baseie no ensino de produtos específicos que dificilmente respeitarão os direitos dos seus utilizadores. É prioritário evitar que seja feito o Vendor Lock-in a gerações inteiras. A inclusão de soluções de Software Livre na formação garante que os alunos não ficam com o seu conhecimento inteiramente dependente do sucesso de empresas Big Tech.

> Garantir a digitalização dos serviços públicos e a sua prestação eficiente, integrada e centrada nas pessoas

A digitalização dos serviços públicos é importante, mas não deve deixar para trás aqueles que não dispõem da literacia digital necessária para os utilizar. É preciso garantir que continua a haver investimento nos serviços analógicos / presenciais e que não há degradação dos serviços utilizados por uma parte significativa da população.

> Promover a interoperabilidade plena de sistemas, garantindo a integração dos serviços públicos para uma experiência unificada

A interoperabilidade dos sistemas é crucial, pelo que é importante fazer a actualização necessária ao Regulamento Nacional de Interoperabilidade Digital e à Lei das Normas Abertas, e garantir que as instituições da Administração estão capacitadas para cumprir o mesmo.

> Um dos pilares desta iniciativa será o desenvolvimento de uma infraestrutura de cloud soberana, uma plataforma segura e confiável
destinada a alojar dados fundamentais da Administração Pública. Esta infraestrutura de cloud poderá também oferecer soluções de
armazenamento digital acessível para PMEs com dificuldades no acesso a serviços de computação em nuvem.

A infraestrutura da Administração Pública deve também incluir um serviço de distribuição do Software Livre desenvolvido e utilizado pelas várias instituições públicas, de forma a que este seja mais facilmente encontrado e utilizado por outras entidades, públicas ou privadas. Deve também ser criado um serviço de disponibilização das aplicações móveis criadas pelo governo, de forma a evitar a dependência dos serviços da Apple e da Google no que toca a distribuição de aplicações móveis.

A ANSOL encontra-se disponível para esclarecimentos e/ou cooperação no uso de software livre, normas abertas e dados abertos no âmbito nacional.
