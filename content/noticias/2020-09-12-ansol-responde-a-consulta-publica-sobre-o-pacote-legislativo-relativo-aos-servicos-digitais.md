---
categories:
- dsm
- eu
- Consulta Pública
metadata:
  tags:
  - tags_tid: 328
  - tags_tid: 94
  node_id: 749
layout: article
title: ANSOL responde à consulta pública sobre o pacote legislativo relativo aos serviços
  digitais
created: 1599930012
date: 2020-09-12
aliases:
- "/article/749/"
- "/node/749/"
---
<p>A Comissão Europeia lançou uma consulta pública sobre o pacote legislativo relativo aos serviços digitais, também conhecido por Digital Services Act, que pretende atualizar a Diretiva Europeia do Comércio Eletrónico, aprovada em junho de 2000.</p><p>A posição que a ANSOL transmitiu pode ser sumariada nos seguintes pontos:</p><ul><li><p>A legislação deve manter que os intermediários de serviços em linha não devem ser responsabilizados pelos conteúdos que transmitem;</p></li><li><p>A nova legislação deve passar a permitir e incentivar aquilo que se designa por <strong><a href="https://www.eff.org/deeplinks/2019/10/adversarial-interoperability" title="https://www.eff.org/deeplinks/2019/10/adversarial-interoperability" rel="noopener noreferrer nofollow">interoperabilidade adversarial</a></strong>, passando para um modelo em que os utilizadores possam comunicar e seguir os seus contactos independentemente da plataforma que usam. Tal incentivaria a escolha das plataformas pelas suas funcionalidades, em vez de serem escolhidas por lá estarem os nossos contactos; incentivaria ainda a criação de serviços para fazerem triagem de informação de acordo com as preferências dos utilizadores; diminuiria os "<em>walled gardens</em>"; tornaria muito mais difícil a disseminação de desinformação e discurso de ódio;</p></li><li><p>A legislação deve proibir a publicidade direcionada, com base em <em>profiling</em> e dados pessoais dos utilizadores; as plataformas devem ser obrigadas a mostrarem aos utilizadores todas as informações que determinaram que aquele utilizador esteja a ver aquele conteúdo ou produto;</p></li><li><p>A legislação deve obrigar as plataformas a publicarem os seus algoritmos com uma licença de software livre e de código aberto;</p></li><li><p>O uso de software proprietário por alunos e professores aumentou com a pandemia. Não sendo o código destas plataformas escrutinável, estamos a colocar os dados de alunos e professores em risco de poderem vir a ser usados contra estas pessoas no futuro. A ANSOL apontou o caso do Governo Francês, que disponibilizou um <em>site</em> com software livre, pronto para as escolas usarem, como uma boa prática, que devia ser seguida a nível europeu.</p></li></ul><p>A resposta completa pode ser consultada <a href="https://opencloud.ansol.org/s/XiQq7jP6EQLiFyq" title="https://opencloud.ansol.org/s/XiQq7jP6EQLiFyq">neste documento [PDF]</a>.</p>
