---
categories:
- imprensa
- '2011'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 30
  node_id: 157
layout: page
title: Presença na Imprensa de 2011
created: 1303992182
date: 2011-04-28
aliases:
- "/imprensa/2011/"
- "/node/157/"
- "/page/157/"
---
<ul><li><p class="line862">2011-12-28:&nbsp;<a href="http://www.cnoticias.net/?p=63013" class="http">Coimbra: Universidade acusada de lançar concurso ilegal</a></p></li><li><p class="line862">2011-12-27:&nbsp;<a href="http://www.i-gov.org/index.php?article=17108&amp;visual=1" class="http">ANSOL reclama contra concursos públicos</a></p></li><li><p class="line862">2011-12-27:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ansol_denuncia_ilegalidades_em_concursos_publ_1209977.html" class="http">ANSOL denuncia ilegalidades em concursos públicos</a></p></li><li><p class="line862">2011-10-22:&nbsp;<a href="http://radiofutura.radiozero.pt/?page_id=12&amp;lang=en" class="http">Entrevista da ANSOL na Radio Futura</a></p></li><li><p class="line862">2011-09-30:&nbsp;<a href="http://tek.sapo.pt/opiniao/opiniao_20_anos_de_linux_em_portugal_1189837.html" class="http">Paulo Trezentos fala sobre Linux em Portugal, referindo a ANSOL</a></p></li><li><p class="line862">2011-09-30:&nbsp;<a href="http://tek.sapo.pt/noticias/internet/ansol_alerta_para_perigos_do_tratado_internac_1190008.html" class="http">Ansol alerta para perigos do tratado internacional contra a pirataria</a></p></li><li><p class="line862">2011-09:&nbsp;<a href="http://issuu.com/fossgisbrasil/docs/revista_fossgis_brasil_ed_03_setembro_2011/28?mode=window" class="http">Software Livre no Governo: o caso de Portugal</a>, artigo na terceira edição da revista FOSSIG</p></li><li><p class="line862">2011-05-27:&nbsp;<a href="http://www.defectivebydesign.org/taxonomy/term/428" class="http">Interview with Marcos Marado on the Day Against DRM</a></p></li><li><p class="line862">2011-05-26:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ansol_questiona_partidos_politicos_sobre_soft_1155415.html" class="http">Ansol questiona partidos políticos sobre software livre</a></p></li><li><p class="line862">2011-05-24:&nbsp;<a href="http://aeiou.exameinformatica.pt/opiniao-liberdade-zero=f1009608" class="http">Liberdade Zero</a></p></li><li><p class="line862">2011-05-11:&nbsp;<a href="http://www.archive.org/details/EntrevistaRuiSeabra" class="http">Entrevista sobre DRM na Rádio Ciroma</a></p></li><li><p class="line862">2011-05-04:&nbsp;<a href="http://www.archive.org/details/diaContraODrm2011NaRdioZero" class="http">Entrevista sobre DRM na Rádio Zero</a></p></li><li><p class="line862">2011-04-08:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/lei_das_normas_abertas_gera_expectativas_posi_1143738.html" class="http">Lei das Normas Abertas gera expectativas positivas mas também cautela</a></p></li><li><p class="line862">2011-04-08:&nbsp;<a href="http://tek.sapo.pt/opiniao/lei_das_normas_abertas_a_definicao_nao_e_tao_1143733.html" class="http">Lei das Normas Abertas: "A definição não é tão clara e sólida quanto todas as entidades consultadas recomendaram"</a></p></li><li><p class="line862">2011-04-06:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/lei_das_normas_abertas_aprovada_na_assembleia_1143110.html" class="http">Lei das Normas abertas aprovada na Assembleia da República</a></p></li></ul>
