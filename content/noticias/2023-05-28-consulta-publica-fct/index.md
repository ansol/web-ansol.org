---
categories:
- Consulta Pública
layout: article
title: ANSOL participa em Consulta Pública sobre a Política sobre acesso aberto a publicações científicas resultantes de investigação financiada pela FCT
date: 2023-05-28
---

A ANSOL participou na [consulta pública sobre a Política de Acesso Aberto da FCT](https://www.fct.pt/wp-content/uploads/2023/04/Politica_de_Acesso_Aberto_FCT.pdf).

<!--more-->

Neste contributo, que pode ser [lido na íntegra](ConsultaPublicaAcessoAberto.pdf), a ANSOL focou-se em duas sugestões:

- Permitir explicitamente a utilização de licenças livres de software (desde que aprovadas pela FSF ou pela OSI) para os casos em que as publicações incluam código;
- Recomendar licenças Creative Commons copyleft (Share-Alike) em vez das suas alternativas permissivas.
