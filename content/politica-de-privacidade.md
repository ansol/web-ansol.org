# Política de Privacidade

Para a ANSOL a privacidade de todos é uma questão fundamental, e acreditamos que é necessário limitar ao mínimo os dados que recolhemos. Com este texto pretendemos explicar de uma forma clara, simples e concisa quais os dados pessoais que recolhemos, porquê, e o que fazemos com eles.

## Responsável pelo tratamento dos dados:

ANSOL - Associação Nacional para o Software Livre, NIPC: 513661174, com sede estatutária na Rua de Mouzinho da Silveira, nº 234,4050-417 Porto.

## Quando navega em cada um dos nossos sites

### Cookies e rastreadores

Um cookie é um ficheiro de texto que é guardado no navegador do seu computador pelo servidor da página a que acede ([mais informação](https://pt.wikipedia.org/wiki/Cookie_\(inform%C3%A1tica\)) ). Quando estritamente necessário, utiliza um cookie não persistente de exclusivamente técnico que serve para reconhecer e gerir uma sessão do visitante e dar-lhe uma experiência melhor no site. Este cookie não recolhe dados pessoais, e é automaticamente apagado assim que fecha no navegador. Quando se autentica nos nossos sites são cookies adicionais, também de carácter exclusivamente técnico. Não utilizamos cookies de terceiros. Não utilizamos rastreadores.

### IP

O endereço IP é sempre necessário para estabelecer uma comunicação entre o servidor do site e o computador do visitante. Os endereços de IP dos visitantes do site são registrados no nosso servidor. A listagem de IPs de visitantes não é associada a outro tipo de informação. Este dado é recolhido por razões de segurança e prevenção de fraude, o que corresponde a um dos fundamentos legalmente resultados para uma licitude da operação em causa: o legítimo interesse do responsável pelo tratamento de dados (previsto no art.º 6/1 / f fazer RGPD).

## Quando se inscrever como sócio

### Dados recolhidos

Quem quiser juntar-se à nossa Associação pode fazê-lo tornando-se sócio. Este registo implica necessariamente a recolha de certos dados pessoais. Assim, mediante o preenchimento de formulário online de sócio recolhemos:

* nome de utilizador, para acesso aos serviços digitais providenciados aos sócios
* nome completo
* morada
* endereço de correio electrónico
* Número de documento de identificação

### Fundamento

Os dados são recolhidos e tratados com base na relação pré-contratual / contratual entre candidato / sócio e a associação (Art. 6º nº1 b) do Regulamento Geral de Protecção de Dados), pois configuram informações necessárias ao normal decorrer do negócio jurídico estabelecido ou a estabelecer, nomeadamente em relação à identificação do sócio.

A morada é uma exigência legal (cfr, art. 174º nº1 do Código Civil) para o envio de convocação para Assembleia Geral. O fundamento jurídico do seu tratamento reside então no facto de corresponder a uma obrigação legal (artigo 6.º/1/c RGPD).

A morada poderá ser utilizada para confirmação de contactos do sócio que por qualquer razão perca acesso ao e-mail utilizado no registo, encontrando-se aqui um legítimo interesse para o tratamento desses dados (art. 6/1/d RGPD). Os restantes dados recolhidos destinam-se, exclusivamente, a permitir a comunicação entre candidato/sócio e a associação, gestão dos membros da associação e verificação de identidade em actos eleitorais.

Em suma, a comunicação dos seus dados é uma obrigação contratual para que possa ser sócio da associação, pelo que não se poderá tornar sócio, ou manter-se como sócio, caso não nos permita o tratamento dos seus dados.

### Por quanto tempo são mantidos os presentes dados?

Os dados dos candidatos levantam até à decisão relativa à sua candidatura, e nunca por mais de 100 dias. Os dados dos sócios serão coletados enquanto se mantiverem como sócios da associação.

### Quem acede a estes dados?

Os dados recolhidos são comunicados aos membros da direcção da associação. São ainda comunicados aos titulares dos restantes órgãos sociais os dados corrigidos ao desempenho das funções.

### Infra-estruturas informáticas

O nosso site utiliza infra-estruturas informáticas ("alojamento") que são fornecidos pela empresa Netureza, em servidores em território nacional.

## Quando usa as listas de distribuição (listas de distribuição)

À medida que as nossas listas de distribuição utilizam o software Mailman. Qualquer pessoa pode registar-se existindo, contudo, listas específicas de acesso restrito. Todas as listas possuem um arquivo de mensagens trocadas, e algumas têm esse arquivo publicado.

### Dados recolhidos

Com o registo na lista, é recolhido um endereço de e-mail e, opcionalmente, um nome ou pseudónimo.

### Fundamento

A participação nas listas de distribuição, espaço em que as pessoas trocam ideias, opiniões, partilham interesses e divergências explicam, implica a associação dos participantes a um endereço de correio eletrônico. Um escrito para o endereço de correio eletrônico dos participantes é necessária ao registo e ao envio de conteúdos de mensagens. O fundamento do tratamento desde dado pessoal, neste contexto, é tratar-se de uma operação necessária à prestação em causa (art. 6.º / 1 / c RGPD).

### Infra-estrutura informática

As listas de distribuição utilizam uma infra-estrutura informática ("alojamento") cedida gratuitamente pela empresa Netureza, em servidores fornecidos em território nacional.

### Por quanto tempo são coletados estes dados?

Os dados relativos às listas de discussão são coletados até que o usuário se desinscreva delas, exceto os arquivos das mensagens.

## Não se esqueça dos seus direitos!

Tem sempre o direito de acesso, retificar ou apagar, opor-se ou limitar o tratamento dos seus dados, e retirar o consentimento quando seja esse o fundamento para a operação sobre os seus dados pessoais. Note que mesmo depois de o titular retirar o permissão à operação em causa, o tratamento que foi dado até então válido.

Basta enviar-nos um e-mail para: contacto@ansol.org.

Se se sentir insatisfeito com o tratamento de dados pessoais, pode também apresentar uma reclamação à autoridade de controle e. Em Portugal, autoridade de controlo é a Comissão Nacional de Protecção de Dados.

## O que acha da nossa política de privacidade?

Ficou alguma coisa por esclarecer? Podemos melhorar? Queremos ser claros, transparentes e completos em informações que fornecemos sobre o tratamento de dados pessoais. Por favor, se achar que pode ajudar-nos a melhorar ou se não achou a nossa política de privacidade suficientemente, dê-nos a sua opinião para: contacto@ansol.org .
