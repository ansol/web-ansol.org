---
title: Documentação e Transparência
aliases:
- /documentacao
---


## Documentação

- [Estatutos]({{< ref "estatutos" >}})
- [Regulamento Interno]({{< ref "regulamento-interno" >}})

### Apenas visível a membros da ANSOL
- [Reuniões da Direcção](https://opencloud.ansol.org/f/972)
- [Assembleias Gerais](https://opencloud.ansol.org/f/239441)


## Registo de transparência da UE

Número de Registo de Transparência da UE: [736822225106-05](https://transparency-register.europa.eu/searchregister-or-update/organisation-detail_pt?id=736822225106-05)

## Protocolos

A ANSOL assinou, em agosto de 2022, protocolos de parceria com os Agrupamentos de Escolas
de Santa Maria da Feira e Augusto Cabrita:

- [Protocolo de parceria com Agrupamento de Escolas de Santa Maria da Feira][protocolo-feira]
- [Protocolo de parceria com Agrupamento de Escolas Augusto Cabrita][protocolo-barreiro]

[protocolo-feira]: /protocolos/smfeira.pdf
[protocolo-barreiro]: /protocolos/barreiro.pdf

Em 2022 a ANSOL tornou-se associada da PASC – Plataforma de Associações da
Sociedade Civil – Casa da Cidadania. A PASC representa a sociedade civil na
Rede Nacional de Administração Aberta, uma rede criada para coordenar a
participação de Portugal na OGP.

## Reuniões com partidos políticos, deputados e entidades governamentais

- Fevereiro de 2024:
  - Eurodeputado José Gusmão, BE | Parlamento Europeu - Bruxelas | ChatControl, FediGov, Public Money Public Code, Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado Francisco Guerreiro, Independente | Parlamento Europeu - Bruxelas | FediGov, Public Money Public Code, Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputada Sandra Pereira, PCP | Parlamento Europeu - Bruxelas | FediGov, Public Money Public Code, Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado Carlos Zorrinho, PS | Parlamento Europeu - Bruxelas | FediGov, Public Money Public Code, Direito de instalar qualquer software em qualquer dispositivo
  - Vogal no Conselho Directivo do IRN I.P. Bruno Adrego Maia | FOSDEM - Bruxelas | Protocolo de colaboração
  - Candidato da CDU às Eleições Legislativas Miguel Tiago e membro da Comissão de Assuntos Económicos do PCP Manuel Gouveia | Reunião online | Ideias para a legislatura 2024

- Fevereiro de 2023:
  - Eurodeputada Maria da Graça Carvalho, PSD | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado Francisco Guerreiro, Independente | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Assistente da Eurodeputada Sandra Pereira, PCP | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado José Gusmão, BE | Parlamento Europeu - Bruxelas | ChatControl
  - Assistentes dos Eurodeputados Carlos Zorrinho e Isabel Santos, PS | Parlamento Europeu - Bruxelas | ChatControl

## Relatórios

- [Plano de actividades para 2025](/relatorios/2025-plano-de-actividades.pdf), provisório;
- [Relatório de Actividade e Contas de 2024](/relatorios/2024-relatorio-actividade-e-contas.pdf), provisório;

- [Relatório de Resumo da Festa do Software Livre 2024 (inglês)](/relatorios/2024-festa-do-software-livre-relatorio-resumo-en.pdf);
- [Relatório de Resumo da Festa do Software Livre 2024 (português)](/relatorios/2024-festa-do-software-livre-relatorio-resumo-pt.pdf);
- [Relatório de Contas da Festa do Software Livre 2024](/relatorios/2024-festa-do-software-livre-relatorio-de-contas.pdf);

- [Plano Estratégico 2024 - 2030](/relatorios/2024-plano-estrategico.pdf), apresentado em Assembleia Geral a 16 de março de 2024;
- [Plano de actividades para 2024](/relatorios/2024-plano-de-actividades.pdf), apresentado em Assembleia Geral a 16 de março de 2024;
- [Relatório de Actividade e Contas de 2023](/relatorios/2023-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 16 de março de 2024;

- [Relatório de Resumo da Festa do Software Livre 2023 (inglês)](/relatorios/2023-festa-do-software-livre-relatorio-resumo-en.pdf);
- [Relatório de Resumo da Festa do Software Livre 2023 (português)](/relatorios/2023-festa-do-software-livre-relatorio-resumo-pt.pdf);
- [Relatório de Contas da Festa do Software Livre 2023](/relatorios/2023-festa-do-software-livre-relatorio-de-contas.pdf);

- [Plano de actividades para 2023](/relatorios/2023-plano-de-actividades.pdf), apresentado em Assembleia Geral a 29 de abril de 2023;
- [Relatório de Actividade e Contas de 2022](/relatorios/2022-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 29 de abril de 2023;

- [Plano de actividades para 2022](/relatorios/2022-plano-de-actividades.pdf), apresentado em Assembleia Geral a 5 de março de 2022;
- [Relatório de Actividade e Contas de 2021](/relatorios/2021-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 5 de março de 2022;

- Plano de actividades para 2021: não foi redigido, porque a Assembleia Geral foi adiada para dezembro;
- [Relatório de Actividade e Contas de 2020](/relatorios/2020-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 18 de dezembro de 2021;

- Plano de actividades para 2020: em falta;
- [Relatório de Actividade e Contas de 2019](/relatorios/2019-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 29 de fevereiro de 2020;

- [Plano de actividades para 2019](/relatorios/2019-plano-de-actividades-e-orcamento.pdf), apresentado em Assembleia Geral a 16 de março de 2019;
- [Relatório de Actividade e Contas de 2018](/relatorios/2018-relatorio-actividade-e-contas.pdf): apresentado em Assembleia Geral a 16 de março de 2019;

- [Plano de actividades para 2018](/relatorios/2018-plano-de-actividades-e-orcamento.pdf), apresentado em Assembleia Geral a 27 de janeiro de 2018;
- [Relatório de Actividade e Contas de 2017](/relatorios/2017-relatorio-actividade-e-contas.pdf): apresentado em Assembleia Geral a 27 de janeiro de 2018;

- [Plano de actividades para 2017](/relatorios/2017-plano-de-actividades-e-orcamento.pdf): apresentado em Assembleia Geral a 12 de março de 2017;
- Relatório de Actividade e Contas de 2016: em falta;

- Plano de actividades para 2016: em falta;
- [Relatório de Actividade e Contas de 2015](/relatorios/2015-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 17 de setembro de 2016;

- Plano de actividades para 2015: em falta;
