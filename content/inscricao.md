---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 18
layout: page
title: Inscrição na ANSOL
created: 1333266103
date: 2012-04-01
aliases:
- "/node/18/"
- "/page/18/"
---

Pode inscrever-se na associação qualquer pessoa (portuguesa ou estrangeira,
moradora em Portugal ou no estrangeiro) que possa contribuir para a prossecução
dos objectivos da Associação. Não são aceites inscrições de empresas, mas uma
ou várias pessoas da empresa podem fazer a sua inscrição individual.

O primeiro passo para a inscrição consiste em tomar conhecimento e
aceitar os <a href="{{< ref "estatutos" >}}">estatutos</a> e
o <a href="{{< ref "regulamento-interno" >}}">regulamento interno</a>.

A jóia de inscrição é actualmente inexistente (0.00€), e a **quota anual são 30.00€**
(6.00€ no caso de estudantes, pessoas desempregadas ou reformadas,
mediante apresentação de comprovativo).

O pagamento da quota pode ser feito pessoalmente, por cheque, MultiBanco,
MBWay, ou transferência bancária para o IBAN
**PT50&nbsp;0035&nbsp;2178&nbsp;00027478430&nbsp;14**.

A inscrição é feita através do preenchimento do seguinte formulário:

<style>
  .control-group {
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
    margin-bottom: 20px;
  }

  .control-group p { font-size: small; margin-bottom: 0 !important; }

  .control-label { min-width: 170px; }
  .controls { display: flex; flex-wrap: wrap; gap: 10px; flex-grow: 1;}
  .input-large, .input-xxlarge { width: 100%; }


</style>
<form method="post" name="inscricaosocio" action="https://form.ansol.org/forms/inscricao_socios_form.php" class="form-horizontal">
  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="nomesocial">Nome social</label>
    <div class="controls">
      <input id="nomesocial" name="nomesocial" type="text" placeholder="Nome social" class="input-large" required>
    </div>
    <p>O nome pelo qual a pessoa é conhecida, utilizado nas comunicações com a associação. Muitas vezes é o primeiro e último nome.</p>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="nome">Nome completo</label>
    <div class="controls">
      <input id="nome" name="nome" type="text" placeholder="Nome completo" class="input-large" required>
    </div>
    <p>O nome legal completo, usado apenas nos registos internos da associação para fins administrativos.</p>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="email">Email</label>
    <div class="controls">
      <input id="email" name="email" type="email" placeholder="Endereço de correio electrónico" class="input-large" required>
    </div>
    <p>Este endereço vai ser utilizado em todas as comunicações, incluíndo convocatórias para a Assembleia Geral. É importante que seja visto regularmente.</p>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="bicc">BI/Cartão de Cidadão</label>
    <div class="controls">
      <input id="bicc" name="bicc" type="text" placeholder="Nº de BI ou Cartão de Cidadão" class="input-large" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="morada">Morada</label>
    <div class="controls">
      <textarea rows=5 id="morada" name="morada" type="text" placeholder="Morada completa" class="input-large" required></textarea>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="cp">Código Postal</label>
    <div class="controls">
      <input id="cp" name="cp" type="text" placeholder="0000-000" class="input-mini" required>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="localidade">Localidade</label>
    <div class="controls">
      <input id="localidade" name="localidade" type="text" placeholder="Localidade" class="input-mini" required>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="localidade">Como nos conheceste?</label>
    <div class="controls">
      <input id="referencia" name="referencia" type="text" placeholder="Evento, artigo em revista, etc." class="input-large" required>
    </div>
    <p>Onde ouviste falar da ANSOL / o que te levou a inscrever agora? Informação usada apenas para nos ajudar a perceber que métodos de divulgação funcionam melhor.</p>
  </div>

  <!-- Multiple Checkboxes (inline) -->
  <div class="control-group">
    <label class="checkbox inline" for="maillist-0">
      <input type="checkbox" name="maillist" id="maillist-0" value="newsletter">
      Pretendo receber newsletters e outras comunicações para sócios
      (Utilizada para contacto entre a ANSOL e os seus sócios, assim como entre estes.)
    </label>
  </div>

  <!-- Multiple Checkboxes -->
  <div class="control-group">
    <label class="checkbox" for="statement-0">
      <input type="checkbox" name="statement" id="statement-0" value="Declaro ter conhecimento e cumprir os Estatutos e o Regulamento Interno da ANSOL" required>
      Declaro ter conhecimento e cumprir os <a href="{{< ref "estatutos" >}}" target="_blank">Estatutos</a> e o <a href="{{< ref "regulamento-interno" >}}" target="_blank">Regulamento Interno</a> da ANSOL
    </label>
  </div>

  <!-- Button -->
  <div class="control-group" style="display: flex; justify-content: center">
    <button id="submit" name="submit" class="cta">Submeter Inscrição</button>
  </div>
</form>

<span style="font-size: 13.008px; line-height: 1.538em;">
  A direcção da ANSOL reúne uma vez por semana para aprovar novas inscrições.
  Podemos demorar cerca de uma semana a responder ao teu pedido.
  Após aprovação por parte da direcção, receberás um email com instruções de pagamento das quotas.
  A inscrição é considerada activa a partir do pagamento.
</span>
